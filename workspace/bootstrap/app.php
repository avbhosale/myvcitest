<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {

}

$app = new Laravel\Lumen\Application(realpath(__DIR__ . '/../'));
$app->instance('path.config', app()->basePath() . DIRECTORY_SEPARATOR . 'config');
$app->withFacades();
$app->withEloquent();

$app->singleton(Illuminate\Contracts\Debug\ExceptionHandler::class, App\Exceptions\Handler::class);
$app->singleton(Illuminate\Contracts\Console\Kernel::class, App\Console\Kernel::class);
$app->middleware([App\Http\Middleware\InputTrim::class]);
$app->routeMiddleware(['AzureAuth' => App\Http\Middleware\AzureAuth::class]);
$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(Dingo\Api\Provider\LumenServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);

$app->register(Modules\Infrastructure\InfrastructureServiceProvider::class);
$app->register(Modules\User\UserServiceProvider::class);
$app->register(Modules\Group\MasterGroupServiceProvider::class);
$app->register(Modules\Fee\MasterFeeServiceProvider::class);
$app->register(Modules\Hardware\MasterHardwareServiceProvider::class);
$app->register(Modules\Sec\MasterSecServiceProvider::class);
$app->register(Modules\Holiday\MasterHolidayServiceProvider::class);
$app->register(Modules\Configuration\MasterConfigurationServiceProvider::class);
$app->register(Modules\Note\MasterNoteServiceProvider::class);
$app->register(Modules\Prohibited\MasterProhibitedServiceProvider::class);
$app->register(Modules\State\MasterStateServiceProvider::class);
$app->register(Modules\CorporateStructure\MasterCorporateStructureServiceProvider::class);
$app->register(Modules\DeclineCode\MasterDeclineCodeServiceProvider::class);
$app->register(Modules\FundingTime\MasterFundingTimeServiceProvider::class);
$app->register(Modules\Gateway\MasterGatewayServiceProvider::class);
$app->register(Modules\ACL\ACLServiceProvider::class);
$app->register(Modules\Company\CompanyServiceProvider::class);
$app->register(Modules\Bank\BankServiceProvider::class);


$app->register(SwaggerLume\ServiceProvider::class);
$app->register(Webpatser\Uuid\UuidServiceProvider::class);
$app->register(JohannesSchobel\DingoQueryMapper\DingoQueryMapperServiceProvider::class);

$app->configure('database');
$app->configure('app');
$app->configure('api');
$app->configure('swagger-lume');
$app->configure('dingoquerymapper');

$app['Dingo\Api\Exception\Handler']->setErrorFormat(['status_code' => ':status_code', 'message' => ':message', 'errors' => ':errors', 'debug' => ':debug']);

$app->router->group(['namespace' => 'App\Http\Controllers',], function ($router) {
    include __DIR__ . '/../routes/web.php';
});

/* Define Transformer */
$app['Dingo\Api\Transformer\Factory']->setAdapter(function ($app) {
    $fractal = new League\Fractal\Manager;
    $fractal->setSerializer(new League\Fractal\Serializer\JsonApiSerializer());
    return new Dingo\Api\Transformer\Adapter\Fractal($fractal);
});

return $app;
