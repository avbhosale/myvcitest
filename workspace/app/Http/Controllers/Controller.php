<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Auth
 * @package  Authentication
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace App\Http\Controllers;

use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Routing\Helpers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Laravel\Lumen\Routing\Controller as BaseController;
use Modules\Infrastructure\Services\ValidationRegex;
use stdClass;

//use Illuminate\Support\Facades\Config;

/**
 * Controller class defines for all api call
 *
 * @name     Controller
 * @category Controller
 * @package  App
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id: da75e564ffb4f7fd67bad71778009e8c02067ce9 $
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class Controller extends BaseController
{

    use Helpers;
    use ValidationRegex;

    const SUCCESS_CODE = 200;

    public $cmsUrl = null;

    /**
     * Default constructor
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Validate each request for handling error request
     *
     * @param Obj    $request          is used to get data sent by client
     * @param array  $rules            is rules array passed to handle api request
     * @param array  $message          is display the default error message
     * @param array  $attributes       is attributes are different according to fields
     * @param array  $attributeCode    is code associated with each fields
     * @param String $exceptionMessage is display the exception message if occurred during error handling
     *
     * @name   validateRequest
     * @access protected
     * @author VCI <info@vericheck.net>
     *
     * @return boolean
     */
    protected function validateRequest(Request $request, array $rules, array $message = [], $attributes = [], $attributeCode = [], $exceptionMessage = 'Validation Failed')
    {
        $validator = Validator::make($request->all(), $rules, $message, $attributes);
        if ($validator->fails()) {
            $reformErrors = $this->reformErrors($validator, $attributeCode);
            throw new ResourceException($exceptionMessage, $reformErrors);
        }
        return true;
    }

    /**
     * This method will reform error response.
     *
     * @param Obj   $validator is used to get error object
     * @param array $codes     is code associated with each filed
     *
     * @name   _reformErrors
     * @access protected
     * @author VCI <info@vericheck.net>
     *
     * @return Obj Reform error object
     */
    protected function reformErrors($validator, $codes)
    {
        $reformErrors = [];
        $codeErrors = $validator->errors()->getMessages();
        $objErrors = $validator->failed();
        foreach ($objErrors as $input => $rules) {
            $i = 0;
            foreach ($rules as $rule => $ruleInfo) {
                $code = (!empty($codes) && isset($codes[$input])) ? $codes[$input] : "";
                $errorObj = new stdClass();
                $errorObj->code = $code;
                $errorObj->type = $rule;
                $errorObj->message = $codeErrors[$input][$i];
                $errorObj->more_info = $this->cmsUrl . $code;
                $reformErrors[] = $errorObj;
                $i++;
            }
        }
        return $reformErrors;
    }

    /**
     * Set application guzzlehttp call global to graph api's
     *
     * @param string $method      method type of https request
     * @param string $graphUrl    graph api url
     * @param string $token       user access token
     * @param string $contentType content-type for the request
     * @param string $body        content-type for the request
     *
     * @name   makeGraphApiCall
     * @access protected
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    protected function makeGraphApiCall($method = 'GET', $graphUrl = '', $token = '', $contentType = 'application/json', $body = '')
    {
        $response = [];
        $client = new Client(['base_uri' => $graphUrl, 'verify' => false]);
        try {
            if (isset($method) && ($method == 'GET')) {
                $response = $client->request($method, $graphUrl, ['headers' => ['Authorization' => 'Bearer ' . $token, 'Content-Type' => $contentType]]);
            } else {
                $response = $client->request($method, $graphUrl, ['headers' => ['Authorization' => 'Bearer ' . $token, 'Content-Type' => 'application/json'],
                    'body' => $body]
                );
            }
            $response = $response->getBody()->getContents();
        } catch (\Exception $exception) {
            $res = json_decode($exception->getResponse()->getBody()->getContents(), true);
            if (isset($res['odata.error']['message'])) {
                $response['data']['error']['status_code'] = $exception->getCode();
                $response['data']['error']['message'] = $res['odata.error']['message']['value'];
                return json_encode($response);
            } else {
                $response = $res;
            }
        }
        return $response;
    }
}
