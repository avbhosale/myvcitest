<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Providers
 * @package  Event
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

/**
 * Service provider class for events
 *
 * @name     OAuthController
 * @category Auth
 * @package  Authentication
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id: da75e564ffb4f7fd67bad71778009e8c02067ce9 $
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Modules\Infrastructure\Events\Log\ActivityLogEvent' => [
            'Modules\Infrastructure\Listeners\Log\ActivityLogEventListener',
        ],
        'Modules\User\Events\ACL\DeleteACLMappingEvent' => [
            'Modules\User\Listeners\ACL\DeleteACLMappingEventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @name   MethodName
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        \Queue::failing(function ($connection, $job, $data) {
            // code to log the errors inside the meessage
            Log::info('Redis push queue event is failing');
        });
    }
}
