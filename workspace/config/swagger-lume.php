<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Config
 * @package  Config
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
return [
    'api'                   => [
        /*
          |--------------------------------------------------------------------------
          | Edit to set the api's title
          |--------------------------------------------------------------------------
         */
        'title' => 'Swagger Lume API',
    ],
    'routes'                => [
        /*
          |--------------------------------------------------------------------------
          | Route for accessing api documentation interface
          |--------------------------------------------------------------------------
         */
        'api'             => '/api/documentation',
        /*
          |--------------------------------------------------------------------------
          | Route for accessing parsed swagger annotations.
          |--------------------------------------------------------------------------
         */
        'docs'            => '/docs',
        /*
          |--------------------------------------------------------------------------
          | Route for Oauth2 authentication callback.
          |--------------------------------------------------------------------------
         */
        'oauth2_callback' => '/api/oauth2-callback',
        /*
          |--------------------------------------------------------------------------
          | Route for serving assets
          |--------------------------------------------------------------------------
         */
        'assets'          => '/swagger-ui-assets',
        /*
          |--------------------------------------------------------------------------
          | Middleware allows to prevent unexpected access to API documentation
          |--------------------------------------------------------------------------
         */
        'middleware'      => [
            'api'             => [],
            'asset'           => [],
            'docs'            => [],
            'oauth2_callback' => [],
        ],
    ],
    'paths'                 => [
        /*
          |--------------------------------------------------------------------------
          | Absolute path to location where parsed swagger annotations will be stored
          |--------------------------------------------------------------------------
         */
        'docs'        => storage_path('api-docs'),
        /*
          |--------------------------------------------------------------------------
          | File name of the generated json documentation file
          |--------------------------------------------------------------------------
         */
        'docs_json'   => 'api-docs.json',
        /*
          |--------------------------------------------------------------------------
          | Absolute path to directory containing the swagger annotations are stored.
          |--------------------------------------------------------------------------
         */
        'annotations' => base_path('/'),
        /*
          |--------------------------------------------------------------------------
          | Absolute path to directories that you would like to exclude from swagger generation
          |--------------------------------------------------------------------------
         */
        'excludes'    => ['vendor', 'tests', 'storage', 'routes', 'resources', 'public', 'database', 'config', 'bootstrap', 'app'],
        /*
          |--------------------------------------------------------------------------
          | Edit to set the swagger scan base path
          |--------------------------------------------------------------------------
         */
        'base'        => env('L5_SWAGGER_BASE_PATH', null),
        /*
          |--------------------------------------------------------------------------
          | Absolute path to directory where to export views
          |--------------------------------------------------------------------------
         */
        'views'       => base_path('resources/views/vendor/swagger-lume'),
    ],
    /*
      |--------------------------------------------------------------------------
      | API security definitions. Will be generated into documentation file.
      |--------------------------------------------------------------------------
     */
    'security'              => [
    ],
    /*
      |--------------------------------------------------------------------------
      | Turn this off to remove swagger generation on production
      |--------------------------------------------------------------------------
     */
    'generate_always'       => env('SWAGGER_GENERATE_ALWAYS', false),
    /*
      |--------------------------------------------------------------------------
      | Edit to set the swagger version number
      |--------------------------------------------------------------------------
     */
    'swagger_version'       => env('SWAGGER_VERSION', '2.0'),
    /*
      |--------------------------------------------------------------------------
      | Edit to trust the proxy's ip address - needed for AWS Load Balancer
      |--------------------------------------------------------------------------
     */
    'proxy'                 => false,
    /*
      |--------------------------------------------------------------------------
      | Configs plugin allows to fetch external configs instead of passing them to SwaggerUIBundle.
      | See more at: https://github.com/swagger-api/swagger-ui#configs-plugin
      |--------------------------------------------------------------------------
     */
    'additional_config_url' => null,
    /*
      |--------------------------------------------------------------------------
      | Apply a sort to the operation list of each API. It can be 'alpha' (sort by paths alphanumerically),
      | 'method' (sort by HTTP method).
      | Default is the order returned by the server unchanged.
      |--------------------------------------------------------------------------
     */
    'operations_sort'       => env('L5_SWAGGER_OPERATIONS_SORT', null),
    /*
      |--------------------------------------------------------------------------
      | Uncomment to pass the validatorUrl parameter to SwaggerUi init on the JS
      | side.  A null value here disables validation.
      |--------------------------------------------------------------------------
     */
    'validator_url'         => null,
    /*
      |--------------------------------------------------------------------------
      | Uncomment to add constants which can be used in anotations
      |--------------------------------------------------------------------------
     */
    'constants'             => [
    // 'SWAGGER_LUME_CONST_HOST' => env('SWAGGER_LUME_CONST_HOST', 'http://my-default-host.com'),
    ],
];
