<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Auth
 * @package  Authentication
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
return [
    /*
      |--------------------------------------------------------------------------
      | Default Cache Store
      |--------------------------------------------------------------------------
      |
      | This option controls the default cache connection that gets used while
      | using this caching library. This connection is used when another is
      | not explicitly specified when executing a given caching function.
      |
      | Supported: "apc", "array", "database", "file", "memcached", "redis"
      |
     */

    'default' => env('CACHE_DRIVER', 'redis'),
    /*
      |--------------------------------------------------------------------------
      | Cache Stores
      |--------------------------------------------------------------------------
      |
      | Here you may define all of the cache "stores" for your application as
      | well as their drivers. You may even define multiple stores for the
      | same cache driver to group types of items stored in your caches.
      |
     */
    'stores' => [
        'redis' => [
            'client' => 'phpredis',
            'driver' => 'redis',
            'default' => [
                'host' => env('REDIS_HOST', 'localhost'),
                'password' => env('REDIS_PASSWORD', null),
                'port' => env('REDIS_PORT', 6379),
                'database' => 0
            ]
        ]
    ],
    /*
      |--------------------------------------------------------------------------
      | Cache Key Prefix
      |--------------------------------------------------------------------------
      |
      | When utilizing a RAM based store such as APC or Memcached, there might
      | be other applications utilizing the same cache. So, we'll specify a
      | value to get prefixed to all our keys so we can avoid collisions.
      |
     */
    'prefix' => env('CACHE_PREFIX', str_slug(env('APP_ENV', 'DIT'), '_')),
];
