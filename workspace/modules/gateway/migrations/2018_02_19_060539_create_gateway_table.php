<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateGatewayTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateGatewayTable class is used to create Master.Gateway table
 *
 * @name     CreateGatewayTable.php
 * @category Migration
 * @package  CreateGatewayTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateGatewayTable extends Migration
{

    public $tableName;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Master'; // Schema Name to Identify Table group
        $tableName = 'Gateway'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Master.Gateway
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('GatewayId')->primary()->default(DB::raw('newsequentialid()')) : $table->bigIncrements('GatewayId')->unsigned()->primary();
            $table->string('Type', '64');
            $table->string('Description', '1024')->nullable();
            $table->tinyInteger('Status')->default(0);
            $table->integer('CreatedAt');
            $table->integer('Etag')->default(0);
            $table->timestamp('DeletedAt')->nullable();
        });
    }

    /**
     * Function used to Drop table Master.Gateway
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
