<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Models;

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="GroupGetData",
 *  type="object",
 *  @SWG\Property(property="data",  type="array",  @SWG\Items(ref="#/definitions/GroupData")),
 *  @SWG\Property(property="meta",  type="object", ref="#/definitions/Meta"),
 *  @SWG\Property(property="links", type="object", ref="#/definitions/Links")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="GroupPostData",
 *  type="object",
 *  @SWG\Property(property="name",        type="string", description="group name"),
 *  @SWG\Property(property="type",        type="string", description="group type"),
 *  @SWG\Property(property="status",      type="integer",description="status active or inactive"),
 *  @SWG\Property(property="description", type="string", description="name"),
 *  @SWG\Property(property="created_by",  type="string", description="group created by"),
 *  @SWG\Property(property="updated_by",  type="string", description="group updated by"),
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="GroupPutData",
 *  type="object",
 *  @SWG\Property(property="name",        type="string", description="group name"),
 *  @SWG\Property(property="type",        type="string", description="group type"),
 *  @SWG\Property(property="description", type="string", description="name"),
 *  @SWG\Property(property="status",      type="integer",description="status active or inactive"),
 *  @SWG\Property(property="updated_by",  type="string", description="group updated by"),
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="GroupDeleteData",
 *  type="object",
 *  @SWG\Property(property="id",     type="string", description="group id"),
 *  @SWG\Property(property="status", type="string", description="status as deleted"),
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="GroupData",
 *  type="object",
 *  @SWG\Property(property="id",          type="string", description="id"),
 *  @SWG\Property(property="type",        type="string", description="type"),
 *  @SWG\Property(
 *          property="attributes",
 *          type="object",
 *          @SWG\Property(property="name",        type="string",  description="group name"),
 *          @SWG\Property(property="type",        type="string",  description="group type"),
 *          @SWG\Property(property="status",      type="integer", description="status active or inactive"),
 *          @SWG\Property(property="description", type="string",  description="group description"),
 *          @SWG\Property(property="created_by",  type="string",  description="group created by"),
 *          @SWG\Property(property="updated_by",  type="string",  description="group updated by"),
 *          @SWG\Property(property="created_at",  type="integer", description="group created at"),
 *          @SWG\Property(property="etag",        type="integer", description="group updated at")
 *      )
 * )
 */
