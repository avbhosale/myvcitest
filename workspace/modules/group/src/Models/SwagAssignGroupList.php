<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Swagger
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\AssignGroup\Models;

/**
 * Output response in json format
 *
 * @SWG\Definition(
 *  definition="AssignGroupGetData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="array",
 *  @SWG\Items(ref="#/definitions/Data")),
 *  @SWG\Property(property="meta",
 *  type="object", ref="#/definitions/Meta"),
 *  @SWG\Property(property="links",
 *  type="object", ref="#/definitions/Links")
 * )
 */

/**
 * Swagger Definition
 *
 * @SWG\Definition(
 *  definition="AssignGroupData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="object",  ref="#/definitions/Data")
 * )
 */


/**
 * Create api for AssignGroup
 *
 * @SWG\Definition(
 *  definition="AssignGroupPostData",
 *  type="object",
 *  @SWG\Property(property="group_id",    type="string", description="group_id"),
 *  @SWG\Property(property="company_ids", type="array", @SWG\Items(ref="#/definitions/AssignCompanyGroup"))
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AssignGroupDeleteData",
 *  type="object",
 *  @SWG\Property(property="id",     type="string", description="group id"),
 *  @SWG\Property(property="status", type="string", description="status as deleted"),
 * )
 */


/**
 * Update api for AssignGroup .
 *
 * @SWG\Definition(
 *  definition="AssignGroupPutData",
 *  type="object",
 *  @SWG\Property(property="group_id",    type="string", description="group_id"),
 *  @SWG\Property(property="company_ids", type="array", @SWG\Items(ref="#/definitions/AssignCompanyGroup"))
 * )
 */

/**
 * Assign group
 *
 * @SWG\Definition(
 *  definition="AssignCompanyGroup"
 * )
 */


/**
 * Api for Get all AssignGroup data
 *
 * @SWG\Definition(
 *  definition="ListAssignGroupData",
 *  type="object",
 *  @SWG\Property(property="id",
 *  type="string", description="id"),
 *  @SWG\Property(property="type",
 *  type="string", description="type"),
 *  @SWG\Property(
 *          property="attributes",
 *          type="object",
 *          @SWG\Property(property="group_id",    type="string", description="group_id"),
 *          @SWG\Property(property="company_ids", type="array", @SWG\Items(ref="#/definitions/AssignCompanyGroup"))
 *      )
 * )
 */
