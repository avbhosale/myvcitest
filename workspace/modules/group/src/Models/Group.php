<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Models;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Group Model
 *
 * @name     Group
 * @category Model
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class Group extends BaseModel
{

    use SoftDeletes;

    const DELETED_AT = 'DeletedAt';

    protected $tablePrefix = 'Master';
    protected $table = 'Group';
    protected $primaryKey = 'GroupId';
    protected $fillable = [
        'GroupId',
        'Name',
        'Type',
        'Status',
        'Description',
        'CreatedBy',
        'UpdatedBy',
        'CreatedAt',
        'Etag',
        'DeletedAt'
    ];
    protected $dates = ['DeletedAt'];
    public $timestamps = false;
    public $errorCodes = [
        'name' => 'GR001',
        'type' => 'GR002',
        'description' => 'GR004',
        'status' => 'GR003',
        'created_by' => 'GR005',
        'updated_by' => 'GR006'];

    /**
     * Default Constructor
     *
     * @param array $attributes constructor attributes
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $this->perPage = config('app.records_per_page');
    }
}
