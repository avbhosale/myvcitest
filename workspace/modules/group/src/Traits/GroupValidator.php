<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Master_Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Traits;

/**
 * Master group module validator methods
 *
 * @name     GroupValidator
 * @category Trait
 * @package  Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait GroupValidator
{

    /**
     * Function to set validation rules
     *
     * @param string  $action action name
     * @param integer $id     request object
     *
     * @name   validationRules
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules($action, $id = null)
    {
        $table = config('database.default') . '.' . $this->_groupRepository->getTableName();

        $rules = [];

        switch ($action) {
            case 'create':
                $rules = [
                    'name' => 'bail|required|alpha_num|unique:' . $table . ',Name,NULL,GroupId,DeletedAt,NULL|min:3|max:128',
                    'description' => 'bail|max:2048|string|nullable',
                    'type' => 'bail|required|alpha|max:16',
                    'status' => 'bail|min:0|max:1|numeric',
                    'created_by' => 'bail|required|regex:' . static::$REGEX_UUID,
                    'updated_by' => 'bail|required|regex:' . static::$REGEX_UUID,
                ];
                break;
            case 'update':
                $rules = [
                    'name' => 'bail|required|alpha_num|unique:' . $table . ',Name,' . $id . ',GroupId,DeletedAt,NULL|min:3|max:128',
                    'description' => 'bail|max:2048|string|nullable',
                    'type' => 'bail|required|alpha|max:16',
                    'status' => 'bail|min:0|max:1|numeric',
                    'updated_by' => 'bail|required|regex:' . static::$REGEX_UUID,
                ];
                break;
        }
        return $rules;
    }

    /**
     * Function for validation messages
     *
     * @param action $action action name
     *
     * @name   validationMessage
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage($action)
    {
        $messages = [];

        switch ($action) {
            case 'create':
                $messages = [
                    'status.min' => 'The :attribute must be either 1 or 0.',
                    'status.max' => 'The :attribute must be either 1 or 0.',
                ];
                break;
            case 'update':
                $messages = [
                    'status.min' => 'The :attribute must be either 1 or 0.',
                    'status.max' => 'The :attribute must be either 1 or 0.',
                ];
                break;
        }
        return $messages;
    }
}
