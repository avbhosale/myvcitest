<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Master_Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Traits;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Group\Models\Group;
use Modules\Group\Transformers\GroupTransformer;
use Modules\Infrastructure\Services\QueryMapper;

/**
 * Master group module list method
 *
 * @name     ListGroup
 * @category Trait
 * @package  Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ListGroup
{

    /**
     * Get Group List
     *
     * @param Obj $request Request Object
     *
     * @name   listGroup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function listGroup(Request $request)
    {
        try {
            $objGroup = new Group();
            $queryMapper = new QueryMapper($request);
            $groups = $queryMapper->createFromModel($objGroup);
            $groups = $groups->build()->paginate();
            return $this->response->paginator($groups, new GroupTransformer, ['key' => 'group']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
