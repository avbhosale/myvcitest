<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Master_Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Traits;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Group\Models\Group;

/**
 * Master group module edit methods
 *
 * @name     EditGroup
 * @category Trait
 * @package  Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait EditGroup
{

    /**
     * Edit Group by Group ID
     *
     * @param Obj    $request Request Object
     * @param String $id      Group ID
     *
     * @name   editGroup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function editGroup(Request $request, $id)
    {
        try {
            // check for valid group id
            $group = $this->checkRecordExistsById(new Group(), $id);

            $this->validateRequest($request, $this->_validationRules('update', $id), $this->_validationMessage('update'), $this->_setAttributes(), $this->_setAttributeCode());

            $group = $this->_groupRepository->update($group, $this->_groupTransformer->transformRequestParameters($request->all()));
            return $this->response->item($group, $this->_groupTransformer, ['key' => 'group']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
