<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Unit_Test_Case
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Tests\GroupTest;

use Modules\Group\Models\Group;
use Modules\Group\Traits\AddGroup;
use Modules\Group\Traits\DeleteGroup;
use Modules\Group\Traits\EditGroup;
use Modules\Group\Traits\GroupAttributes;
use Modules\Group\Traits\GroupValidator;
use Modules\Group\Traits\ListGroup;
use Modules\Group\Traits\ShowGroup;
use TestCase;

/**
 * Test case for Master Group Module
 *
 * @name     GroupTest
 * @category UnitTestCase
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class GroupTest extends TestCase
{

    protected $groupRepository;
    protected $groupTransformer;
    protected $model;

    use GroupAttributes;
    use GroupValidator;
    use AddGroup;
    use EditGroup;
    use DeleteGroup;
    use ListGroup;
    use ShowGroup;

    /**
     * Setup repository
     *
     * @name   setup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function setup()
    {
        parent::setUp();
        $this->model = new Group();
    }

    /**
     * Test CRUD operation on API
     *
     * @name   testCRUDApi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDApi()
    {
        $arrFakeGroup = factory(Group::class)->make()->toArray();

        // add group
        $addResponse = $this->call('POST', 'groups', $arrFakeGroup);
        $this->refreshApplication();

        $this->assertEquals(200, $addResponse->status());

        $intGroupId = json_decode($addResponse->getContent())->data->id;

        // list group
        $listResponse = $this->call('GET', 'groups');
        $this->refreshApplication();

        $this->assertEquals(200, $listResponse->status());

        // list group paginationation and sorting
        $listResponse = $this->call('GET', 'groups?page=3&limit=5&sort=-Name');
        $this->refreshApplication();

        $this->assertEquals(200, $listResponse->status());

        // show group by ID
        $showResponse = $this->call('GET', 'groups/' . $intGroupId);
        $this->refreshApplication();

        $this->assertEquals(200, $showResponse->status());

        // edit group by ID
        $arrFakeGroup = factory(Group::class)->make()->toArray();

        $editResponse = $this->call('PUT', 'groups/' . $intGroupId, $arrFakeGroup);
        $this->refreshApplication();

        $this->assertEquals(200, $editResponse->status());

        //delete group by ID
        $deleteResponse = $this->call('DELETE', 'groups/' . $intGroupId);
        $this->refreshApplication();

        $this->assertEquals(200, $deleteResponse->status());

        $objGroup = $this->model::withTrashed()->find($intGroupId);

        $objGroup->forceDelete();
    }

    /**
     * Test CRUD operation on API for failure
     *
     * @name   testCRUDApiFail
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDApiFail()
    {
        $arrFakeGroup = [];
        $intFakeID = factory(Group::class)->make()->toArray()['created_by'];

        // add group
        $addResponse = $this->call('POST', 'groups', $arrFakeGroup);
        $this->refreshApplication();

        $this->assertEquals(422, $addResponse->status());

        $intGroupId = '';

        // blank ID in edit
        $editResponse = $this->call('PUT', 'groups/' . $intGroupId, $arrFakeGroup);
        $this->refreshApplication();

        $this->assertEquals(405, $editResponse->status());

        // invalid ID in edit
        $editResponse = $this->call('PUT', 'groups/' . $intFakeID, factory(Group::class)->make()->toArray());
        $this->refreshApplication();

        $this->assertEquals(404, $editResponse->status());

        //delete group by ID
        $deleteResponse = $this->call('DELETE', 'groups/' . $intGroupId);
        $this->refreshApplication();

        $this->assertEquals(405, $deleteResponse->status());

        // show group by ID
        $showResponse = $this->call('GET', 'groups/' . $intFakeID);
        $this->refreshApplication();

        $this->assertEquals(404, $showResponse->status());

        $intGroupId = 'ahksdgaksdgh';

        // show group by ID
        $showResponse = $this->call('GET', 'groups/' . $intGroupId);
        $this->refreshApplication();

        $this->assertEquals(500, $showResponse->status());

        //delete group by ID
        $deleteResponse = $this->call('DELETE', 'groups/' . $intGroupId);
        $this->refreshApplication();

        $this->assertEquals(500, $deleteResponse->status());
        $this->refreshApplication();

        //delete group by invalid ID
        $deleteResponse = $this->call('DELETE', 'groups/' . $intFakeID);
        $this->refreshApplication();

        $this->assertEquals(404, $deleteResponse->status());
    }
}
