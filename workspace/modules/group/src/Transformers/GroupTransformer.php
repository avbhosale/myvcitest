<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Transformers;

use Modules\Group\Models\Group;
use League\Fractal\TransformerAbstract;

/**
 * Two line description of class
 *
 * @name     GroupTransformer
 * @category Transformer
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class GroupTransformer extends TransformerAbstract
{

    /**
     * Function to set transform format
     *
     * @param Model $group Group model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Group $group)
    {
        $transform = [
            'id' => $group->GroupId,
            'name' => $group->Name,
            'type' => $group->Type,
            'status' => $group->Status,
            'description' => $group->Description,
            'created_by' => $group->CreatedBy,
            'updated_by' => $group->UpdatedBy,
            'created_at' => $group->CreatedAt,
            'etag' => $group->Etag,
            'merchant' => 0
        ];

        return $transform;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array  $input  array of input received
     * @param String $action action string create or update
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input, $action = 'update')
    {
        $groupData = [];
        $groupData["Status"] = 1;
        $groupData["Name"] = $input['name'];
        $groupData["Type"] = $input['type'];
        $groupData["Etag"] = time();
        $groupData["Description"] = $input['description'];
        if (isset($input['status'])) {
            $groupData["Status"] = $input['status'];
        }
        if ($action == 'create') {
            $groupData["CreatedBy"] = $input['created_by'];
            $groupData["CreatedAt"] = time();
            return $groupData;
        }
        $groupData["UpdatedBy"] = $input['updated_by'];
        return $groupData;
    }
}
