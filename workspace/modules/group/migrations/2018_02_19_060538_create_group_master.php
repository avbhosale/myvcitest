<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Migration
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * Two line description of class
 *
 * @name     CreateGroupMaster
 * @category Migration
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateGroupMaster extends Migration
{

    public $tableName;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Master'; // Database Schema
        $tableName = 'Group'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            if (config('database.default') == 'sqlsrv') {
                $table->uuid('GroupId');
                $table->primary('GroupId');
            } else {
                $table->increments('GroupId');
            }
            $table->string('Name', 128);
            $table->string('Description', 2048)->nullable();
            $table->string('Type', 16);
            $table->tinyInteger('Status')->default(0);
            config('database.default') == 'sqlsrv' ? $table->uuid('CreatedBy')->index() : $table->bigInteger('CreatedBy')->unsigned()->index();
            config('database.default') == 'sqlsrv' ? $table->uuid('UpdatedBy')->index()->nullable() : $table->bigInteger('UpdatedBy')->unsigned()->index()->nullable();
            $table->integer('CreatedAt');
            $table->integer('Etag')->default(0);
            $table->dateTime('DeletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
