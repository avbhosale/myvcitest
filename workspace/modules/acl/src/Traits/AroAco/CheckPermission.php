<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\AroAco;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

/**
 * ACL AroAco module methods
 *
 * @name     ShowAroAcoByAroId
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait CheckPermission
{

    /**
     * Check whether user has permission for given ACO ID
     *
     * @param Obj $request Request data
     *
     * @name   hasAccess
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function checkPermission(Request $request)
    {
        try {
            $data = $request->all();

            if (empty($data) || !isset($data['alias']) || !isset($data['user_id'])) {
                return $this->response->errorNotFound();
            }

            $arrARO = $this->_aroRepository->findBy(['ModelId' => $data['user_id']]);
            $objACO = $this->_acoRepository->findOneBy(['Alias' => $data['alias']]);

            if (!is_null($objACO) && !empty($arrARO->all())) {
                $arrARoIds = $arrARO->pluck('AroId');
                $arrAROACO = $this->_aroAcoRepository->getPermissionByAcoIdByAroIds($objACO->AcoId, $arrARoIds);
                $arrACOAccess = $arrAROACO->pluck('Access');

                if (in_array('1', $arrACOAccess->all())) {
                    return $this->response->array(['status_code' => 200, 'message' => 'Permission']);
                } else {
                    return $this->response->array(['status_code' => 403, 'message' => 'Permission Denied']);
                }
            }

            return $this->response->array(['status_code' => 403, 'message' => 'Permission Denied']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
