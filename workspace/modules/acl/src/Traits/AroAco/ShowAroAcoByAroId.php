<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\AroAco;

use Illuminate\Database\QueryException;
use Modules\ACL\Models\AROACO;
use Modules\ACL\Transformers\AROACOTransformer;

/**
 * ACL AroAco module methods
 *
 * @name     ShowAroAcoByAroId
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ShowAroAcoByAroId
{

    /**
     * Show AroAco Method
     *
     * @param String $id AroAco ID
     *
     * @name   showAroAcoByAroId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function showAroAcoByAroId($id)
    {
        try {
            $columnAroId = 'AroId';

            if ($this->boolIsUser) {
                $arrAroIds = $this->aroUser->pluck('AroId')->toArray();
                $aroAco = $this->_aroAcoRepository->findIn($columnAroId, $arrAroIds);

                $arrPermissions = [];
                $arrTempAcos = [];
                if (0 < count($aroAco)) {
                    foreach ($aroAco->all() as $key => $value) {
                        $arrTempAcos[$value->AcoId]['id'] = $value->AcoId;
                        $arrTempAcos[$value->AcoId]['aco_id'] = $value->AcoId;
                        $arrTempAcos[$value->AcoId]['access'][] = $value->Access;
                    }

                    foreach ($arrTempAcos as $acoId => $arrValues) {
                        $objAROACO = new AROACO();
                        $objAROACO->AroAcoId = $arrValues['id'];
                        $objAROACO->AcoId = $arrValues['aco_id'];

                        if (in_array('1', $arrValues['access'])) {
                            $objAROACO->Access = 1;
                        } else {
                            $objAROACO->Access = 0;
                        }

                        $arrPermissions[] = $objAROACO;
                    }
                }

                $acos = collect($arrPermissions);
            } else {
                $aroAco = $this->_aroAcoRepository->getAroAcobyAroid($columnAroId, $id);

                $arrPermissions = [];
                $arrTempAcos = [];
                if (0 < count($aroAco)) {
                    foreach ($aroAco->all() as $key => $value) {
                        $objAROACO = new AROACO();
                        $objAROACO->AroAcoId = $value->AroAcoId;
                        $objAROACO->AcoId = $value->AcoId;
                        $objAROACO->Access = $value->Access;

                        $arrPermissions[] = $objAROACO;
                    }
                }

                $acos = collect($arrPermissions);
            }

            //dd($acos);
            return $this->response->collection($acos, new AROACOTransformer, ['key' => 'ACO']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
