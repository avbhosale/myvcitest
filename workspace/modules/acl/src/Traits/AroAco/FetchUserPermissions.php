<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\AroAco;

use Illuminate\Database\QueryException;

/**
 * Get permissions by user ID
 *
 * @name     FetchUserPermissions
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait FetchUserPermissions
{

    /**
     * Get access of user by AroAco Method
     *
     * @param String $strUserId UserId as id
     *
     * @name   fetchPermissionByUserId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function fetchPermissionByUserId($strUserId)
    {
        try {
            $columnModelId = 'ModelId';
            $columnAroId = 'AroId';
            $arrPermissions = [];

            $aroUser = $this->_aroRepository->findAroById($columnModelId, $strUserId);

            $boolIsUser = false;

            if (0 < count($aroUser)) {
                $boolIsUser = true;

                $arrAroIds = $aroUser->pluck('AroId')->toArray();
                $arrObjAroAcos = $this->_aroAcoRepository->findIn($columnAroId, $arrAroIds);

                if (!empty($arrObjAroAcos->all())) {
                    $arrTempAcos = [];

                    foreach ($arrObjAroAcos->all() as $objAroAco) {
                        $arrTempAcos[$objAroAco->AcoId][] = $objAroAco->Access;
                    }

                    $arrAcoIds = $arrObjAroAcos->unique('AcoId')->pluck('AcoId')->toArray();

                    $arrObjAcos = $this->_acoRepository->findIn('AcoId', $arrAcoIds);

                    if (!empty($arrObjAcos->all())) {
                        foreach ($arrObjAcos->all() as $objAco) {
                            $arrPermissions[$objAco->Alias] = 0;

                            if (isset($arrTempAcos[$objAco->AcoId]) && in_array('1', $arrTempAcos[$objAco->AcoId])) {
                                $arrPermissions[$objAco->Alias] = 1;
                            }
                        }
                    }

                    return $this->response->array(['status_code' => 200, 'data' => $arrPermissions]);
                }
            }

            return $this->response->array(['status_code' => 200, 'data' => $arrPermissions]);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
