<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\AroAco;

/**
 * ACL module methods
 *
 * @name     AroAcoAttributes
 * @category Attribute
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait CheckUserOrRole
{

    public $aroUser;
    public $objAro;

    /**
     * Check whether Aro ID is a User ID or Aro ID
     *
     * @param String $aroId Aro ID
     *
     * @name   checkUserOrRole
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return boolean
     */
    public function checkUserOrRole($aroId)
    {
        $columnModelId = 'ModelId';

        $this->aroUser = $this->_aroRepository->findAroById($columnModelId, $aroId);
        $this->boolIsUser = false;

        if (0 < count($this->aroUser)) {
            $this->boolIsUser = true;
        }

        if (false == $this->boolIsUser) {
            $this->objAro = $this->_aroRepository->findOne($aroId);
            $this->boolIsRole = false;
            if (0 < count($this->objAro)) {
                $this->boolIsRole = true;
                if (!is_null($this->objAro->model_id) && '' != $this->objAro->model_id) {
                    $this->boolIsRole = false;
                    $this->aroUser = $this->_aroRepository->findAroById($columnModelId, $this->objAro->model_id);
                    if (0 < count($this->aroUser)) {
                        $this->boolIsUser = true;
                    }
                }
            }
        }

        if (false == $this->boolIsUser && false == $this->boolIsRole) {
            return $this->response->errorNotFound("The user/role with id {$aroId} doesn't exist");
        }
    }
}
