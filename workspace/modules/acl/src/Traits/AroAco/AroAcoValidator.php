<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\AroAco;

/**
 * ACL module AroAco Validation methods
 *
 * @name     AroAcoValidator
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AroAcoValidator
{

    /**
     * Function to set AroAco validation rules
     *
     * @param string $action action name
     *
     * @name   _aroAcoValidationRules
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _aroAcoValidationRules($action)
    {
        $table = config('database.default') . '.' . $this->_acoRepository->getTableName();

        $rules = [];
        switch ($action) {
            case 'bulkUpdate':
                $rules = [
                    'aco_id' => 'bail|required|regex:' . static::$REGEX_UUID . '|exists:' . $table . ',AcoId,DeletedAt,NULL|max:64',
                    'access' => 'bail|required|numeric|min:0|max:1'
                ];
                break;
        }

        return $rules;
    }

    /**
     * Function for validation messages
     *
     * @param action $aroAcoAction action name
     *
     * @name   _aroAcoValidationMessage
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _aroAcoValidationMessage($aroAcoAction)
    {
        $aroAcoMsg = [];

        switch ($aroAcoAction) {
            case 'bulkUpdate':
                $aroAcoMsg = [
                    'access.min' => 'The :attribute must be either 1 or 0.',
                    'access.max' => 'The :attribute must be either 1 or 0.',
                ];
                break;
        }
        return $aroAcoMsg;
    }
}
