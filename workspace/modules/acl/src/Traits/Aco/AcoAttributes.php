<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aco;

use Modules\ACL\Models\ACO;

/**
 * ACL module methods
 *
 * @name     AcoAttributes
 * @category Attribute
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AcoAttributes
{

    /**
     * Function set attributes for ACO validation messages
     *
     * @name   _setAcoAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAcoAttributes()
    {
        $acoAttributes = [];
        $acoAttributes = [
            'AcoId' => 'ID',
            'Name' => 'Name',
            'ParentId' => 'Parent Id',
            'Model' => 'Model',
            'Alias' => 'Alias',
            'Etag' => 'ETag',
            'Alias' => 'Alias',
            'Etag' => 'ETag',
        ];

        return $acoAttributes;
    }

    /**
     * This Function set attributes for validation messages
     *
     * @name   _setAcoAttributeCode
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAcoAttributeCode()
    {
        $assignAco = new ACO();
        return $assignAco->acoErrorCodes;
    }
}
