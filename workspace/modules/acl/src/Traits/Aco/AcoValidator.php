<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aco;

/**
 * ACL module Aco Validation methods
 *
 * @name     AcoValidator
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AcoValidator
{

    /**
     * Function to set validation rules
     *
     * @param string  $acoAction action name
     * @param integer $id        aco Id
     *
     * @name   _acoValidationRules
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _acoValidationRules($acoAction, $id = null)
    {
        $table = config('database.default') . '.' . $this->_acoRepository->getTableName();

        $acoRules = [];
        switch ($acoAction) {
            case 'create':
                $regexAcoUUID = '';
                $regexAcoUUIDS = '';

                if ('' != $id) {
                    $regexAcoUUID = 'regex:' . static::$REGEX_UUID . '|exists:' . $table . ',AcoId,DeletedAt,NULL';
                    $regexAcoUUIDS = 'regex:' . static::$REGEX_UUID;
                }

                $acoRules = [
                    'parent_id' => 'bail|' . $regexAcoUUID . '|max:64',
                    'model' => 'bail|max:100|string',
                    'model_id' => 'bail|' . $regexAcoUUIDS . '|max:64',
                    'name' => 'bail|required|max:255|min:3|regex:' . static::$REGEX_ALPHA,
                    'alias' => 'bail|required|unique:' . $table . ',Alias,NULL,AcoId|max:255',
                    'left_node' => 'bail|max:100',
                    'right_node' => 'bail|max:100',
                ];
                break;
            case 'update':
                $acoRules = [
                    'name' => 'bail|required|max:255|min:3|regex:' . static::$REGEX_ALPHA,
                    'alias' => 'bail|unique:' . $table . ',Alias,' . $id . ',AcoId|max:255',
                    'left_node' => 'bail|max:100',
                    'right_node' => 'bail|max:100',
                ];
                break;
        }

        return $acoRules;
    }

    /**
     * Function for validation messages
     *
     * @param action $actionAco action name
     *
     * @name   validationMessage
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _acoValidationMessage($actionAco)
    {
        $acoMessages = [];
        return $acoMessages;
    }

    /**
     * This Function is used to transform user fields to table fields
     *
     * @param Obj    $input           is used to get data sent by client
     * @param action $actionTransform action name
     *
     * @name   _acoTransform
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    private function _acoTransform($input, $actionTransform = 'update')
    {
        $acoData = [];
        $acoData['Name'] = $input['name'];
        $acoData["Etag"] = time();

        if (true == isset($input['left_node'])) {
            $acoData["LeftNode"] = $input['left_node'];
        }

        if (true == isset($input['right_node'])) {
            $acoData["RightNode"] = $input['right_node'];
        }

        if (true == isset($input['model'])) {
            $acoData["Model"] = $input['model'];
        }

        if (true == isset($input['id'])) {
            $acoData["AcoId"] = $input['id'];
        }

        switch ($actionTransform) {
            case 'create':
                $acoData['Alias'] = $input['alias'];
                $acoData["ParentId"] = null;

                if (isset($input['parent_id']) && '' != $input['parent_id']) {
                    $acoData["ParentId"] = $input['parent_id'];
                }

                if (isset($input['model_id']) && '' != $input['model_id']) {
                    $aroData["ModelId"] = $input['model_id'];
                }
                break;

            case 'update':
                if (true == isset($input['alias'])) {
                    $acoData["Alias"] = $input['alias'];
                }
                break;
            default:
                break;
        }
        return $acoData;
    }
}
