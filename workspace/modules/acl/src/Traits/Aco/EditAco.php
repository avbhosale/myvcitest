<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aco;

use Illuminate\Database\QueryException;
use Modules\ACL\Models\ACO;
use Illuminate\Http\Request;

/**
 * ACL Edit Aco module methods
 *
 * @name     EditAco
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait EditAco
{

    /**
     * Show Aco Method
     *
     * @param Obj    $request Request Object
     * @param String $id      Aco ID
     *
     * @name   editAco
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function editAco(Request $request, $id)
    {
        if ('' != $id) {
            try {
                $aco = $this->_acoRepository->findOne($id);

                if (!$aco instanceof ACO) {
                    return $this->errorNotFound("The aco with id {$id} doesn't exist");
                }

                $this->validateRequest($request, $this->_acoValidationRules('update', $id), $this->_acoValidationMessage('update'), $this->_setAcoAttributes(), $this->_setAcoAttributeCode());

                $aco = $this->_acoRepository->update($aco, $this->_acoTransform($request->all()));

                return $this->response->item($aco, $this->_acoTransformer, ['key' => 'ACO']);
            } catch (QueryException $exception) {
                $this->errorInternal();
            }
        } else {
            $this->errorUnauthorized('Invalid Aco Id');
        }
    }
}
