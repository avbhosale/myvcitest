<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aco;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

/**
 * ACL Aco module methods
 *
 * @name     AddAco
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddAco
{

    /**
     * Add Aco Method
     *
     * @param Obj $request Request Object
     *
     * @name   addAco
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addAco(Request $request)
    {
        try {
            $this->validateRequest($request, $this->_acoValidationRules('create'), $this->_acoValidationMessage('create'), $this->_setAcoAttributes(), $this->_setAcoAttributeCode());
            $aco = $this->_acoRepository->save($this->_acoTransform($request->all(), 'create'));
            $aro = $this->_aroRepository->getAro();

            $bulkInsertArr = [];
            $bulkInsertArr = $this->_bulkInsertPermissions($aro, $aco["AcoId"]);
            $this->_aroAcoRepository->bulkInsert($bulkInsertArr);
            return $this->response->item($aco, $this->_acoTransformer, ['key' => 'ACO']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
