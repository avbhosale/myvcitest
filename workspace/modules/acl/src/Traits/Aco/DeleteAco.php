<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aco;

use Illuminate\Database\QueryException;
use Modules\ACL\Models\ACO;

/**
 * ACL Aco module methods
 *
 * @name     DeleteAco
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait DeleteAco
{

    /**
     * Delete Aco Method
     *
     * @param String $id ACO ID
     *
     * @name   deleteAco
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function deleteAco($id)
    {
        try {
            $aco = $this->_acoRepository->findOne($id);
            $acoId = $aco["AcoId"];
            $acoChild = $this->_acoRepository->findChild($acoId);

            if (!empty($acoChild)) {
                return $this->errorNotFound("Unable to delete permission as it have child permissions.");
            }

            if (!$aco instanceof ACO) {
                return $this->errorNotFound("This entity with {$id} doesn't exist");
            }

            if ($this->_acoRepository->forceDelete($aco)) {
                $column = 'AcoId';
                $aroAco = $this->_aroAcoRepository->deleteAroAcoById($column, $acoId);
                return $this->response->array(['id' => $id, 'status' => 'deleted']);
            } else {
                return $this->response->errorBadRequest();
            }
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
