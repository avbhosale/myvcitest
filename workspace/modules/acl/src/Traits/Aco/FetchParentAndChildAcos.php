<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aco;

/**
 * ACL Aco module methods
 *
 * @name     ShowAco
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait FetchParentAndChildAcos
{

    public $arrobjParentAcos = [];
    public $arrobjChildAcos = [];

    /**
     * Get All parent ACOs by given ACO ID (response includes given ACO ID)
     *
     * @param String $strAcoId Aco ID
     *
     * @name   getAllParentAcosByAcoId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function getAllParentAcosByAcoId($strAcoId)
    {
        $objAco = $this->_acoRepository->findOne($strAcoId);

        if (!is_null($objAco) && !is_null($objAco->ParentId)) {
            array_push($this->arrobjParentAcos, $objAco);
            $this->getAllParentAcosByAcoId($objAco->ParentId);
        } else {
            array_push($this->arrobjParentAcos, $objAco);
        }
    }

    /**
     * Get All child ACOs by given ACO ID
     *
     * @param String $strAcoId Aco ID
     *
     * @name   getAllChildAcosByAcoId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function getAllChildAcosByAcoId($strAcoId)
    {
        $arrobjAcos = $this->_acoRepository->findBy(['ParentId' => $strAcoId]);

        if (!empty($arrobjAcos->all())) {
            foreach ($arrobjAcos->all() as $objAco) {
                array_push($this->arrobjChildAcos, $objAco);
                $this->getAllChildAcosByAcoId($objAco->AcoId);
            }
        }
    }
}
