<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aro;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

/**
 * ACL Aro module methods
 *
 * @name     AddAro
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddAro
{

    /**
     * Add Aro Method
     *
     * @param Obj $request Request Object
     *
     * @name   addAro
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addAro(Request $request)
    {
        try {
            $intParentId = (!isset($request->parent_id)) ? '' : $request->parent_id;
            $intModelId = (!isset($request->model_id)) ? '' : $request->model_id;
            $this->validateRequest($request, $this->_aroValidationRules('create', $intParentId, $intModelId), $this->_aroValidationMessage('create'), $this->_setAroAttributes(), $this->_setAroAttributeCode());

            $aro = $this->_aroRepository->save($this->_aroTransform($request->all(), 'create'));

            $aco = $this->_acoRepository->getAco();
            $aro = $this->_aroRepository->findOne($aro["AroId"]);
            $column = 'AroId';
            $aroAco = $this->_aroAcoRepository->getAroAcobyAroid($column, $request->parent_id);

            $bulkInsert = [];
            $bulkInsert = (0 < count($aroAco)) ? $this->_bulkInsertAroAco($aroAco, $aro["AroId"], $noAccess = 0) : $this->_bulkInsertAroAco($aco, $aro["AroId"], $noAccess = 1);
            $this->_aroAcoRepository->bulkInsert($bulkInsert);

            return $this->response->item($aro, $this->_aroTransformer, ['key' => 'ARO']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
