<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aro;

use Modules\ACL\Models\ARO;

/**
 * ACL module methods
 *
 * @name     AroAttributes
 * @category Attribute
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AroAttributes
{

    /**
     * This function set attributes for ARO validation messages
     *
     * @name   _setAroAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAroAttributes()
    {
        $aroAttributes = [];
        $aroAttributes = [
            'AroId' => 'ID',
            'Name' => 'Name',
            'ParentId' => 'Parent Id',
            'Model' => 'Model',
            'Alias' => 'Alias',
            'Etag' => 'ETag',
        ];

        return $aroAttributes;
    }

    /**
     * This Function set attributes for validation messages
     *
     * @name   _setAroAttributeCode
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAroAttributeCode()
    {
        $assignAro = new ARO();
        return $assignAro->aroErrorCodes;
    }
}
