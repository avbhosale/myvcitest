<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aro;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\ACL\Transformers\AROTransformer;

/**
 * ACL Aro module methods
 *
 * @name     ListAro
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ListAro
{

    /**
     * List Aro Method
     *
     * @param Obj $request Request Object
     *
     * @name   listAro
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function listAro(Request $request)
    {
        try {
            $aros = $this->_aroRepository->all($request->all());
            $aros = collect($aros);
            $response = $this->response()->collection($aros, new AROTransformer, ['key' => 'ARO']);

            return $response;
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
