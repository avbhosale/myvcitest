<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Traits\Aro;

/**
 * ACL module Aro Validation methods
 *
 * @name     AroValidator
 * @category Trait
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AroValidator
{

    /**
     * Function to set validation rules
     *
     * @param string  $aroAction action name
     * @param integer $id        aro Id
     * @param integer $modelId   model Id
     *
     * @name   _aroValidationRules
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _aroValidationRules($aroAction, $id = null, $modelId = null)
    {
        $table = config('database.default') . '.' . $this->_aroRepository->getTableName();

        $rules = [];
        switch ($aroAction) {
            case 'create':
                $regexUUID = '';
                $regexUUIDS = '';

                if ('' != $id) {
                    $regexUUID = 'regex:' . static::$REGEX_UUID . '|exists:' . $table . ',AroId,DeletedAt,NULL';
                }

                if ('' != $modelId) {
                    $regexUUIDS = 'regex:' . static::$REGEX_UUID;
                }

                $rules = [
                    'parent_id' => 'bail|' . $regexUUID . '|max:64',
                    'model' => 'bail|required|string|max:100',
                    'model_id' => 'bail|' . $regexUUIDS . '|max:64',
                    'name' => 'bail|required|regex:' . static::$REGEX_ALPHA . '|max:255',
                    'alias' => 'bail|required|unique:' . $table . ',Alias,NULL,AroId|max:255',
                    'left_node' => 'bail|max:100',
                    'right_node' => 'bail|max:100',
                ];
                break;
            case 'update':
                $rules = [
                    'model' => 'bail|required|string|max:100',
                    'name' => 'bail|required|regex:' . static::$REGEX_ALPHA . '|max:255',
                    'alias' => 'bail|unique:' . $table . ',Alias,' . $id . ',AroId|max:255',
                    'left_node' => 'bail|max:100',
                    'right_node' => 'bail|max:100',
                ];
                break;
        }

        return $rules;
    }

    /**
     * Function for validation messages
     *
     * @param action $actionAro action name
     *
     * @name   validationMessage
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _aroValidationMessage($actionAro)
    {
        $aroMessages = [];
        return $aroMessages;
    }

    /**
     * This Function is used to transform user fields to table fields
     *
     * @param Obj    $input           is used to get data sent by client
     * @param action $transformAction action name
     *
     * @name   _aroTransform
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    private function _aroTransform($input, $transformAction = 'update')
    {
        $aroData = [];
        $aroData['Model'] = $input['model'];
        $aroData['Name'] = $input['name'];

        if (true == isset($input['model_id'])) {
            $aroData["ModelId"] = $input['model_id'];
        }

        if (true == isset($input['id'])) {
            $aroData["AroId"] = $input['id'];
        }

        if (true == isset($input['left_node'])) {
            $aroData["LeftNode"] = $input['left_node'];
        }

        if (true == isset($input['right_node'])) {
            $aroData["RightNode"] = $input['right_node'];
        }

        $aroData["Etag"] = time();

        switch ($transformAction) {
            case 'create':
                $aroData['Alias'] = $input['alias'];
                $aroData["ParentId"] = null;

                if (isset($input['parent_id']) && '' != $input['parent_id']) {
                    $aroData["ParentId"] = $input['parent_id'];
                }
                break;

            case 'update':
                if (true == isset($input['alias'])) {
                    $aroData["Alias"] = $input['alias'];
                }
                break;
            default:
                break;
        }
        return $aroData;
    }
}
