<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Route
 * @package  Authentication
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
$api = app('Dingo\Api\Routing\Router');

$defaultApiVersion = config('api.version');
$api->version($defaultApiVersion, function ($api) {
    $limit = config('api.throttling.limit');
    $expires = config('api.throttling.expires');
    $api->group(['namespace' => '\Modules\ACL\Controllers', 'middleware' => 'api.throttle', 'limit' => $limit, 'expires' => $expires], function () use ($api) {
        // Aco api
        $api->get('acos', ['uses' => 'ACOController@index',]);
        $api->get('acos/{id}', ['uses' => 'ACOController@show',]);
        $api->post('acos', ['uses' => 'ACOController@store',]);
        $api->put('acos/{id}', ['uses' => 'ACOController@update',]);
        $api->delete('acos/{id}', ['uses' => 'ACOController@destroy',]);

        // Aro api
        $api->get('aros', ['uses' => 'AROController@index',]);
        $api->get('aros/{id}', ['uses' => 'AROController@show',]);
        $api->post('aros', ['uses' => 'AROController@store',]);
        $api->put('aros/{id}', ['uses' => 'AROController@update',]);
        $api->delete('aros/{id}', ['uses' => 'AROController@destroy',]);

        // AroAco api
        $api->get('aroacos/aro/{id}', ['uses' => 'AROACOController@getAroAcobyAroid',]);
        $api->put('aroacos/mapping/{id}', ['uses' => 'AROACOController@bulkUpdate',]);
        $api->post('has/access', ['uses' => 'AROACOController@hasAccess',]);
        $api->get('permissions/user/{id}', ['uses' => 'AROACOController@getPermissionByUserId',]);
    });
});
