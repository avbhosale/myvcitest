<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Transformer
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Transformers;

use Modules\ACL\Models\AROACO;
use League\Fractal\TransformerAbstract;

/**
 * Class to manage Transformer for AROACO table
 *
 * @name     AROACOTransformer
 * @category Transformer
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AROACOTransformer extends TransformerAbstract
{

    /**
     * Function to set transform format
     *
     * @param Model $aroAco model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(AROACO $aroAco)
    {
        $formattedAROACO = [
            'id' => $aroAco->AroAcoId,
            'aro_id' => $aroAco->AroId,
            'aco_id' => $aroAco->AcoId,
            'access' => $aroAco->Access,
            'etag' => $aroAco->Etag,
            'deleted_at' => $aroAco->DeletedAt
        ];

        return $formattedAROACO;
    }
}
