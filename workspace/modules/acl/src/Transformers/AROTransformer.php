<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Transformer
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */

namespace Modules\ACL\Transformers;

use Modules\ACL\Models\ARO;
use League\Fractal\TransformerAbstract;

/**
 * Class to manage Transformer for ARO table
 *
 * @name     AROTransformer
 * @category Transformer
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AROTransformer extends TransformerAbstract
{

    /**
     * Function to set transform format
     *
     * @param Model $aro model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(ARO $aro)
    {
        $formattedARO = [
            'id'         => isset($aro->id) ? $aro->id : $aro->AroId,
            'parent_id'  => $aro->parent_id,
            'model'      => $aro->model,
            'model_id'   => $aro->model_id,
            'name'       => $aro->name,
            'alias'      => $aro->alias,
            'left_node'  => $aro->left_node,
            'right_node' => $aro->right_node,
            'etag'       => $aro->etag,
            'deleted_at' => $aro->deleted_at,
            'child'      => $aro->child,
        ];

        return $formattedARO;
    }
}
