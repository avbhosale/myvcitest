<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Transformer
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Transformers;

use Modules\ACL\Models\ACO;
use League\Fractal\TransformerAbstract;

/**
 * Class to manage Transformer for ACO table
 *
 * @name     ACOTransformer
 * @category Transformer
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class ACOTransformer extends TransformerAbstract
{

    /**
     * Function to set transform format
     *
     * @param Model $aco model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(ACO $aco)
    {
        $formattedACO = [
            'id' => isset($aco->id) ? $aco->id : $aco->AcoId,
            'parent_id' => isset($aco->parent_id) ? $aco->parent_id : $aco->ParentId,
            'model' => isset($aco->model) ? $aco->model : $aco->Model,
            'model_id' => isset($aco->model_id) ? $aco->model_id : $aco->ModelId,
            'name' => isset($aco->name) ? $aco->name : $aco->Name,
            'alias' => isset($aco->alias) ? $aco->alias : $aco->Alias,
            'left_node' => isset($aco->left_node) ? $aco->left_node : $aco->LeftNode,
            'right_node' => isset($aco->right_node) ? $aco->right_node : $aco->RightNode,
            'etag' => isset($aco->etag) ? $aco->etag : $aco->Etag,
            'deleted_at' => isset($aco->deleted_at) ? $aco->deleted_at : $aco->DeletedAt,
            'child' => isset($aco->child) ? $aco->child : $aco->Child,
        ];

        return $formattedACO;
    }
}
