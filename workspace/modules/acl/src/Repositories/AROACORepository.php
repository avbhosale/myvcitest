<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category AroAco
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Repositories;

use Illuminate\Database\Eloquent\Model;
use Modules\ACL\Repositories\Contracts\AROACOInterface;
use Modules\ACL\Models\AROACO;

/**
 * Repository class for AROACO model
 *
 * @name     AROACORepository
 * @category AROACO
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AROACORepository implements AROACOInterface
{

    protected $model;

    /**
     * Default constructor method of class
     *
     * @param Obj $aroAco object of model class
     *
     * @name   constructor
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(AROACO $aroAco)
    {
        $this->model = $aroAco;
    }

    /**
     * Find all resources using pagination
     *
     * @param array $searchCriteria array of search criteria
     *
     * @name   all
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function all(array $searchCriteria)
    {
    }

    /**
     * Save a resource
     *
     * @param array $data array of values
     *
     * @name   save
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function save(array $data)
    {
    }

    /**
     * Update resource
     *
     * @param Obj   $aroAco object of model class
     * @param array $data   array of data
     *
     * @name   update
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function update(Model $aroAco, array $data)
    {
    }

    /**
     * Find a resource
     *
     * @param array $searchCriteria array of resource data
     *
     * @name   findBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findBy(array $searchCriteria = [])
    {
        return $this->model->where($searchCriteria)->get();
    }

    /**
     * Find a resource by id
     *
     * @param integer $id id of resource
     *
     * @name   findOne
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOne($id)
    {
    }

    /**
     * Delete a resource
     *
     * @param Model $aroAco model
     *
     * @name   delete
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function delete(Model $aroAco)
    {
    }

    /**
     * Search All resources by any values of a key
     *
     * @param string $key    key
     * @param array  $values array
     *
     * @name   findIn
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findIn($key, array $values)
    {
        return $this->model->whereIn($key, $values)->get();
    }

    /**
     * Search All resources by any values of a multiple keys
     *
     * @param string $strWhereInKey Column name as key to find in whereIn clause
     * @param array  $arrValues     Array of values for whereIn clause
     * @param string $strWhereKey   Column name as key to find in where clause
     * @param string $value         value for where clause
     *
     * @name   findWhereIn
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function findWhereIn($strWhereInKey, $arrValues, $strWhereKey, $value)
    {
        return $this->model->whereIn($strWhereInKey, $arrValues)->where($strWhereKey, '=', $value)->get();
    }

    /**
     * Find a AroAco permissions group by AroId
     *
     * @param string  $column name of table
     * @param integer $aroId  id of resource
     *
     * @name   getAroAcobyAroid
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function getAroAcobyAroid($column, $aroId)
    {
        return $this->model->where($column, "=", $aroId)->get();
    }

    /**
     * Save a resource using bulk insert
     *
     * @param array $data array of values
     *
     * @name   bulkInsert
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function bulkInsert(array $data)
    {
        return $this->model->insert($data);
    }

    /**
     * Bulk Update Access Permissions for user having multiple roles
     *
     * @param array $data is array of request
     * @param array $ids  is array of user AroAcoId
     *
     * @name   bulkUpdate
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return obj
     */
    public function bulkUpdate(array $data, $ids = [])
    {
        return $this->model->whereIn('AroAcoId', $ids)->update($data);
    }

    /**
     * Delete a AroAco table records if User/Role/Permission is deleted from ARO or ACO for specific entity
     *
     * @param integer $column name of table
     * @param integer $aroId  id   of resource
     *
     * @name   deleteAroAcobyId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function deleteAroAcoById($column, $aroId)
    {
        return $this->model->where($column, "=", $aroId)->delete();
    }

    /**
     * Bulk Delete AroAco table records if User is deleted from Active Directory
     *
     * @param integer $column name of table
     * @param array   $ids    of AroId
     *
     * @name   bulkDeleteAroAcoById
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return obj
     */
    public function bulkDeleteAroAcoById($column, $ids = [])
    {
        return $this->model->whereIn($column, $ids)->delete();
    }

    /**
     * Check whether user has permission to access the given ACO
     *
     * @param String $strAcoId  ACO ID
     * @param String $arrAroIds array ARO IDs
     *
     * @name   getPermissionByUserIdByAcoId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return obj
     */
    public function getPermissionByAcoIdByAroIds($strAcoId, $arrAroIds)
    {
        return $this->model->whereIn('AroId', $arrAroIds)->where('AcoId', $strAcoId)->get();
    }
}
