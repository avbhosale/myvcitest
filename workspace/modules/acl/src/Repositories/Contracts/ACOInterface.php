<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Interface
 * @package  ACO
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Repositories\Contracts;

use Modules\Infrastructure\Interfaces\BaseInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Two line description of class
 *
 * @name     ACOInterface
 * @category Interface
 * @package  ACO
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
interface ACOInterface extends BaseInterface
{

    /**
     * Description
     *
     * @param string $parentId is used to get data sent by client
     *
     * @name   getParentChildAcoArray
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function getParentChildAcoArray($parentId);

    /**
     * Get model table name
     *
     * @name   getTableName
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return string
     */
    public function getTableName();

    /**
     * Get child entity by parent id
     *
     * @param Obj $parentId is used to get child
     *
     * @name   findChild
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function findChild($parentId);

    /**
     * Get all ACO list with AcoId and Name
     *
     * @name   getAco
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function getAco();

    /**
     * Force Delete a resource
     *
     * @param Model $model model
     *
     * @name   forceDelete
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function forceDelete(Model $model);
}
