<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Interface
 * @package  ARO
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Repositories\Contracts;

use Modules\Infrastructure\Interfaces\BaseInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Two line description of class
 *
 * @name     AROInterface
 * @category Interface
 * @package  ARO
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
interface AROInterface extends BaseInterface
{

    /**
     * Find Single Entity by Specified Column with Id to apply search criteria using any column
     *
     * @param string $column column name
     * @param Obj    $id     is used to get child
     *
     * @name   findOneById
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function findOneById($column, $id);

    /**
     * Description
     *
     * @param string $parentId is used to get data sent by client
     *
     * @name   getParentChildAroArray
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function getParentChildAroArray($parentId);

    /**
     * Get model table name
     *
     * @name   getTableName
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return string
     */
    public function getTableName();

    /**
     * Find Multiple Entities by Specified Column with Id to apply search criteria using any column
     *
     * @param string $column column name
     * @param Obj    $id     is used to get all entity by id
     *
     * @name   findAroById
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function findAroById($column, $id);

    /**
     * Get all ARO list with AroId and Name
     *
     * @name   getAro
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function getAro();

    /**
     * Delete entity by a specified Column using Id
     *
     * @param String  $column column name
     * @param Integer $id     is used to get all entity by id
     *
     * @name   deleteById
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function deleteById($column, $id);
}
