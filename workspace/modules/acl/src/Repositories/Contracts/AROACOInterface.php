<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Interface
 * @package  AROACO
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Repositories\Contracts;

use Modules\Infrastructure\Interfaces\BaseInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Two line description of class
 *
 * @name     AROACOInterface
 * @category Interface
 * @package  AROACO
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
interface AROACOInterface extends BaseInterface
{

    /**
     * Get model table name
     *
     * @param integer $column name of table
     * @param integer $aroId  id   of resource
     *
     * @name   getAroAcobyAroid
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return string
     */
    public function getAroAcobyAroid($column, $aroId);

    /**
     * Save a resource using bulk insert
     *
     * @param array $data array of values
     *
     * @name   bulkInsert
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function bulkInsert(array $data);

    /**
     * Delete a AroAco table records if User/Role/Permission is deleted from ARO or ACO for specific entity
     *
     * @param integer $column name of table
     * @param integer $aroId  id   of resource
     *
     * @name   deleteAroAcobyId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function deleteAroAcoById($column, $aroId);

    /**
     * Search All resources by any values of a multiple keys
     *
     * @param string $strWhereInKey Column name as key to find in whereIn clause
     * @param array  $arrValues     Array of values for whereIn clause
     * @param string $strWhereKey   Column name as key to find in where clause
     * @param string $value         value for where clause
     *
     * @name   findWhereIn
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findWhereIn($strWhereInKey, $arrValues, $strWhereKey, $value);

    /**
     * Bulk Update Access Permissions for user having multiple roles
     *
     * @param array $data is array of request
     * @param array $ids  is array of user AroAcoId
     *
     * @name   bulkUpdate
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function bulkUpdate(array $data, $ids = []);

    /**
     * Bulk Delete AroAco table records if User is deleted from Active Directory
     *
     * @param integer $column name of table
     * @param array   $ids    of AroId
     *
     * @name   bulkDeleteAroAcoById
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function bulkDeleteAroAcoById($column, $ids = []);

    /**
     * Check whether user has permission to access the given ACO
     *
     * @param String $strAcoId  ACO ID
     * @param String $arrAroIds array ARO IDs
     *
     * @name   getPermissionByUserIdByAcoId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return obj
     */
    public function getPermissionByAcoIdByAroIds($strAcoId, $arrAroIds);
}
