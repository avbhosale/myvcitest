<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category ACL
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Repositories;

use Illuminate\Database\Eloquent\Model;
use Modules\ACL\Repositories\Contracts\AROInterface;
use Modules\ACL\Models\ARO;

/**
 * Repository class for ARO model
 *
 * @name     ARORepository
 * @category ARO
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class ARORepository implements AROInterface
{

    protected $model;

    /**
     * Default constructor method of class
     *
     * @param Obj $aro object of model class
     *
     * @name   constructor
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(ARO $aro)
    {
        $this->model = $aro;
    }

    /**
     * Find all resources using pagination
     *
     * @param array $searchCriteria array of search criteria
     *
     * @name   all
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function all(array $searchCriteria)
    {
        $tree = $this->getParentChildAroArray(null);

        return $queryBuilder = $tree;
    }

    /**
     * Save a resource
     *
     * @param array $data array of values
     *
     * @name   save
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function save(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update resource
     *
     * @param Obj   $aro  object of model class
     * @param array $data array of data
     *
     * @name   update
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function update(Model $aro, array $data)
    {
        $fillAbleProperties = $aro->getFillable();


        foreach ($data as $key => $value) {
            if (in_array($key, $fillAbleProperties)) {
                $aro->$key = $value;
            }
        }

        $aro->save();

        $updateACL = $this->findOne($aro->AroId);
        return $updateACL;
    }

    /**
     * Update resource
     *
     * @param array $strUserName User Name to update
     * @param array $strUserId   User Id
     *
     * @name   updateUserNameByModelId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function updateUserNameByModelId($strUserName, $strUserId)
    {
        return $this->model->where('ModelId', $strUserId)->update(array('Name' => $strUserName));
    }

    /**
     * Description
     *
     * @param array $searchCriteria array of resource data
     *
     * @name   findBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findBy(array $searchCriteria = [])
    {
        return $this->model->where($searchCriteria)->get();
    }

    /**
     * Find a resource by id
     *
     * @param integer $id id of resource
     *
     * @name   findOne
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOne($id)
    {
        return $this->findOneBy(['AroId' => $id]);
    }

    /**
     * Delete a resource
     *
     * @param Model $aro model
     *
     * @name   delete
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function delete(Model $aro)
    {
        return $aro->delete();
    }

    /**
     * Find a resource by criteria
     *
     * @param array $criteria array of search criteria
     *
     * @name   findOneBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOneBy(array $criteria)
    {
        return $this->model->select('AroId', 'ParentId as parent_id', 'Model as model', 'ModelId as model_id', 'Alias as alias', 'Name as name', 'LeftNode as left_node', 'RightNode as right_node', 'Etag as etag', 'DeletedAt as deleted_at')->where($criteria)->first();
    }

    /**
     * Function is used to get aro tree
     *
     * @param string $parentId is used to get data sent by client
     *
     * @name   getParentChildAroArray
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function getParentChildAroArray($parentId)
    {
        $arrAro = [];
        $thisLevel = [];

        //get child permissions
        $arrAro = $this->model->select('AroId as id', 'ParentId as parent_id', 'Model as model', 'ModelId as model_id', 'Alias as alias', 'Name as name', 'LeftNode as left_node', 'RightNode as right_node', 'Etag as etag', 'DeletedAt as deleted_at')->where('ParentId', '=', $parentId)->orderBy('etag', 'desc')->get();

        //prepare tree
        if (0 < count($arrAro)) {
            foreach ($arrAro as $aro) {
                $thisLevel[$aro->id] = $aro;
                $arrData = $this->getParentChildAroArray($aro->id);

                $thisLevel[$aro->id]['child'] = $arrData;
            }
        }
        return $thisLevel;
    }

    /**
     * Get model table name
     *
     * @name   getTableName
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->model->getTable();
    }

    /**
     * Find Entity by Specified Column with Id to apply search criteria using any column
     *
     * @param string $column column name
     * @param Obj    $id     is used to get child
     *
     * @name   findOneById
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOneById($column, $id)
    {
        return $this->findOneBy([$column => $id]);
    }

    /**
     * Find All Entities by Specified Column with Id to apply search criteria using any column
     *
     * @param String  $column column name
     * @param Integer $id     is used to get all entity by id
     *
     * @name   findAroById
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function findAroById($column, $id)
    {
        return $this->model->select('AroId', 'Name', 'ModelId')->where($column, $id)->get();
    }

    /**
     * Get all ARO list with AroId and Name
     *
     * @name   getAro
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function getAro()
    {
        return $this->model->get();
    }

    /**
     * Delete entity by a specified Column using Id
     *
     * @param String  $column column name
     * @param Integer $id     is used to get all entity by id
     *
     * @name   deleteById
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function deleteById($column, $id)
    {
        return $this->model->where($column, '=', $id)->delete();
    }
}
