<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category ACL
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Repositories;

use Illuminate\Database\Eloquent\Model;
use Modules\ACL\Repositories\Contracts\ACOInterface;
use Modules\ACL\Models\ACO;

/**
 * Repository class for ACO model
 *
 * @name     ACORepository
 * @category ACO
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class ACORepository implements ACOInterface
{

    protected $model;

    /**
     * Default constructor method of class
     *
     * @param Obj $aco object of model class
     *
     * @name   constructor
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(ACO $aco)
    {
        $this->model = $aco;
    }

    /**
     * Find all resources using pagination
     *
     * @param array $searchCriteria array of search criteria
     *
     * @name   all
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function all(array $searchCriteria)
    {

        $tree = $this->getParentChildAcoArray(null);

        return $queryBuilder = $tree;
    }

    /**
     * Save a resource
     *
     * @param array $data array of values
     *
     * @name   save
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function save(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update resource
     *
     * @param Obj   $aco  object of model class
     * @param array $data array of data
     *
     * @name   update
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function update(Model $aco, array $data)
    {
        $fillAbleProperties = $aco->getFillable();


        foreach ($data as $key => $value) {
            if (in_array($key, $fillAbleProperties)) {
                $aco->$key = $value;
            }
        }

        $aco->save();

        $updateACL = $this->findOne($aco->AcoId);
        return $updateACL;
    }

    /**
     * Description
     *
     * @param array $searchCriteria array of resource data
     *
     * @name   findBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findBy(array $searchCriteria = [])
    {
        return $this->model->where($searchCriteria)->get();
    }

    /**
     * Find a resource by id
     *
     * @param integer $id id of resource
     *
     * @name   findOne
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOne($id)
    {
        return $this->findOneBy(['AcoId' => $id]);
    }

    /**
     * Delete a resource
     *
     * @param Model $aco model
     *
     * @name   delete
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function delete(Model $aco)
    {
        return $aco->delete();
    }

    /**
     * Force Delete a resource
     *
     * @param Model $aco model
     *
     * @name   forceDelete
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function forceDelete(Model $aco)
    {
        return $aco->forceDelete();
    }

    /**
     * Find a resource by criteria
     *
     * @param array $criteria array of search criteria
     *
     * @name   findOneBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOneBy(array $criteria)
    {
        return $this->model->where($criteria)->first();
    }

    /**
     * Search All resources by any values of a key
     *
     * @param string $key    key
     * @param array  $values array
     *
     * @name   findIn
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findIn($key, array $values)
    {
        return $this->model->whereIn($key, $values)->get();
    }

    /**
     * Function is used to get aco tree
     *
     * @param string $parentId is used to get data sent by client
     *
     * @name   getParentChildAcoArray
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function getParentChildAcoArray($parentId)
    {
        $arrAco = [];
        $thisLevel = [];

        //get child permissions
        $arrAco = $this->model->select('AcoId as id', 'ParentId as parent_id', 'Model as model', 'ModelId as model_id', 'Alias as alias', 'Name as name', 'LeftNode as left_node', 'RightNode as right_node', 'Etag as etag', 'DeletedAt as deleted_at')->where('ParentId', '=', $parentId)->orderBy('etag', 'desc')->get();

        //prepare tree
        if (0 < count($arrAco)) {
            foreach ($arrAco as $aco) {
                $thisLevel[$aco->id] = $aco;
                $arrData = $this->getParentChildAcoArray($aco->id);

                $thisLevel[$aco->id]['child'] = $arrData;
            }
        }

        return $thisLevel;
    }

    /**
     * Get model table name
     *
     * @name   getTableName
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->model->getTable();
    }

    /**
     * Find a child resources by parent id
     *
     * @param integer $parentId id of resource
     *
     * @name   findChild
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findChild($parentId)
    {
        return $this->findOneBy(['ParentId' => $parentId]);
    }

    /**
     * Get all ACO list with AcoId and Name
     *
     * @name   getAco
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function getAco()
    {
        return $this->model->get();
    }
}
