<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category UnitTestCase
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Tests\AroTest;

use Modules\ACL\Models\ARO;
use Modules\ACL\Traits\Aro\AddAro;
use Modules\ACL\Traits\Aro\AroAttributes;
use Modules\ACL\Traits\Aro\AroValidator;
use Modules\ACL\Traits\Aro\DeleteAro;
use Modules\ACL\Traits\Aro\EditAro;
use Modules\ACL\Traits\Aro\ListAro;
use Modules\ACL\Traits\Aro\ShowAro;
use TestCase;

/**
 * Class for test cases
 *
 * @name     AroTest
 * @category TestCase
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AroTest extends TestCase
{

    use AroAttributes;
    use AroValidator;
    use AddAro;
    use EditAro;
    use DeleteAro;
    use ShowAro;
    use ListAro;

    /**
     * Test CRUD operation on API
     *
     * @name   testCRUDAroApi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDAroApi()
    {

        $rootAroId = null;

        $arrFakeAro = factory(ARO::class)->make()->toArray();
        $arrFakeAro["parent_id"] = $rootAroId;
        $arrFakeAro['model'] = 'Role';
        $arrFakeAro['model_id'] = null;

        // add aro Role
        $response = $this->call('POST', 'aros', $arrFakeAro);
        $this->refreshApplication();

        $this->assertEquals(200, $response->status());

        $intAroId = json_decode($response->getContent())->data->id;

        // list aro
        $responseList = $this->call('GET', 'aros');
        $this->refreshApplication();

        $this->assertEquals(200, $responseList->status());

        // show aro by ID
        $responseShow = $this->call('GET', 'aros/' . $intAroId);
        $this->refreshApplication();

        $this->assertEquals(200, $responseShow->status());

        // edit aro by ID
        $arrFakeAro = factory(ARO::class)->make()->toArray();
        $arrFakeAro["parent_id"] = $rootAroId;
        $arrFakeAro['model'] = 'Role';
        $arrFakeAro['model_id'] = null;

        $responsePut = $this->call('PUT', 'aros/' . $intAroId, $arrFakeAro);
        $this->refreshApplication();

        $this->assertEquals(200, $responsePut->status());


        // add aro User
        $response = $this->call('GET', 'users');
        $this->refreshApplication();

        $userData = json_decode($response->getContent())->data;

        if (!empty($userData) && 0 < count($userData)) {
            $firstUser = $userData[0];

            $arrFakeAro = factory(ARO::class)->make()->toArray();
            $arrFakeAro['model'] = 'User';
            $arrFakeAro['model_id'] = $firstUser->id;
            $arrFakeAro["parent_id"] = null;

            $response = $this->call('POST', 'aros', $arrFakeAro);
            $this->refreshApplication();

            $this->assertEquals(200, $response->status());
        }
    }

    /**
     * Test CRUD operation on API for failure
     *
     * @name   testCRUDAroFail
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDAroFail()
    {
        $arrFakeAro = [];

        // add aro
        $addResponse = $this->call('POST', 'aros', $arrFakeAro);
        $this->refreshApplication();
        $this->assertEquals(500, $addResponse->status());
        $this->refreshApplication();


        $intAroId = '';

        //edit aro
        $editResponse = $this->call('PUT', 'aros/' . $intAroId, $arrFakeAro);
        $this->refreshApplication();

        $this->assertEquals(405, $editResponse->status());

        $intAroId = 'ahksdgaksdgh';

        // show aro by ID
        $responseShow = $this->call('GET', 'aros/' . $intAroId);
        $this->refreshApplication();

        $this->assertEquals(500, $responseShow->status());

        //delete aro by ID
        $deleteResponse = $this->call('DELETE', 'aros/' . $intAroId);
        $this->refreshApplication();

        $this->assertEquals(500, $deleteResponse->status());
        $this->refreshApplication();
    }
}
