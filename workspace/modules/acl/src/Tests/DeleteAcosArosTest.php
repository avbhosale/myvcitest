<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category UnitTestCase
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Tests\DeleteAcosArosTest;

use Modules\ACL\Models\ACO;
use Modules\ACL\Models\ARO;
use TestCase;

/**
 * Class for test cases
 *
 * @name     AroAcoTest
 * @category TestCase
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class DeleteAcosArosTest extends TestCase
{

    protected $acoModel;
    protected $aroModel;

    /**
     * Setup repository
     *
     * @name   setup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function setup()
    {
        parent::setUp();
        $this->acoModel = new ACO();
        $this->aroModel = new ARO();
    }

    /**
     * Delete ACO and ARO
     *
     * @name   deleteAcoAndAro
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testDeleteAcoAndAro()
    {
        // delete aco by ID
        $responseAcoList = $this->call('GET', 'acos');
        $this->refreshApplication();
        $acoList = json_decode($responseAcoList->getContent())->data;

        if (!empty($acoList)) {
            foreach ($acoList as $objTempAco) {
                $responseDelete = $this->call('DELETE', 'acos/' . $objTempAco->id);
                $this->refreshApplication();

                $this->assertEquals(200, $responseDelete->status());

                $objAco = $this->acoModel::withTrashed()->find($objTempAco->id);

                if (!is_null($objAco)) {
                    $objAco->forceDelete();
                }
            }
        }


        //delete aro by ID
        $responseAroList = $this->call('GET', 'aros');
        $aroList = json_decode($responseAroList->getContent())->data;

        $this->refreshApplication();

        if (!empty($aroList)) {
            foreach ($aroList as $objTempAro) {
                $responseDelete = $this->call('DELETE', 'aros/' . $objTempAro->id);

                $this->assertEquals($responseDelete->status(), $responseDelete->status());

                $this->refreshApplication();

                $objAro = $this->aroModel::withTrashed()->find($objTempAro->id);

                $objAro->forceDelete();
            }
        }
    }
}
