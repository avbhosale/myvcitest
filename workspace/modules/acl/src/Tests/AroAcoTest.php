<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category UnitTestCase
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Tests\AroAcoTest;

use Modules\ACL\Traits\Aco\FetchParentAndChildAcos;
use Modules\ACL\Traits\AroAco\AroAcoAttributes;
use Modules\ACL\Traits\AroAco\AroAcoValidator;
use Modules\ACL\Traits\AroAco\CheckPermission;
use Modules\ACL\Traits\AroAco\FetchUserPermissions;
use Modules\ACL\Traits\AroAco\ShowAroAcoByAroId;
use Webpatser\Uuid\Uuid;
use TestCase;

/**
 * Class for test cases
 *
 * @name     AroAcoTest
 * @category TestCase
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AroAcoTest extends TestCase
{

    use AroAcoAttributes;
    use AroAcoValidator;
    use FetchParentAndChildAcos;
    use ShowAroAcoByAroId;
    use CheckPermission;
    use FetchUserPermissions;

    /**
     * Test CRUD operation on API
     *
     * @name   testCRUDAroAcoApi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDAroAcoApi()
    {
        $responseAroList = $this->call('GET', 'aros');
        $aroList = json_decode($responseAroList->getContent())->data;
        $this->refreshApplication();

        $responseAcoList = $this->call('GET', 'acos');
        $acoList = json_decode($responseAcoList->getContent())->data;
        $objAco = $acoList[0];
        $this->refreshApplication();

        if (!empty($aroList)) {
            foreach ($aroList as $objAro) {
                // Get permision list by aro ID
                $permissionList = $this->call('GET', 'aroacos/aro/' . $objAro->id);
                $this->refreshApplication();
                $this->assertEquals(200, $permissionList->getStatusCode());

                // check for access 0
                //update permissions to 0
                $arrMappingBody['aco_id'] = $objAco->id;
                $arrMappingBody['access'] = 0;

                $updateMapping = $this->call('PUT', 'aroacos/mapping/' . $objAro->id, $arrMappingBody);
                $this->refreshApplication();
                $this->assertEquals(200, $updateMapping->getStatusCode());
                if (!is_null($objAro->attributes->model_id) && '' != $objAro->attributes->model_id) {
                    $updateMapping = $this->call('PUT', 'aroacos/mapping/' . $objAro->attributes->model_id, $arrMappingBody);
                    $this->refreshApplication();
                    $this->assertEquals(200, $updateMapping->getStatusCode());

                    // Get permision list by user ID
                    $permissionList = $this->call('GET', 'aroacos/aro/' . $objAro->attributes->model_id);
                    $this->refreshApplication();
                    $this->assertEquals(200, $permissionList->getStatusCode());

                    $arrhasAccessBody['user_id'] = $objAro->attributes->model_id;
                    $arrhasAccessBody['alias'] = $objAco->attributes->alias;

                    $hasAccess = $this->call('POST', 'has/access', $arrhasAccessBody);
                    $this->refreshApplication();
                    $this->assertEquals(200, $hasAccess->getStatusCode());

                    // failed permission returns status code 403
                    $arrhasAccessBody['user_id'] = $objAro->attributes->model_id;
                    $arrhasAccessBody['alias'] = $objAro->attributes->alias;

                    $hasAccess = $this->call('POST', 'has/access', $arrhasAccessBody);
                    $this->refreshApplication();
                    $this->assertEquals(200, $hasAccess->getStatusCode());
                }

                // check for access 1
                //update permissions to 1
                $arrMappingBody['aco_id'] = $objAco->id;
                $arrMappingBody['access'] = 1;

                $updateMapping = $this->call('PUT', 'aroacos/mapping/' . $objAro->id, $arrMappingBody);
                $this->refreshApplication();
                $this->assertEquals(200, $updateMapping->getStatusCode());
                if (!is_null($objAro->attributes->model_id) && '' != $objAro->attributes->model_id) {
                    $arrhasAccessBody['user_id'] = $objAro->attributes->model_id;
                    $arrhasAccessBody['alias'] = $objAco->attributes->alias;

                    $hasAccess = $this->call('POST', 'has/access', $arrhasAccessBody);
                    $this->refreshApplication();
                    $this->assertEquals(200, $hasAccess->getStatusCode());

                    // valid user permission
                    $permissionByUserId = $this->call('GET', 'permissions/user/' . $objAro->attributes->model_id);
                    $this->refreshApplication();
                    $this->assertEquals(200, $permissionByUserId->getStatusCode());

                    // in-valid user permission
                    $permissionByUserId = $this->call('GET', 'permissions/user/' . $objAro->id);
                    $this->refreshApplication();
                    $this->assertEquals(200, $permissionByUserId->getStatusCode());
                }
            }
        }
    }

    /**
     * Test CRUD operation on API for failure
     *
     * @name   testCRUDAroFail
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDAroAcoFail()
    {
        // Get permision list by Invalid ID
        $permissionList = $this->call('GET', 'aroacos/aro/' . Uuid::generate(4));
        $this->refreshApplication();
        $this->assertEquals(404, $permissionList->getStatusCode());

        // in valid has/access

        $hasAccess = $this->call('POST', 'has/access', []);
        $this->refreshApplication();
        $this->assertEquals(404, $hasAccess->getStatusCode());


        //update permissions to 0 for invalid aro ID
        $responseAcoList = $this->call('GET', 'acos');
        $this->refreshApplication();
        $acoList = json_decode($responseAcoList->getContent())->data;
        $objAco = $acoList[0];

        $arrMappingBody['aco_id'] = $objAco->id;
        $arrMappingBody['access'] = 0;
        $updateMapping = $this->call('PUT', 'aroacos/mapping/' . $objAco->id, $arrMappingBody);
        $this->assertEquals(404, $updateMapping->getStatusCode());
        $this->refreshApplication();
    }
}
