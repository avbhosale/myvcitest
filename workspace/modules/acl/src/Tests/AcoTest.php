<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category UnitTestCase
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Tests\AcoTest;

use Modules\ACL\Models\ACO;
use Modules\ACL\Traits\Aco\AddAco;
use Modules\ACL\Traits\Aco\ListAco;
use Modules\ACL\Traits\Aco\ShowAco;
use Modules\ACL\Traits\Aco\DeleteAco;
use Modules\ACL\Traits\Aco\EditAco;
use Modules\ACL\Traits\Aco\AcoValidator;
use Modules\ACL\Traits\Aco\AcoAttributes;
use TestCase;

/**
 * Class for test cases
 *
 * @name     UnitTestCase
 * @category TestCase
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AcoTest extends TestCase
{

    use AddAco;
    use ListAco;
    use ShowAco;
    use DeleteAco;
    use EditAco;
    use AcoValidator;
    use AcoAttributes;

    /**
     * Test CRUD operation on API
     *
     * @name   testCRUDAcoApi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDAcoApi()
    {

        $rootAcoId = null;

        $arrFakeAco = factory(ACO::class)->make()->toArray();
        $arrFakeAco["parent_id"] = $rootAcoId;
        // add aco
        $response = $this->call('POST', 'acos', $arrFakeAco);
        $this->assertEquals(200, $response->status());
        $this->refreshApplication();


        $intAcoId = json_decode($response->getContent())->data->id;
        $this->refreshApplication();


        // list aco
        $responseList = $this->call('GET', 'acos');
        $this->assertEquals(200, $responseList->status());
        $this->refreshApplication();


        // show aco by ID
        $responseShow = $this->call('GET', 'acos/' . $intAcoId);
        $this->assertEquals(200, $responseShow->status());
        $this->refreshApplication();


        // edit aco by ID
        $arrFakeAco = factory(ACO::class)->make()->toArray();
        $arrFakeAco["parent_id"] = $rootAcoId;

        $responsePut = $this->call('PUT', 'acos/' . $intAcoId, $arrFakeAco);
        $this->assertEquals(200, $responsePut->status());
        $this->refreshApplication();
    }

    /**
     * Test CRUD operation on API for failure
     *
     * @name   testCRUDAcoFail
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDAcoFail()
    {
        $arrFakeAco = [];

        // add group
        $addResponse = $this->call('POST', 'acos', $arrFakeAco);
        $this->assertEquals(422, $addResponse->status());
        $this->refreshApplication();


        $intAcoId = '';

        //edit aco
        $editResponse = $this->call('PUT', 'acos/' . $intAcoId, $arrFakeAco);
        $this->assertEquals(405, $editResponse->status());
        $this->refreshApplication();


        $intAcoId = 'ahksdgaksdgh';

        // show aco by ID
        $responseShow = $this->call('GET', 'acos/' . $intAcoId);
        $this->assertEquals(500, $responseShow->status());
        $this->refreshApplication();


        //delete aco by ID
        $intAcosId = 'ab7ad310-6985-11e8-9b53-213c65f8c3b7';
        $deleteResponse = $this->call('DELETE', 'acos/' . $intAcosId);
        $this->assertEquals(404, $deleteResponse->status());
        $this->refreshApplication();
    }
}
