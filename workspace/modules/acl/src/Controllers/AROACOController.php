<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Controller
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\ACL\Repositories\ACORepository;
use Modules\ACL\Repositories\AROACORepository;
use Modules\ACL\Repositories\ARORepository;
use Modules\ACL\Traits\Aco\FetchParentAndChildAcos;
use Modules\ACL\Traits\AroAco\AroAcoAttributes;
use Modules\ACL\Traits\AroAco\AroAcoValidator;
use Modules\ACL\Traits\AroAco\CheckPermission;
use Modules\ACL\Traits\AroAco\CheckUserOrRole;
use Modules\ACL\Traits\AroAco\FetchUserPermissions;
use Modules\ACL\Traits\AroAco\ShowAroAcoByAroId;
use Modules\ACL\Transformers\AROACOTransformer;

/**
 * Controller class for AROACO Module
 *
 * @name     AROACOController
 * @category Controller
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AROACOController extends Controller
{

    private $_aroAcoRepository;
    private $_aroAcoTransformer;
    private $_aroRepository;
    private $_acoRepository;
    public $boolIsUser;
    public $boolIsRole;

    use AroAcoAttributes;
    use AroAcoValidator;
    use FetchParentAndChildAcos;
    use ShowAroAcoByAroId;
    use CheckPermission;
    use FetchUserPermissions;
    use CheckUserOrRole;

    /**
     * Default constructor function
     *
     * @param Object $aroAcoRepository  ACLRepository object
     * @param object $aroAcoTransformer ACLTransformer object
     * @param object $aroRepository     ARORepository object
     * @param object $acoRepository     ACORepository object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return mixed
     */
    public function __construct(AROACORepository $aroAcoRepository, AROACOTransformer $aroAcoTransformer, ARORepository $aroRepository, ACORepository $acoRepository)
    {
        $this->_aroAcoRepository = $aroAcoRepository;
        $this->_aroAcoTransformer = $aroAcoTransformer;
        $this->_aroRepository = $aroRepository;
        $this->_acoRepository = $acoRepository;
    }

    /**
     * Update an existing aroaco data in bulk by Model ID
     *
     * @param object  $request request object
     * @param integer $aroId   aroId
     *
     * @return object returns json object
     *
     * @SWG\Put(
     *     path="/aroacos/mapping/{id}",
     *     summary="Update an existing aroaco data in bulk by Model Id",
     *     description="Update an existing aroaco data in bulk by Model Id if user have single or multiple role.",
     *     operationId="bulkUpdateAroAco",
     *     tags={"aroacos"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="Model ID to update AroAco data in bulk",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/AroAcoPutBulkData")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/AroAcoGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the ‘Accept’ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function bulkUpdate(Request $request, $aroId)
    {
        try {
            $this->validateRequest($request, $this->_aroAcoValidationRules('bulkUpdate'), $this->_aroAcoValidationMessage('bulkUpdate'), $this->_setAroAcoAttributes());

            $columnAroId = 'AroId';
            $acoId = $request->aco_id;

            // check whether  ARO ID is of User or Role
            $this->checkUserOrRole($aroId);

            // get All parent ACOs
            if ($request->access) {
                $this->getAllParentAcosByAcoId($acoId);
                $arrobjParentAcos = collect($this->arrobjParentAcos)->pluck('AcoId')->toArray();
            } else {
                $arrobjParentAcos[] = $acoId;
            }

            // get all child ACOs
            $this->getAllChildAcosByAcoId($acoId);
            $arrobjChildAcos = collect($this->arrobjChildAcos)->pluck('AcoId')->toArray();

            $arrAllAcoIds = [];
            $arrAllAcoIds = array_merge($arrobjParentAcos, $arrobjChildAcos);

            if ($this->boolIsUser) {
                $arrAroIds = $this->aroUser->pluck('AroId')->toArray();

                $arrAroAcoIds = [];
                if (!empty($arrAllAcoIds)) {
                    foreach ($arrAllAcoIds as $strAcoId) {
                        $arrobjAroAcos = $this->_aroAcoRepository->findWhereIn($columnAroId, $arrAroIds, 'AcoId', $strAcoId);
                        $arrAroAcoIds = array_merge($arrAroAcoIds, $arrobjAroAcos->pluck('AroAcoId')->toArray());
                    }
                }
            } else {
                $arrAroAcoIds = [];
                if (!empty($arrAllAcoIds)) {
                    foreach ($arrAllAcoIds as $strAcoId) {
                        $arrobjAroAcos = $this->_aroAcoRepository->findBy([$columnAroId => $this->objAro->AroId, 'AcoId' => $strAcoId]);
                        $arrAroAcoIds = array_merge($arrAroAcoIds, $arrobjAroAcos->pluck('AroAcoId')->toArray());
                    }
                }
            }

            $intRecordCount = $this->_aroAcoRepository->bulkUpdate(['access' => $request->access], $arrAroAcoIds);

            return $this->response->array(['status_code' => 200, 'message' => 'Permission granted successfully.']);
        } catch (QueryException $exception) {
            return $this->response->errorInternal();
        }
    }

    /**
     * Get aroaco by aro ID to get all permission as per aro
     *
     * @param integer $id aroId
     *
     * @return object returns json object
     *
     * @SWG\Get(
     *     path="/aroacos/aro/{id}",
     *     summary="Find aroaco permission by ARO ID",
     *     description="Get aroaco(manage permission) object by ARO ID",
     *     operationId="getAroAcobyAroid",
     *     tags={"aroacos"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="ID of Aro to fetch specified AroId Data",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(    type="object", ref="#/definitions/GetAroAcoDataByAroId")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the ‘Accept’ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function getAroAcobyAroid($id)
    {
        $this->checkUserOrRole($id);

        return $this->showAroAcoByAroId($id);
    }

    /**
     * Check whether user has permission for given ACO ID
     *
     * @param Obj $request Request data
     *
     * @name   hasAccess
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function hasAccess(Request $request)
    {
        return $this->checkPermission($request);
    }

    /**
     * Get all permissions for given User ID
     *
     * @param String $strUserId User ID
     *
     * @name   getPermissionByUserId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function getPermissionByUserId($strUserId)
    {
        return $this->fetchPermissionByUserId($strUserId);
    }
}
