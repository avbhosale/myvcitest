<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Controller
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\ACL\Repositories\AROACORepository;
use Modules\ACL\Repositories\ARORepository;
use Modules\ACL\Transformers\AROACOTransformer;
use Modules\ACL\Transformers\AROTransformer;
use Modules\ACL\Repositories\ACORepository;
use Webpatser\Uuid\Uuid;
use Modules\ACL\Traits\Aro\AroAttributes;
use Modules\ACL\Traits\Aro\AroValidator;
use Modules\ACL\Traits\Aro\AddAro;
use Modules\ACL\Traits\Aro\EditAro;
use Modules\ACL\Traits\Aro\DeleteAro;
use Modules\ACL\Traits\Aro\ShowAro;
use Modules\ACL\Traits\Aro\ListAro;

/**
 * Controller class for ARO Module
 *
 * @name     AROController
 * @category Controller
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AROController extends Controller
{

    private $_aroRepository;
    private $_aroTransformer;
    private $_aroAcoRepository;
    private $_aroAcoTransformer;
    private $_acoRepository;

    use AroAttributes;
    use AroValidator;
    use AddAro;
    use EditAro;
    use DeleteAro;
    use ShowAro;
    use ListAro;

    /**
     * Default constructor function
     *
     * @param Object $aroRepository     ACLRepository object
     * @param object $aroTransformer    ACLTransformer object
     * @param object $aroAcoRepository  AROACORepository object
     * @param object $aroAcoTransformer AROACOTransformer object
     * @param object $acoRepository     ACORepository object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return mixed
     */
    public function __construct(ARORepository $aroRepository, AROTransformer $aroTransformer, AROACORepository $aroAcoRepository, AROACOTransformer $aroAcoTransformer, ACORepository $acoRepository)
    {
        $this->_aroRepository = $aroRepository;
        $this->_aroTransformer = $aroTransformer;
        $this->_aroAcoRepository = $aroAcoRepository;
        $this->_aroAcoTransformer = $aroAcoTransformer;
        $this->_acoRepository = $acoRepository;
    }

    /**
     * To get aro list
     *
     * @param object $request request object

     * @return object returns json object
     *
     * @SWG\Get(
     *     path="/aros",
     *     summary="Get aro list of acl",
     *     description="Get access request object list of access control list",
     *     operationId="getAro",
     *     produces={"application/json"},
     *     tags={"aros"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(type="object", ref="#/definitions/AroGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the Accept header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function index(Request $request)
    {
        return $this->listAro($request);
    }

    /**
     * To show aro details of id
     *
     * @param integer $id aroId
     *
     * @return object returns json object
     *
     * @SWG\Get(
     *     path="/aros/{id}",
     *     summary="Find aro by ID",
     *     description="Find access request object by ID",
     *     operationId="getAroById",
     *     tags={"aros"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="ID of Aro to fetch",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(    type="object", ref="#/definitions/AroGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the Accept header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function show($id)
    {
        return $this->showAro($id);
    }

    /**
     * To store aro data
     *
     * @param object $request request object
     *
     * @return object returns json object
     *
     * @SWG\Post(
     *     path="/aros",
     *     summary="Add a new aro to the acl",
     *     description="Add a new access request object to access control list",
     *     operationId="addAro",
     *     tags={"aros"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/AroPostData")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(                                 type="object", ref="#/definitions/AroGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the Accept header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function store(Request $request)
    {
        return $this->addAro($request);
    }

    /**
     * Bulk insertion of Access Permission against new User
     *
     * @param array   $arrPermission array of permission as per parent permission
     * @param string  $aroId         is aroId
     * @param integer $noAccess      access integer
     *
     * @name   getAroAcobyAroid
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    private function _bulkInsertAroAco($arrPermission, $aroId, $noAccess = 1)
    {
        $bulkInsert = [];

        foreach ($arrPermission as $rolePermission) {
            $data = [
                'AroAcoId' => Uuid::generate()->string,
                'AroId' => $aroId,
                'AcoId' => $rolePermission->AcoId,
                'Access' => ($noAccess === 1) ? 0 : $rolePermission->Access,
                'Etag' => time()
            ];
            $bulkInsert[] = $data;
        }
        return $bulkInsert;
    }

    /**
     * To update aro data of id
     *
     * @param object  $request request object
     * @param integer $id      aroId
     *
     * @return object returns json object
     *
     * @SWG\Put(
     *     path="/aros/{id}",
     *     summary="Update an existing aro by ID",
     *     description="Update an existing access request object by ID",
     *     operationId="updateAro",
     *     tags={"aros"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="ID of Aro to update",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/AroPutData")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(                                type="object", ref="#/definitions/AroGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the Accept header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        return $this->editAro($request, $id);
    }

    /**
     * To delete aro data of id
     *
     * @param integer $id aroId
     *
     * @return object returns json object
     *
     * @SWG\Delete(
     *     path="/aros/{id}",
     *     summary="Delete an existing aro by ID",
     *     description="Delete an existing access request object by ID",
     *     operationId="DeleteAro",
     *     tags={"aros"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="ID of Aro to delete",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(    type="object", ref="#/definitions/AroDeleteData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type  does not match the Accept header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function destroy($id)
    {
        return $this->deleteAro($id);
    }
}
