<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Models;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Model class for table Aco
 *
 * @name     ACO
 * @category Model
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class ACO extends BaseModel
{

    use SoftDeletes;

    const DELETED_AT = 'DeletedAt';

    protected $keyType = 'string';
    protected $tablePrefix = 'Acl';
    protected $table = 'Aco';
    protected $primaryKey = 'AcoId';
    protected $fillable = [
        'AcoId',
        'ParentId',
        'Model',
        'ModelId',
        'Name',
        'Alias',
        'LeftNode',
        'RightNode',
        'Etag',
        'DeletedAt'
    ];
    protected $dates = ['DeletedAt'];
    public $timestamps = false;
    public $acoErrorCodes = [
        'parent_id' => 'AC001',
        'name' => 'AC002',
        'alias' => 'AC003',
        'deleted_at' => 'AC004'
    ];

    /**
     * Default Constructor
     *
     * @param Array $attributes Attributes array
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $this->perPage = config('app.records_per_page');
    }
}
