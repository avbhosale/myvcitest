<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Swagger
 * @package  Swagger
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\AroAco\Models;

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroAcoGetData",
 *  type="object",
 *  @SWG\Property(property="data",  type="array",  @SWG\Items(ref="#/definitions/AroAco")),
 *  @SWG\Property(property="meta",  type="object", ref="#/definitions/Meta"),
 *  @SWG\Property(property="links", type="object", ref="#/definitions/Links")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroAcoData",
 *  type="object",
 *  @SWG\Property(property="data", type="object",  ref="#/definitions/AroAco")
 * )
 */


/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroAcoPostData",
 *  type="object",
 *  @SWG\Property(property="aro_id", type="string",  description="aro_id"),
 *  @SWG\Property(property="aco_id", type="string",  description="aco_id"),
 *  @SWG\Property(property="access", type="integer", description="access"),
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroAcoPutData",
 *  type="object",
 *  @SWG\Property(property="access", type="integer", description="access"),
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroAcoPutBulkData",
 *  type="object",
 *  @SWG\Property(property="aco_id", type="string",  description="aco_id"),
 *  @SWG\Property(property="access", type="integer", description="access"),
 * )
 */



/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroAcoDeleteData",
 *  type="object",
 *  @SWG\Property(property="id",     type="string", description="aroaco id"),
 *  @SWG\Property(property="status", type="string", description="status as deleted")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroAcoPatchData",
 *  type="object",
 *  @SWG\Property(property="access", type="integer", description="access")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="GetAroAcoDataByAroId",
 *  type="object",
 *  @SWG\Property(property="id",
 *  type="string", description="id"),
 *  @SWG\Property(property="type",
 *  type="string", description="type"),
 *  @SWG\Property(
 *          property="attributes",
 *          type="object",
 *          @SWG\Property(property="aro_id",     type="string",  description="aro_id"),
 *          @SWG\Property(property="aco_id",     type="string",  description="aco_id"),
 *          @SWG\Property(property="access",     type="integer", description="access"),
 *          @SWG\Property(property="etag",       type="string",  description="etag"),
 *          @SWG\Property(property="deleted_at", type="string",  description="deleted_at")
 *      )
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroAco",
 *  type="object",
 *  @SWG\Property(property="id",
 *  type="string", description="id"),
 *  @SWG\Property(property="type",
 *  type="string", description="type"),
 *  @SWG\Property(
 *          property="attributes",
 *          type="object",
 *          @SWG\Property(property="aro_id",     type="string",  description="aro_id"),
 *          @SWG\Property(property="aco_id",     type="string",  description="aco_id"),
 *          @SWG\Property(property="access",     type="integer", description="access"),
 *          @SWG\Property(property="etag",       type="string",  description="etag"),
 *          @SWG\Property(property="deleted_at", type="string",  description="deleted_at")
 *      )
 * )
 */
