<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  ARO
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\ACL\Models;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Two line description of class
 *
 * @name     ARO
 * @category Class
 * @package  ARO
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class ARO extends BaseModel
{

    use SoftDeletes;

    const DELETED_AT = 'DeletedAt';

    protected $keyType = 'string';
    protected $tablePrefix = 'Acl';
    protected $table = 'Aro';
    protected $primaryKey = 'AroId';
    protected $fillable = [
        'AroId',
        'ParentId',
        'Model',
        'ModelId',
        'Name',
        'Alias',
        'LeftNode',
        'RightNode',
        'Etag',
        'DeletedAt'
    ];
    protected $dates = ['DeletedAt'];
    public $timestamps = false;
    public $aroErrorCodes = [
        'parent_id' => 'AR001',
        'model' => 'AR003',
        'model_id' => 'AR002',
        'alias' => 'AR004',
        'name' => 'AR005',
        'deleted_at' => 'AR006'
    ];

    /**
     * Default Constructor
     *
     * @param array $attributes is used to get data sent by client
     *
     * @name   MethodName
     * @access public
     * @author VCI <info@vericheck.net>
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $this->perPage = config('app.records_per_page');
    }
}
