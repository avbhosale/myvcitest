<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Swagger
 * @package  Swagger
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */

namespace Modules\Aro\Models;

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroGetData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="array",  @SWG\Items(ref="#/definitions/Data")),
 *  @SWG\Property(property="meta",
 *  type="object", ref="#/definitions/Meta"),
 *  @SWG\Property(property="links",
 *  type="object", ref="#/definitions/Links")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="object",  ref="#/definitions/Data")
 * )
 */


/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroPostData",
 *  type="object",
 *  @SWG\Property(property="parent_id", type="string", description="parent_id"),
 *  @SWG\Property(property="model_id",  type="string", description="model_id"),
 *  @SWG\Property(property="model",     type="string", description="model name"),
 *  @SWG\Property(property="name",      type="string", description="name"),
 *  @SWG\Property(property="alias",     type="string", description="alias"),
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroPutData",
 *  type="object",
 *  @SWG\Property(property="name",  type="string", description="name"),
 *  @SWG\Property(property="alias", type="string", description="alias"),
 *  @SWG\Property(property="model", type="string", description="model name"),
 * )
 */



/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroDeleteData",
 *  type="object",
 *  @SWG\Property(property="id",     type="string", description="aro id"),
 *  @SWG\Property(property="status", type="string", description="status as deleted")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="AroPatchData",
 *  type="object",
 *  @SWG\Property(property="parent_id",  type="string", description="parent_id"),
 *  @SWG\Property(property="model",      type="string", description="model"),
 *  @SWG\Property(property="model_id",   type="string", description="model_id"),
 *  @SWG\Property(property="name",       type="string", description="name"),
 *  @SWG\Property(property="alias",      type="string", description="alias"),
 *  @SWG\Property(property="left_node",  type="string", description="left_node"),
 *  @SWG\Property(property="right_node", type="string", description="right_node"),
 *  @SWG\Property(property="child",      type="object", description="child"),
 *  @SWG\Property(property="etag",       type="string", description="etag"),
 * )
 */
