<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Migration
 * @package  ACL
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateAcoTable class is used to create Aco table
 *
 * @name     CreateAcoTable
 * @category ACL
 * @package  Migration
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateAcoTable extends Migration
{

    public $tableName;

    /**
     *  __construct function to set data
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Acl'; // Schema Name to Identify Table group
        $tableName = 'Aco'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = (config('database.default') == 'sqlsrv') ? $tablePrefix . $seperator . $tableName : $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function to create table
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('AcoId')->primary() : $table->bigIncrements('AcoId')->primary()->unsigned();

            $table->uuid('ParentId')->nullable()->default(null);
            $table->string('Model', 100)->nullable()->default(null);
            $table->uuid('ModelId')->nullable()->default(null);
            $table->string('Name', 255);
            $table->string('Alias', 255)->unique();
            $table->uuid('LeftNode')->nullable()->default(null)->index();
            $table->uuid('RightNode')->nullable()->default(null)->index();
            $table->integer('Etag');
            $table->dateTime('DeletedAt')->nullable();
        });
    }

    /**
     * Function to delete table
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
