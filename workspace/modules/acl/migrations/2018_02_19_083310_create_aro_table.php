<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Auth
 * @package  Authentication
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id: da75e564ffb4f7fd67bad71778009e8c02067ce9 $
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateAcoTable class is used to create Aro Table
 *
 * @name     CreateAroTable
 * @category ACL
 * @package  Migration
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateAroTable extends Migration
{

    public $aroTableName;

    /**
     * Default constructor
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $aroTablePrefix = 'Acl'; // Schema Name to Identify Table group
        $aroTableName = 'Aro'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->aroTableName = (config('database.default') == 'sqlsrv') ? $aroTablePrefix . $seperator . $aroTableName : $aroTablePrefix . $seperator . $aroTableName;
    }

    /**
     * Function to create aroTable
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->aroTableName, function (Blueprint $aroTable) {
            config('database.default') == 'sqlsrv' ? $aroTable->uuid('AroId')->primary() : $aroTable->bigIncrements('AroId')->primary()->unsigned();

            $aroTable->uuid('ParentId')->nullable()->default(null);
            $aroTable->string('Model', 100)->nullable()->default(null);
            $aroTable->uuid('ModelId')->nullable()->default(null);
            $aroTable->string('Name', 255);
            $aroTable->string('Alias', 255)->unique();
            $aroTable->uuid('LeftNode')->nullable()->default(null)->index();
            $aroTable->uuid('RightNode')->nullable()->default(null)->index();
            $aroTable->integer('Etag');
            $aroTable->dateTime('DeletedAt')->nullable();
        });
    }

    /**
     * Function to delete aroTable
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->aroTableName);
    }
}
