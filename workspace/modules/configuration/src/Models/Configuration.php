<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Configuration\Models;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Configuration Model
 *
 * @name     Configuration
 * @category Model
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class Configuration extends BaseModel
{

    use SoftDeletes;

    const DELETED_AT = 'DeletedAt';

    protected $tablePrefix = 'Master';
    protected $table = 'Configuration';
    protected $primaryKey = 'ConfigurationId';
    protected $fillable = [
        'ConfigurationId',
        'ConfigKey',
        'ConfigValue',
        'ConfigType',
        'EffectiveStartDate',
        'EffectiveEndDate',
        'ExpiresIn',
        'Status',
        'Etag',
        'CreatedAt',
        'DeletedAt'
    ];
    protected $dates = ['DeletedAt'];
    public $timestamps = false;

    /**
     * Default Constructor
     *
     * @param array $attributes constructor attributes
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $this->perPage = config('app.records_per_page');
    }
}
