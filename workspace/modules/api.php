<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Swagger
 * @package  Swagger
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules;

/**
 * This is a details documents for VeriCheck Phoenix API
 *
 * @SWG\Swagger(
 *     host="api.vericheck.in",
 *     basePath="/",
 *     schemes={"https"},
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="VeriCheck Phoenix API ",
 *         description="This is a details documents for VeriCheck Phoenix API",
 *         termsOfService="https://www.vericheck.com/",
 *         @SWG\Contact(name="VeriCheck API Team", email="support@vericheck.net"),
 *         @SWG\License(name="MIT")
 *     ),
 *     @SWG\Definition(
 *         definition="ErrorModel",
 *         type="object",
 *         required={"status", "code", "message"},
 *        @SWG\Property(
 *             property="status",
 *             type="string"
 *         ),
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */


/**
 * Azure AD security schema
 *
 * @SWG\SecurityScheme(
 *   securityDefinition="api_key",
 *   type="apiKey",
 *   in="header",
 *   name="api_key"
 * )
 */

/**
 * Azure AD security schema
 *
 * @SWG\SecurityScheme(
 *   securityDefinition="phoenix_auth",
 *   type="oauth2",
 *   authorizationUrl="https://login.microsoftonline.com/testphoenixdomain.onmicrosoft.com/oauth2/v2.0/authorize",
 *   flow="implicit",
 *   scopes={
 *     "read:post": "read your post",
 *     "write:post": "modify post in your account"
 *   }
 * )
 */


/**
 * This is a definition for Meta
 *
 * @SWG\Definition(
 *  definition="Meta",
 *  type="object",
 *  @SWG\Property(property="pagination",type="object",ref="#/definitions/Pagination")
 * )
 */

/**
 * This is a definition for Pagination
 *
 *  @SWG\Definition(
 *  definition="Pagination",
 *  type="object",
 *  @SWG\Property(property="total",type="string",description="id"),
 *  @SWG\Property(property="count",type="string",description="type"),
 *  @SWG\Property(property="per_page",type="string",description="property"),
 *  @SWG\Property(property="current_page",type="string",description="content"),
 *  @SWG\Property(property="total_pages",type="string",description="created_by")
 * )
 */

/**
 * This is a definition for Links
 *
 * @SWG\Definition(
 *  definition="Links",
 *  type="object",
 *  @SWG\Property(property="self",type="string",description="id"),
 *  @SWG\Property(property="first",type="string",description="type"),
 *  @SWG\Property(property="next",type="string",description="property"),
 *  @SWG\Property(property="last",type="string",description="created_by")
 * )
 */
