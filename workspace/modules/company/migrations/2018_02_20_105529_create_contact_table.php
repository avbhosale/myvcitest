<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateContactTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateContactTable class is used to create Company.Contact table
 *
 * @name     CreateContactTable.php
 * @category Migration
 * @package  CreateContactTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateContactTable extends Migration
{

    public $tableName;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'Contact'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.Contact
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('ContactId')->primary() : $table->bigIncrements('ContactId')->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('CompanyId')->index() : $table->bigInteger('CompanyId')->unsigned()->index();
            $table->string('FirstName', '64')->default('NULL');
            $table->string('LastName', '64')->nullable();
            $table->string('Phone', '16')->default('NULL');
            $table->string('Mobile', '16')->nullable();
            $table->string('PrimaryEmail', '256')->default('NULL');
            $table->string('SecondaryEmail', '256')->nullable();
            $table->string('Fax', '16')->nullable();
            $table->string('EmailFormat', '4')->default('HTML');
            $table->string('Type', '16')->default('SUPPORT');
            $table->integer('CreatedAt')->default(0);
            $table->integer('Etag')->default(0);
            $table->dateTime('DeletedAt')->nullable();
            $table->foreign('CompanyId', 'Contact_Company_CompanyId')->references('CompanyId')->on('Company.Company')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.Contact
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('Contact_Company_CompanyId');
        });
        Schema::drop($this->tableName);
    }
}
