<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreatePrincipalTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreatePrincipalTable class is used to create Company.Principal table
 *
 * @name     CreatePrincipalTable.php
 * @category Migration
 * @package  CreatePrincipalTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreatePrincipalTable extends Migration
{

    public $tableName;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'Principal'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.Principal
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('PrincipalId')->primary() : $table->bigIncrements('PrincipalId')->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('ContactId')->index() : $table->bigInteger('ContactId')->unsigned()->index();
            $table->string('SocialSecurityNumber', '64')->default('NULL');
            $table->string('DriverLicenseNumber', '32')->nullable()->default('NULL');
            $table->string('DriverLicenseState', '2')->nullable()->default('NULL');
            $table->tinyInteger('OwnershipPercentage')->nullable()->default(0);
            $table->integer('EffectiveStartDate')->default(0);
            $table->integer('EffectiveEndDate')->nullable();
            $table->integer('Etag')->default(0);
            $table->foreign('ContactId', 'Principal_Contact_ContactId')->references('ContactId')->on('Company.Contact')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.Principal
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('Principal_Contact_ContactId');
        });
        Schema::drop($this->tableName);
    }
}
