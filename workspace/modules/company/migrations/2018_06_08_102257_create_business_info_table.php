<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Migration
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration class to create table Business Info Table
 *
 * @name     CreateBusinessInfoTable
 * @category Migration
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateBusinessInfoTable extends Migration
{

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'BusinessInfo'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.BusinessInfo
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('BusinessInfoId')->primary() : $table->bigIncrements('BusinessInfoId')->nullable()->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('CompanyId') : $table->bigInteger('CompanyId')->unsigned()->index();
            config('database.default') == 'sqlsrv' ? $table->uuid('CorporateStructureId')->nullable()->default(null) : $table->bigInteger('CorporateStructureId')->nullable()->unsigned();
            $table->string('Description', '2048')->nullable()->default('NULL');
            $table->string('BusinessType', '16')->nullable()->default('NULL');
            $table->string('ProductService', '256')->nullable()->default('NULL');
            $table->string('NaicsCode', '64')->nullable()->default('NULL');
            $table->integer('BusinessStartDate')->nullable()->default(0);
            $table->integer('EffectiveStartDate')->nullable()->default(0);
            $table->integer('EffectiveEndDate')->nullable()->default(0);
            $table->integer('Etag');
            $table->foreign('CompanyId', 'BusinessInfo_Company_Id')->references('CompanyId')->on('Company.Company')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.business_info
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('BusinessInfo_Company_Id');
        });
        Schema::drop($this->tableName);
    }
}
