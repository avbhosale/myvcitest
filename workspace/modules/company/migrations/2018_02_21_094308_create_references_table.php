<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateReferencesTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateReferencesTable class is used to create Company.References table
 *
 * @name     CreateReferencesTable.php
 * @category Migration
 * @package  CreateReferencesTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateReferencesTable extends Migration
{

    public $tableName;
    public $seperator;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'References'; // Table Name
        $this->seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $this->seperator . $tableName;
    }

    /**
     * Function used to Create table Company.References
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('ReferencesId')->primary() : $table->bigIncrements('ReferencesId')->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('ContactId')->index() : $table->bigInteger('ContactId')->unsigned()->index();
            $table->string('AccountNumber', '16')->nullable();
            $table->tinyInteger('HasApproved')->default(0);
            config('database.default') == 'sqlsrv' ? $table->uuid('ApprovedBy')->index()->nullable() : $table->bigInteger('ApprovedBy')->unsigned()->index()->nullable();
            $table->integer('ApprovedAt')->nullable();
            $table->integer('Etag')->default(0);
            $table->foreign('ContactId', 'References_Contact_ContactId')->references('ContactId')->on('Company' . $this->seperator . 'Contact')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.References
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('References_Contact_ContactId');
        });
        Schema::drop($this->tableName);
    }
}
