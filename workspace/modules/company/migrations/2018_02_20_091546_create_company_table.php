<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateCompanyTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateCompanyTable class is used to create Company.Company table
 *
 * @name     CreateCompanyTable.php
 * @category Migration
 * @package  CreateCompanyTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateCompanyTable extends Migration
{

    public $tableName;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'Company'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.Company
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('CompanyId')->primary() : $table->bigIncrements('CompanyId')->nullable()->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('ParentId')->nullable()->default(null) : $table->bigInteger('ParentId')->nullable()->unsigned()->index();
            $table->string('Type', '16')->default('NULL');
            config('database.default') == 'sqlsrv' ? $table->uuid('OdfiId')->nullable()->index() : $table->bigInteger('OdfiId')->nullable()->unsigned()->index();
            $table->string('Name', '256')->nullable()->default('NULL');
            $table->string('DoingBusinessAs', '1024')->nullable()->default('NULL');
            $table->string('TaxId', '64')->nullable()->default('NULL');
            $table->string('WebsiteUrl', '1024')->nullable();
            $table->string('PinNumber', '64')->nullable()->default('NULL');
            $table->string('Uuid', '64')->nullable()->default('NULL');
            config('database.default') == 'sqlsrv' ? $table->uuid('GatewayId')->nullable()->index() : $table->bigInteger('GatewayId')->nullable()->unsigned()->index();
            $table->tinyInteger('CanIssueCredit')->nullable()->default(0);
            $table->tinyInteger('CanIssueCreditApproved')->nullable()->default(0);
            $table->tinyInteger('CanIssueCreditActive')->nullable()->default(0);
            $table->tinyInteger('HasBankstatements')->nullable()->default(0);
            $table->tinyInteger('HasDriverLicense')->nullable()->default(0);
            $table->tinyInteger('HasBusinessLicense')->nullable()->default(0);
            $table->tinyInteger('HasOther')->nullable()->default(0);
            $table->string('Other', '64')->nullable();
            $table->string('ReasonForDeactivation', '2048')->nullable();
            $table->tinyInteger('Status')->nullable()->default(0);
            config('database.default') == 'sqlsrv' ? $table->uuid('CreatedBy')->nullable()->index() : $table->bigInteger('CreatedBy')->nullable()->unsigned()->index();
            $table->integer('CreatedAt')->nullable()->default(0);
            $table->integer('Etag');
            $table->dateTime('DeletedAt')->nullable();
            $table->foreign('GatewayId', 'Company_Gateway_GatewayId')->references('GatewayId')->on('Master.Gateway')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.Company
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('Company_Gateway_GatewayId');
        });
        Schema::drop($this->tableName);
    }
}
