<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateAssignGroupTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateAssignGroupTable class is used to create Company.AssignGroup table
 *
 * @name     CreateAssignGroupTable.php
 * @category Migration
 * @package  CreateAssignGroupTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateAssignGroupTable extends Migration
{

    public $tableName;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'AssignGroup'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.AssignGroup
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('CompanyGroupId')->primary() : $table->bigIncrements('CompanyGroupId')->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('GroupId')->index() : $table->bigInteger('GroupId')->index()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('CompanyId')->index() : $table->bigInteger('CompanyId')->index()->unsigned();
            $table->foreign('GroupId', 'AssignGroup_Group_GroupId')->references('GroupId')->on('Master.Group')->onDelete('cascade');
            $table->foreign('CompanyId', 'AssignGroup_Company_CompanyId')->references('CompanyId')->on('Company.Company')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.AssignGroup
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('AssignGroup_Group_GroupId');
            $table->dropForeign('AssignGroup_Company_CompanyId');
        });

        Schema::drop($this->tableName);
    }
}
