<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Migration
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration class to create table information
 *
 * @name     CreateTableAmendmentNote
 * @category Migration
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateTableAmendmentNote extends Migration
{

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'AmendmentNote'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.AmendmentNote
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('AmendmentNoteId')->primary() : $table->bigIncrements('AmendmentNoteId')->nullable()->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('CompanyId') : $table->bigInteger('CompanyId')->unsigned()->index();
            config('database.default') == 'sqlsrv' ? $table->uuid('NoteId') : $table->bigInteger('NoteId')->unsigned()->index();
            $table->string('Note')->nullable()->default('NULL');
            $table->string('FieldName', '64');
            $table->string('OldValue', '1024');
            $table->string('NewValue', '1024');
            $table->string('Type', '8')->default('PROFILE');
            config('database.default') == 'sqlsrv' ? $table->uuid('CreatedBy') : $table->bigInteger('CreatedBy')->unsigned();
            $table->integer('CreatedAt')->nullable()->default(0);
            $table->foreign('CompanyId', 'AmendmentNote_Company_Id')->references('CompanyId')->on('Company.Company')->onDelete('cascade');
        });
    }

    /**
     * Function used to Create table Company.AmendmentNote
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('AmendmentNote_Company_Id');
        });
        Schema::drop($this->tableName);
    }
}
