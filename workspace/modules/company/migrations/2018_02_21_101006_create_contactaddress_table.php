<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateContactaddressTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateContactaddressTable class is used to create Company.ContactAddress table
 *
 * @name     CreateContactaddressTable.php
 * @category Migration
 * @package  CreateContactaddressTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateContactaddressTable extends Migration
{

    public $tableName;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'ContactAddress'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.ContactAddress
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('ContactAddressId')->primary() : $table->bigIncrements('ContactAddressId')->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('ContactId')->index() : $table->bigInteger('ContactId')->unsigned()->index();
            config('database.default') == 'sqlsrv' ? $table->uuid('AddressId')->index() : $table->bigInteger('AddressId')->unsigned()->index();
            $table->foreign('ContactId', 'ContactAddress_Contact_ContactId')->references('ContactId')->on('Company.Contact')->onDelete('cascade');
            $table->foreign('AddressId', 'ContactAddress_Address_AddressId')->references('AddressId')->on('Company.Address')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.ContactAddress
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('ContactAddress_Contact_ContactId');
            $table->dropForeign('ContactAddress_Address_AddressId');
        });
        Schema::drop($this->tableName);
    }
}
