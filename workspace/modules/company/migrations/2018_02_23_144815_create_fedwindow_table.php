<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateFedwindowTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateFedwindowTable class is used to create Company.FedWindow table
 *
 * @name     CreateFedwindowTable.php
 * @category Migration
 * @package  CreateFedwindowTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateFedwindowTable extends Migration
{

    var $tableName;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'FedWindow'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.FedWindow
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('FedWindowId')->primary() : $table->bigIncrements('FedWindowId')->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('CompanyId')->index() : $table->bigInteger('CompanyId')->unsigned()->index();
            config('database.default') == 'sqlsrv' ? $table->uuid('OdfiFedWindowId')->nullable()->index() : $table->bigInteger('OdfiFedWindowId')->nullable()->unsigned()->index();
            $table->tinyInteger('HasEnable')->default(0);
            $table->tinyInteger('Type')->nullable();
            $table->integer('Etag')->default(0);
            $table->foreign('CompanyId', 'FedWindow_Company_CompanyId')->references('CompanyId')->on('Company.Company')->onDelete('cascade');
            $table->foreign('OdfiFedWindowId', 'FedWindow_OdfiFedWindow_OdfiFedWindowId')->references('OdfiFedWindowId')->on('Bank.OdfiFedWindow')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.FedWindow
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('FedWindow_Company_CompanyId');
            $table->dropForeign('FedWindow_OdfiFedWindow_OdfiFedWindowId');
        });
        Schema::drop($this->tableName);
    }
}
