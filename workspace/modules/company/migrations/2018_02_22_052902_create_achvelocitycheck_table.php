<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateAchvelocitycheckTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateAchvelocitycheckTable class is used to create Company.AchVelocityCheck table
 *
 * @name     CreateAchvelocitycheckTable.php
 * @category Migration
 * @package  CreateAchvelocitycheckTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateAchvelocitycheckTable extends Migration
{

    public $tableName;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Company'; // Schema Name to Identify Table Company
        $tableName = 'AchVelocityCheck'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Company.AchVelocityCheck
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('AchVelocityCheckId')->primary() : $table->bigIncrements('AchVelocityCheckId')->primary()->unsigned();
            config('database.default') == 'sqlsrv' ? $table->uuid('CompanyId')->index() : $table->bigInteger('CompanyId')->unsigned()->index()->unique();
            $table->tinyInteger('HasApproved')->default(0);
            $table->tinyInteger('HasCheckDuplicate')->default(0);
            $table->tinyInteger('HasNsfDecline')->default(0);
            $table->tinyInteger('HasDefaultVelocity')->default(0);
            $table->smallInteger('MaxChecksPerDay')->default(0);
            $table->decimal('MaxAmountPerDay', '12', '2')->default(0);
            $table->smallInteger('DayPeriodMerchant')->default(0);
            $table->smallInteger('MaxChecksPerPeriod')->default(0);
            $table->decimal('MaxAmountPerPeriod', '12', '2')->default(0);
            $table->smallInteger('MaxChecksPerDayPerAcc')->default(0);
            $table->decimal('MaxAmountPerDayPerAcc', '12', '2')->default(0);
            $table->smallInteger('DayPeriodAcc')->default(0);
            $table->smallInteger('MaxChecksPerPeriodPerAcc')->default(0);
            $table->decimal('MaxAmountPerPeriodPerAcc', '12', '2')->default(0);
            $table->decimal('HighestDollarAmountPerTxn', '12', '2')->default(0);
            config('database.default') == 'sqlsrv' ? $table->uuid('FundingTimeId')->index() : $table->bigInteger('FundingTimeId')->unsigned()->index();
            $table->string('Type', '16')->default('NULL');
            $table->integer('EffectiveStartDate')->default(0);
            $table->integer('EffectiveEndDate')->nullable()->default(0);
            $table->integer('Etag')->default(0);
            $table->foreign('CompanyId', 'AchVelocityCheck_Company_CompanyId')->references('CompanyId')->on('Company.Company')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Company.AchVelocityCheck
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('AchVelocityCheck_Company_CompanyId');
        });
        Schema::drop($this->tableName);
    }
}
