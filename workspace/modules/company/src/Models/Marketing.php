<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

use App\BaseModel;

/**
 * Marketing Model
 *
 * @name     Marketing
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class Marketing extends BaseModel
{

    protected $keyType = 'string';
    protected $tablePrefix = 'Company';
    protected $table = 'Marketing';
    protected $primaryKey = 'MarketingId';
    protected $fillable = [
        'MarketingId',
        'CompanyId',
        'TargetMarket',
        'ProjectedApplications',
        'ExpectedTxnPerMonth',
        'ExpectedAmountPerMonth',
        'AverageTxnAmount',
        'PortfolioLow',
        'PortfolioHigh',
        'PortfolioInternet',
        'PortfolioOther',
        'GeographicArea',
        'MarketingMethod',
        'SiteVisitPerformed',
        'Etag'
    ];
    public $timestamps = false;
    public $marketingErrCodes = [
        'marketing_id' => 'CK001',
        'target_market' => 'CK002',
        'projected_applications' => 'CK003',
        'expected_txn_per_month' => 'CK004',
        'expected_amount_per_month' => 'CK005',
        'average_txn_amount' => 'CK006',
        'portfolio_low' => 'CK007',
        'portfolio_high' => 'CK008',
        'portfolio_internet' => 'CK009',
        'portfolio_other' => 'CK010',
        'geographic_area' => 'CK011',
        'marketing_method' => 'CK012',
        'site_visit_performed' => 'CK013',
        'etag' => 'CK014'
    ];

    /**
     * Default Constructor
     *
     * @param array $attributes request array
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
    }
}
