<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Contact Model
 *
 * @name     Contact.php
 * @category Model
 * @package  Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class Contact extends BaseModel
{

    use SoftDeletes;

    const DELETED_AT = 'DeletedAt';

    protected $tablePrefix = 'Company';
    protected $table = 'Contact';
    protected $primaryKey = 'ContactId';
    protected $fillable = [
        'ContactId',
        'CompanyId',
        'FirstName',
        'LastName',
        'Phone',
        'Mobile',
        'PrimaryEmail',
        'SecondaryEmail',
        'Fax',
        'EmailFormat',
        'Type',
        'CreatedAt',
        'Etag'
    ];
    public $timestamps = false;
    public $contactErrorCodes = [
        'company_id' => 'CT001',
        'first_name' => 'CT002',
        'last_name' => 'CT003',
        'phone' => 'CT004',
        'mobile' => 'CT005',
        'primary_email' => 'CT006',
        'secondary_email' => 'CT007',
        'fax' => 'CT008',
        'email_format' => 'CT009',
        'contact_type' => 'CT010'
    ];
    public $emailFormat = [
        'html' => 'HTML',
        'text' => 'TEXT'
    ];
    public $contactType = [
        'principal' => 'Principal Contact Information',
        'support' => 'Support Contact Information',
        'executive' => 'ISO Account Executive Information',
        'references' => 'Supplier References'
    ];

    /**
     * Default Constructor
     *
     * @param array $attributes request array
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $this->perPage = config('app.records_per_page');
    }
}
