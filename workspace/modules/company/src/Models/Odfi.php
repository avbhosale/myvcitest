<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

use App\BaseModel;

/**
 * Odfi Model
 *
 * @name     Odfi.php
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class Odfi extends BaseModel
{

    protected $keyType = 'string';
    protected $tablePrefix = 'Company';
    protected $table = 'FedWindow';
    protected $primaryKey = 'FedWindowId';
    protected $perPage;
    protected $fillable = [
        'FedWindowId ',
        'CompanyId',
        'OdfiFedWindowId',
        'HasEnable',
        'Type',
        'Etag'
    ];
    public $timestamps = false;
    public $odfiErrCodes = [
        'company_id' => 'OD001',
        'odfi_fed_window_id' => 'OD002',
        'has_enable' => 'OD003',
        'type' => 'OD004',
        'etag' => 'OD006'
    ];

    /**
     * Default Constructor
     *
     * @param array $attributes request array
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $this->perPage = config('app.records_per_page');
    }
}
