<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

use App\BaseModel;

/**
 * Company Model
 *
 * @name     VelocityChecks.php
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class VelocityChecks extends BaseModel
{

    protected $keyType = 'string';
    protected $tablePrefix = 'Company';
    protected $table = 'AchVelocityCheck';
    protected $primaryKey = 'AchVelocityCheckId';
    protected $perPage;
    protected $fillable = [
        'AchVelocityCheckId',
        'CompanyId',
        'HasApproved',
        'HasCheckDuplicate',
        'HasNsfDecline',
        'HasDefaultVelocity',
        'MaxChecksPerDay',
        'MaxAmountPerDay',
        'DayPeriodMerchant',
        'MaxChecksPerPeriod',
        'MaxAmountPerPeriod',
        'MaxChecksPerDayPerAcc',
        'MaxAmountPerDayPerAcc',
        'DayPeriodAcc',
        'MaxChecksPerPeriodPerAcc',
        'MaxAmountPerPeriodPerAcc',
        'HighestDollarAmountPerTxn',
        'FundingTimeId',
        'Type',
        'EffectiveStartDate',
        'EffectiveEndDate',
        'Etag'
    ];
    public $timestamps = false;
    public $velocityErrCodes = [
        'has_check_approved' => 'VD001',
        'has_check_duplicate' => 'VD002',
        'has_nsf_decline' => 'VD003',
        'has_default_velocity' => 'VD004',
        'max_checks_per_day' => 'VD005',
        'max_amount_per_day' => 'VD006',
        'day_period_merchant' => 'VD007',
        'max_checks_per_day' => 'VD008',
        'max_amount_per_day' => 'VD009',
        'max_checks_per_day_per_acc' => 'VD010',
        'max_amount_per_day_per_acc' => 'VD011',
        'day_period_account' => 'VD012',
        'max_checks_per_period_per_acc' => 'VD013',
        'max_amount_per_period_per_acc' => 'VD014',
        'highest_dollar_amount_per_txn' => 'VD015',
        'company_id' => 'VD016',
        'has_approved' => 'VD017',
        'has_default_velocity' => 'VD018',
        'funding_time_id' => 'VD019',
        'type' => 'VD020'
    ];
    public $velocityCheckType = [
        'debit' => 'DEBIT',
        'credit' => 'CREDIT'
    ];

    /**
     * Default Constructor
     *
     * @param array $attributes request array
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $this->perPage = config('app.records_per_page');
    }
}
