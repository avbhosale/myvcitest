<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Swagger
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

/**
 * Add Amendment Note
 *
 * @SWG\Definition(
 *  definition="AddAmendmentNote",
 *  type="object",
 *          @SWG\Property(property="note_id",    type="string", description="Note Id"),
 *          @SWG\Property(property="note",       type="string", description="Note"),
 *          @SWG\Property(property="field_name", type="string", description="Field name"),
 *          @SWG\Property(property="old_value",  type="string", description="Old value"),
 *          @SWG\Property(property="new_value",  type="string", description="New value"),
 *          @SWG\Property(property="type",       type="string", description="Type"),
 *          @SWG\Property(property="created_by", type="string", description="Created by"),
 *
 * )
 */
