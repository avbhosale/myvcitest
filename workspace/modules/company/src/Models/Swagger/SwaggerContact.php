<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Auth
 * @package  Authentication
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models\Swagger;

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactGetData",
 *  type="object",
 *  @SWG\Property(property="data",  type="array",  @SWG\Items(ref="#/definitions/ContactData")),
 *  @SWG\Property(property="meta",  type="object", ref="#/definitions/Meta"),
 *  @SWG\Property(property="links", type="object", ref="#/definitions/Links")
 * )
 */
/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactData",
 *  type="object",
 *  @SWG\Property(property="id",              type="string", description="id"),
 *  @SWG\Property(property="type",            type="string", description="type"),
 *  @SWG\Property(
 *          property="attributes",
 *          type="object",
 *          @SWG\Property(property="company_id",      type="string",  description="Company ID"),
 *          @SWG\Property(property="first_name",      type="string",  description="First Name"),
 *          @SWG\Property(property="last_name",       type="integer", description="Last Name"),
 *          @SWG\Property(property="phone",           type="string",  description="Phone"),
 *          @SWG\Property(property="mobile",          type="string",  description="Mobile"),
 *          @SWG\Property(property="primary_email",   type="string",  description="Primary Email ID"),
 *          @SWG\Property(property="secondary_email", type="integer", description="Secondary Email ID"),
 *          @SWG\Property(property="fax",             type="integer", description="Fax Number"),
 *          @SWG\Property(property="email_format",    type="integer", description="Email Format : html or text"),
 *          @SWG\Property(property="contact_type",    type="integer", description="Contact Type: support, principal, executive or references"),
 *          @SWG\Property(property="created_at",      type="integer", description="Contact created at"),
 *          @SWG\Property(property="etag",            type="integer", description="Contact updated at"),
 *          @SWG\Property(property="address",         type="array",  @SWG\Items(ref="#/definitions/ContactAddressData")),
 *          @SWG\Property(property="principal",       type="object",  ref="#/definitions/ContactPrincipalData"),
 *          @SWG\Property(property="references",      type="object",  ref="#/definitions/ContactReferencesData")
 *      )
 * )
 */
/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactAddressData",
 *  type="object",
 *  @SWG\Property(property="id",              type="string",  description="Address ID"),
 *  @SWG\Property(property="address_line1",   type="string",  description="Address Line1"),
 *  @SWG\Property(property="address_line2",   type="integer", description="Address Line2"),
 *  @SWG\Property(property="city",            type="string",  description="City"),
 *  @SWG\Property(property="state",           type="string",  description="State"),
 *  @SWG\Property(property="zip",             type="string",  description="Zip Code"),
 *  @SWG\Property(property="primary_phone",   type="integer", description="Primary Phone"),
 *  @SWG\Property(property="secondary_phone", type="integer", description="Secondary Phone"),
 *  @SWG\Property(property="email_format",    type="integer", description="Email Format : html or text"),
 *  @SWG\Property(property="primary_email",   type="integer", description="Primary Email"),
 *  @SWG\Property(property="secondary_email", type="integer", description="Secondary Email"),
 *  @SWG\Property(property="fax",             type="integer", description="Fax"),
 *  @SWG\Property(property="address_type",    type="integer", description="Address Type"),
 *  @SWG\Property(property="created_at",      type="integer", description="Contact created at"),
 *  @SWG\Property(property="etag",            type="integer", description="Contact updated at")
 * )
 */
/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPrincipalData",
 *  type="object",
 *  @SWG\Property(property="id",                    type="string",  description="Principal ID"),
 *  @SWG\Property(property="contact_id",            type="string",  description="Contact ID"),
 *  @SWG\Property(property="ssn",                   type="integer", description="Social Security Number"),
 *  @SWG\Property(property="driver_license_number", type="string",  description="Driver License Number"),
 *  @SWG\Property(property="driver_license_state",  type="string",  description="Driver License State"),
 *  @SWG\Property(property="ownership_percentage",  type="string",  description="Ownership Percentage"),
 *  @SWG\Property(property="effective_start_date",  type="integer", description="Effective Start Date"),
 *  @SWG\Property(property="effective_end_date",    type="integer", description="Effective End Date"),
 *  @SWG\Property(property="etag",                  type="integer", description="Principal contact updated at")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactReferencesData",
 *  type="object",
 *  @SWG\Property(property="id",             type="string",  description="References ID"),
 *  @SWG\Property(property="contact_id",     type="string",  description="Contact ID"),
 *  @SWG\Property(property="account_number", type="string",  description="Account Number"),
 *  @SWG\Property(property="has_approved",   type="string",  description="Driver License State"),
 *  @SWG\Property(property="approved_by",    type="string",  description="Ownership Percentage"),
 *  @SWG\Property(property="approved_at",    type="integer", description="Effective Start Date"),
 *  @SWG\Property(property="etag",           type="integer", description="Contact references updated at")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPostData",
 *  type="object",
 *  @SWG\Property(property="first_name",      type="string", description="First name"),
 *  @SWG\Property(property="last_name",       type="string", description="last type"),
 *  @SWG\Property(property="phone",           type="integer",description="Phone number"),
 *  @SWG\Property(property="mobile",          type="integer",description="Mobile number"),
 *  @SWG\Property(property="primary_email",   type="string", description="Primary email"),
 *  @SWG\Property(property="secondary_email", type="string", description="Secondary email"),
 *  @SWG\Property(property="fax",             type="string", description="Fax"),
 *  @SWG\Property(property="email_format",    type="string", description="Email format: html or text"),
 *  @SWG\Property(property="contact_type",    type="string", description="Contact type: support, principal, executive or references"),
 *  @SWG\Property(property="address",         type="object", ref="#/definitions/ContactPostAddressData"),
 *  @SWG\Property(property="principal",       type="object", ref="#/definitions/ContactPostPrincipalData"),
 *  @SWG\Property(property="references",      type="object", ref="#/definitions/ContactPostReferencesData")
 * )
 */

/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPutData",
 *  type="object",
 *  @SWG\Property(property="first_name",      type="string", description="First name"),
 *  @SWG\Property(property="last_name",       type="string", description="last type"),
 *  @SWG\Property(property="phone",           type="integer",description="Phone number"),
 *  @SWG\Property(property="mobile",          type="integer",description="Mobile number"),
 *  @SWG\Property(property="primary_email",   type="string", description="Primary email"),
 *  @SWG\Property(property="secondary_email", type="string", description="Secondary email"),
 *  @SWG\Property(property="fax",             type="string", description="Fax"),
 *  @SWG\Property(property="email_format",    type="string", description="Email format: html or text"),
 *  @SWG\Property(property="contact_type",    type="string", description="Contact type: support, principal, executive or references"),
 *  @SWG\Property(property="address",         type="object", ref="#/definitions/ContactPutAddressData"),
 *  @SWG\Property(property="principal",       type="object", ref="#/definitions/ContactPutPrincipalData"),
 *  @SWG\Property(property="references",      type="object", ref="#/definitions/ContactPutReferencesData")
 * )
 */


/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPostAddressData",
 *  type="object",
 *  @SWG\Property(property="address_line1", type="string",  description="Address line 1"),
 *  @SWG\Property(property="address_line2", type="string",  description="Address line 2"),
 *  @SWG\Property(property="city",          type="string",  description="city"),
 *  @SWG\Property(property="state",         type="string",  description="State"),
 *  @SWG\Property(property="zip",           type="integer", description="Zip")
 * )
 */
/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPostPrincipalData",
 *  type="object",
 *  @SWG\Property(property="ssn",                   type="string",  description="Social Security Number"),
 *  @SWG\Property(property="driver_license_number", type="string",  description="Driver license number"),
 *  @SWG\Property(property="driver_license_state",  type="string",  description="Driver license state"),
 *  @SWG\Property(property="ownership_percentage",  type="string",  description="Ownership Percentage")
 * )
 */
/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPostReferencesData",
 *  type="object",
 *  @SWG\Property(property="account_number", type="string",  description="Account Number")
 * )
 */


/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPutAddressData",
 *  type="object",
 *  @SWG\Property(property="id",            type="string",  description="Address ID"),
 *  @SWG\Property(property="address_line1", type="string",  description="Address line 1"),
 *  @SWG\Property(property="address_line2", type="string",  description="Address line 2"),
 *  @SWG\Property(property="city",          type="string",  description="city"),
 *  @SWG\Property(property="state",         type="string",  description="State"),
 *  @SWG\Property(property="zip",           type="integer", description="Zip")
 * )
 */
/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPutPrincipalData",
 *  type="object",
 *  @SWG\Property(property="ssn",                   type="string",  description="Social Security Number"),
 *  @SWG\Property(property="driver_license_number", type="string",  description="Driver license number"),
 *  @SWG\Property(property="driver_license_state",  type="string",  description="Driver license state"),
 *  @SWG\Property(property="ownership_percentage",  type="string",  description="Ownership Percentage")
 * )
 */
/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactPutReferencesData",
 *  type="object",
 *  @SWG\Property(property="id",             type="string",  description="References ID"),
 *  @SWG\Property(property="account_number", type="string",  description="Account Number")
 * )
 */


/**
 * Definition
 *
 * @SWG\Definition(
 *  definition="ContactDeleteData",
 *  type="object",
 *  @SWG\Property(property="id",     type="string", description="Contact ID"),
 *  @SWG\Property(property="status", type="string", description="status as deleted"),
 * )
 */
