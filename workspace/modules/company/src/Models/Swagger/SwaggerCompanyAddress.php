<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Swagger
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

/**
 * Output response in json format
 *
 * @SWG\Definition(
 *  definition="AddressGetData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="array",
 *  @SWG\Items(ref="#/definitions/Data")),
 *  @SWG\Property(property="meta",
 *  type="object", ref="#/definitions/Meta"),
 *  @SWG\Property(property="links",
 *  type="object", ref="#/definitions/Links")
 * )
 */


/**
 * Swagger Definition
 *
 * @SWG\Definition(
 *  definition="AddressData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="object",  ref="#/definitions/Data")
 * )
 */


/**
 * Create api for Address
 *
 * @SWG\Definition(
 *  definition="AddressPostData",
 *  type="object",
 *  @SWG\Property(property="address_line1",   type="string",  description="address_line1"),
 *  @SWG\Property(property="address_line2",   type="string",  description="address_line2"),
 *  @SWG\Property(property="city",            type="string",  description="city"),
 *  @SWG\Property(property="state",           type="string",  description="state"),
 *  @SWG\Property(property="zip",             type="integer", description="zip"),
 *  @SWG\Property(property="primary_phone",   type="integer", description="primary_phone"),
 *  @SWG\Property(property="secondary_phone", type="integer", description="secondary_phone"),
 *  @SWG\Property(property="primary_email",   type="string",  description="primary_email"),
 *  @SWG\Property(property="secondary_email", type="string",  description="secondary_email"),
 *  @SWG\Property(property="fax",             type="integer", description="fax"),
 *  @SWG\Property(property="address_type",    type="string",  description="address_type"),
 * )
 */

/**
 * Update api for Address .
 *
 * @SWG\Definition(
 *  definition="AddressPutData",
 *  type="object",
 *  @SWG\Property(property="address_line1",   type="string",  description="address_line1"),
 *  @SWG\Property(property="address_line2",   type="string",  description="address_line2"),
 *  @SWG\Property(property="city",            type="string",  description="city"),
 *  @SWG\Property(property="state",           type="string",  description="state"),
 *  @SWG\Property(property="zip",             type="integer", description="zip"),
 *  @SWG\Property(property="primary_phone",   type="integer", description="primary_phone"),
 *  @SWG\Property(property="secondary_phone", type="integer", description="secondary_phone"),
 *  @SWG\Property(property="primary_email",   type="string",  description="primary_email"),
 *  @SWG\Property(property="secondary_email", type="string",  description="secondary_email"),
 *  @SWG\Property(property="fax",             type="integer", description="fax"),
 *  @SWG\Property(property="address_type",    type="string",  description="address_type"),
 * )
 */

/**
 * Delete Address data
 *
 * @SWG\Definition(
 *  definition="AddressDeleteData",
 *  type="object",
 *  @SWG\Property(property="id", type="string", description="id"),
 * )
 */
