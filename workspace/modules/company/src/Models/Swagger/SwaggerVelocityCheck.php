<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Swagger
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

/**
 * Output response in json format
 *
 * @SWG\Definition(
 *  definition="VelocityChecksGetData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="array",
 *  @SWG\Items(ref="#/definitions/Data")),
 *  @SWG\Property(property="meta",
 *  type="object", ref="#/definitions/Meta"),
 *  @SWG\Property(property="links",
 *  type="object", ref="#/definitions/Links")
 * )
 */


/**
 * Swagger Definition
 *
 * @SWG\Definition(
 *  definition="VelocityChecksData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="object",  ref="#/definitions/Data")
 * )
 */


/**
 * Create api for VelocityChecks
 *
 * @SWG\Definition(
 *  definition="VelocityChecksPostData",
 *  type="object",
 *  @SWG\Property(property="has_approved",                  type="integer",  description="has_approved"),
 *  @SWG\Property(property="has_check_duplicate",           type="integer",  description="has_check_duplicate"),
 *  @SWG\Property(property="has_nsf_decline",               type="integer",  description="has_nsf_decline"),
 *  @SWG\Property(property="has_default_velocity",          type="integer",  description="has_default_velocity"),
 *  @SWG\Property(property="max_checks_per_day",            type="integer",  description="max_checks_per_day"),
 *  @SWG\Property(property="max_amount_per_day",            type="string",  description="max_amount_per_day"),
 *  @SWG\Property(property="day_period_merchant",           type="integer",  description="day_period_merchant"),
 *  @SWG\Property(property="max_checks_per_day_per_acc",    type="integer",  description="max_checks_per_day_per_acc"),
 *  @SWG\Property(property="max_amount_per_day_per_acc",    type="string",  description="max_amount_per_day_per_acc"),
 *  @SWG\Property(property="day_period_account",            type="string",  description="day_period_account"),
 *  @SWG\Property(property="max_checks_per_period_per_acc", type="integer", description="max_checks_per_period_per_acc"),
 *  @SWG\Property(property="max_amount_per_period_per_acc", type="string", description="max_amount_per_period_per_acc"),
 *  @SWG\Property(property="highest_dollar_amount_per_txn", type="string", description="highest_dollar_amount_per_txn"),
 *  @SWG\Property(property="funding_time_id",               type="string", description="funding_time_id"),
 *  @SWG\Property(property="type",                          type="string", description="type"),
 *  @SWG\Property(property="effective_start_date",          type="integer", description="effective_start_date"),
 *  @SWG\Property(property="effective_end_date",            type="integer", description="effective_end_date"),
 * )
 */

/**
 * Update api for Add VelocityChecks .
 *
 * @SWG\Definition(
 *  definition="VelocityChecksPutData",
 *  type="object",
 *  @SWG\Property(property="has_approved",                  type="integer",  description="has_approved"),
 *  @SWG\Property(property="has_check_duplicate",           type="integer",  description="has_check_duplicate"),
 *  @SWG\Property(property="has_nsf_decline",               type="integer",  description="has_nsf_decline"),
 *  @SWG\Property(property="has_default_velocity",          type="integer",  description="has_default_velocity"),
 *  @SWG\Property(property="max_checks_per_day",            type="integer",  description="max_checks_per_day"),
 *  @SWG\Property(property="max_amount_per_day",            type="string",  description="max_amount_per_day"),
 *  @SWG\Property(property="day_period_merchant",           type="integer",  description="day_period_merchant"),
 *  @SWG\Property(property="max_checks_per_day_per_acc",    type="integer",  description="max_checks_per_day_per_acc"),
 *  @SWG\Property(property="max_amount_per_day_per_acc",    type="string",  description="max_amount_per_day_per_acc"),
 *  @SWG\Property(property="day_period_account",            type="string",  description="day_period_account"),
 *  @SWG\Property(property="max_checks_per_period",         type="integer",  description="max_checks_per_period"),
 *  @SWG\Property(property="max_amount_per_period",         type="string",  description="max_amount_per_period"),
 *  @SWG\Property(property="max_checks_per_period_per_acc", type="integer", description="max_checks_per_period_per_acc"),
 *  @SWG\Property(property="max_amount_per_period_per_acc", type="string", description="max_amount_per_period_per_acc"),
 *  @SWG\Property(property="highest_dollar_amount_per_txn", type="string", description="highest_dollar_amount_per_txn"),
 *  @SWG\Property(property="funding_time_id",               type="string", description="funding_time_id"),
 *  @SWG\Property(property="type",                          type="string", description="type"),
 *  @SWG\Property(property="effective_start_date",          type="integer", description="effective_start_date"),
 *  @SWG\Property(property="effective_end_date",            type="integer", description="effective_end_date"),
 * )
 */

/**
 * Delete VelocityChecks data
 *
 * @SWG\Definition(
 *  definition="VelocityCheckDeleteData",
 *  type="object",
 *  @SWG\Property(property="id", type="string", description="id"),
 * )
 */
