<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

use App\BaseModel;

/**
 * Marketing Model
 *
 * @name     AmendmentNote
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AmendmentNote extends BaseModel
{

    protected $keyType = 'string';
    protected $tablePrefix = 'Company';
    protected $table = 'AmendmentNote';
    protected $primaryKey = 'AmendmentNoteId';
    protected $fillable = [
        'AmendmentNoteId',
        'CompanyId',
        'NoteId',
        'Note',
        'FieldName',
        'OldValue',
        'NewValue',
        'Type',
        'CreatedBy',
        'CreatedAt'
    ];
    public $timestamps = false;
    public $AmendmentNoteErrCodes = [
        'company_id' => 'NT001',
        'note_id' => 'NT002',
        'note' => 'NT003',
        'field_name' => 'NT004',
        'old_value' => 'NT005',
        'new_value' => 'NT006',
        'type' => 'NT007',
        'created_at' => 'NT008',
        'created_by' => 'NT009'
    ];

    /**
     * Default Constructor
     *
     * @param array $attributes request array
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
    }
}
