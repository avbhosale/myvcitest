<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

use App\BaseModel;

/**
 * BusinessInfo Model
 *
 * @name     BusinessInfo
 * @category Model
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class BusinessInfo extends BaseModel
{

    protected $keyType = 'string';
    protected $tablePrefix = 'Company';
    protected $table = 'BusinessInfo';
    protected $primaryKey = 'BusinessInfoId';
    protected $fillable = [
        'BusinessInfoId',
        'CompanyId',
        'CorporateStructureId',
        'Description',
        'BusinessType',
        'ProductService',
        'NaicsCode',
        'BusinessStartDate',
        'EffectiveStartDate',
        'EffectiveEndDate',
        'Etag'
    ];
    protected $dates = ['DeletedAt'];
    public $timestamps = false;
    public $businessInfoErrCodes = [
        'business_info_id' => 'CZ001',
        'description' => 'CK004',
        'business_type' => 'CZ005',
        'product_service' => 'CZ006',
        'naics_code' => 'CZ007',
        'business_start_date' => 'CZ008',
        'effective_start_date' => 'CZ009',
        'effective_end_date' => 'CZ010',
        'etag' => 'CZ011',
    ];

    /**
     * Default Constructor
     *
     * @param array $attributes request array
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
    }
}
