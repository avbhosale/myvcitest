<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Swagger
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

/**
 * Add Business info
 *
 * @SWG\Definition(
 *  definition="AddBusinessInfo",
 *  type="object",
 *          @SWG\Property(property="description",                           type="string", description="Description"),
 *          @SWG\Property(property="business_type",                         type="string", description="Business type"),
 *          @SWG\Property(property="product_service",                       type="string", description="Product service"),
 *          @SWG\Property(property="naics_code",                            type="string", description="NAICS code"),
 *          @SWG\Property(property="corporate_structure_id",                type="string", description="Corporate structure id"),
 *          @SWG\Property(property="business_start_date",                   type="string", description="Business start date"),
 *          @SWG\Property(property="expected_txn_per_month",                type="string", description="Expected transactions per month"),
 *          @SWG\Property(property="expected_amount_per_month",             type="string", description="Expected amount per month"),
 *          @SWG\Property(property="average_txn_amount",                    type="string", description="Average transaction amount"),
 *          @SWG\Property(property="expected_total_transactions_per_month", type="string", description="Expected total transactions per month"),
 *          @SWG\Property(property="expected_total_dollars_per_month",      type="string", description="Expected total dollars per_month"),
 *
 * )
 */


/**
 * Edit Business info
 *
 * @SWG\Definition(
 *  definition="EditBusinessInfo",
 *  type="object",
 *          @SWG\Property(property="description",                           type="string", description="Description"),
 *          @SWG\Property(property="business_type",                         type="string", description="Business type"),
 *          @SWG\Property(property="product_service",                       type="string", description="Product service"),
 *          @SWG\Property(property="naics_code",                            type="string", description="NAICS code"),
 *          @SWG\Property(property="corporate_structure_id",                type="string", description="Corporate structure id"),
 *          @SWG\Property(property="business_start_date",                   type="string", description="Business start date"),
 *          @SWG\Property(property="expected_txn_per_month",                type="string", description="Expected transactions per month"),
 *          @SWG\Property(property="expected_amount_per_month",             type="string", description="Expected amount per month"),
 *          @SWG\Property(property="average_txn_amount",                    type="number", description="Average transaction amount"),
 *          @SWG\Property(property="expected_total_transactions_per_month", type="string", description="Expected total transactions per month"),
 *          @SWG\Property(property="expected_total_dollars_per_month",      type="string", description="Expected total dollars per_month"),
 *
 * )
 */
