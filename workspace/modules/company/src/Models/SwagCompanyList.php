<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Swagger
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Models;

/**
 * Get all assigned groups of ISO/Merchants from database
 *
 * @SWG\Definition(
 *  definition="AssignedGroupList",
 *  type="object",
 *          @SWG\Property(property="id",   type="string", description="id"),
 *          @SWG\Property(property="type", type="string", description="type"),
 * )
 */


/**
 * Create the group assignment
 *
 * @SWG\Definition(
 *  definition="CreateGroupAssignement",
 *  type="object",
 *          @SWG\Property(property="group_id",    type="string", description="Group id of assigned group"),
 *          @SWG\Property(property="company_ids", type="string", description="Comma separated company ids")
 * )
 */


/**
 * Create/Update company operation
 *
 * @SWG\Definition(
 *  definition="CompanyOperation",
 *  type="object",
 *  @SWG\Property(property="gateway_id",                type="string", description="Gateway source for the operation"),
 *  @SWG\Property(property="can_issue_credit",          type="boolean", description="Can merchant want to issue credit"),
 *  @SWG\Property(property="can_issue_credit_approved", type="boolean", description="Can merchant issue credit approved"),
 *  @SWG\Property(property="can_issue_credit_active",   type="boolean", description="Can merchant issue the credit"),
 *  @SWG\Property(property="reason_for_deactivation",   type="string", description="merchant reason for service deactivation"),
 * )
 */

/**
 * Create/Update company operation
 *
 * @SWG\Definition(
 *  definition="CompanyUnderwriting",
 *  type="object",
 *  @SWG\Property(property="has_bank_statements",  type="boolean", description="Can merchant want to issue credit"),
 *  @SWG\Property(property="has_driver_license",   type="boolean", description="Can merchant issue credit approved"),
 *  @SWG\Property(property="has_business_license", type="boolean", description="Can merchant issue the credit"),
 *  @SWG\Property(property="has_other",            type="string", description="merchant reason for service deactivation"),
 * )
 */

/**
 * Get Company Operation
 *
 * @SWG\Definition(
 *  definition="GetCompanyOperation",
 *  type="object",
 *  @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/CompanyOperation")),
 * )
 */

/**
 * Get Company Underwriting
 *
 * @SWG\Definition(
 *  definition="GetCompanyUnderwriting",
 *  type="object",
 *  @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/CompanyUnderwriting")),
 * )
 */
