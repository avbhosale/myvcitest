<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Company\Repositories\CompanyRepository;
use Modules\Company\Repositories\OdfiRepository;
use Modules\Company\Transformers\OdfiTransformer;
use Modules\Company\Traits\Odfi\AddOdfi;
use Modules\Company\Traits\Odfi\ShowOdfi;
use Modules\Company\Traits\Odfi\OdfiAttributes;
use Modules\Company\Traits\Odfi\OdfiValidator;
use Modules\Infrastructure\Services\ValidateIsExists;

/**
 * Controller class for Company Module
 *
 * @name     OdfiController
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class OdfiController extends Controller
{

    private $_companyRepository;
    private $_odfiRepository;
    private $_odfiTransformer;

    use OdfiValidator;
    use OdfiAttributes;
    use AddOdfi;
    use ShowOdfi;
    use ValidateIsExists;

    /**
     * Default constructor function
     *
     * @param Obj $_companyRepository Repository
     * @param Obj $_odfiRepository    Repository
     * @param Obj $_odfiTransformer   Transformer
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct(CompanyRepository $_companyRepository, OdfiRepository $_odfiRepository, OdfiTransformer $_odfiTransformer)
    {
        $this->_odfiRepository = $_odfiRepository;
        $this->_odfiTransformer = $_odfiTransformer;
        $this->_companyRepository = $_companyRepository;
        parent::__construct();
    }

    /**
     * To show odfi detail of id
     *
     * @param integer $companyId companyId
     *
     * @return object returns json object
     *
     * @SWG\Get(
     *     path="/companies/{id}/odfis",
     *     summary="Get odfi by Company ID",
     *     description="Retrieve odfi detail by company id",
     *     operationId="getOdfiById",
     *     tags={"odfis"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="ID of Company to fetch",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(    type="object", ref="#/definitions/OdfiGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the ‘Accept’ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function show($companyId)
    {
        return $this->showOdfi($companyId);
    }

    /**
     * To Add ODFI
     *
     * @param object $request request object
     * @param string $id      id
     *
     * @return object returns json object
     *
     * @SWG\Post(
     *     path="/companies/{id}/odfis",
     *     summary="Add ODFI",
     *     description="Add ODFI",
     *     operationId="OdfiPostData",
     *     tags={"odfis"},     *
     * @SWG\Parameter(
     *         description="ID of Company to fetch",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/OdfiPutData")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/OdfiGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function store(Request $request, $id)
    {
        return $this->addOdfi($request, $id);
    }
}
