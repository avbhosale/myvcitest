<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
//use Modules\Company\Models\AmendmentNote;
//use Modules\Infrastructure\Services\QueryMapper;
use Modules\Infrastructure\Services\ValidateIsExists;
use Modules\Company\Traits\AmendmentNote\AddAmendmentNote;
use Modules\Company\Repositories\AmendmentNoteRepository;
use Modules\Company\Transformers\AmendmentNoteTransformer;
use Modules\Company\Traits\AmendmentNote\AmendmentNoteValidator;
use Modules\Company\Traits\AmendmentNote\AmendmentNoteAttributes;

/**
 * AmendmentNote Controller
 *
 * @name     AmendmentNoteController
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AmendmentNoteController extends Controller
{

    use AddAmendmentNote;
    use AmendmentNoteValidator;
    use AmendmentNoteAttributes;
    use ValidateIsExists;

    private $_amendmentNoteTransformer;
    private $_amendmentNoteRepository;

    /**
     * Default constructor
     *
     * @param object $_amendmentNoteRepository  Repository object
     * @param object $_amendmentNoteTransformer Transformer object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(AmendmentNoteRepository $_amendmentNoteRepository, AmendmentNoteTransformer $_amendmentNoteTransformer)
    {
        $this->_amendmentNoteTransformer = $_amendmentNoteTransformer;
        $this->_amendmentNoteRepository = $_amendmentNoteRepository;
    }

    /**
     * To add amendment note
     *
     * @param object $request      request object
     * @param object $strCompanyId Company id
     *
     * @return object returns json object
     *
     * @SWG\Post(
     *     path="/companies/{companyId}/amendmentnote",
     *     summary="Add Business Data",
     *     description="Add Amendment Note",
     *     operationId="addAmendmentNote",
     *     tags={"amendment-note"},
     * @SWG\Parameter(
     *         description="ID of company",
     *         format="string",
     *         in="path",
     *         name="companyId",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/AddAmendmentNote")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/AddBusinessInfo")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function store(Request $request, $strCompanyId)
    {

        return $this->addAmendmentNote($request, $strCompanyId);
    }
}
