<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Company\Models\BusinessInfo;
use Modules\Company\Models\Marketing;
use Modules\Company\Models\Information;
use Modules\Infrastructure\Services\QueryMapper;
use Modules\Company\Traits\BusinessData\AddBusinessData;
use Modules\Company\Traits\BusinessData\UpdateBusinessData;
use Modules\Company\Repositories\BusinessInfoRepository;
use Modules\Company\Repositories\MarketingRepository;
use Modules\Company\Repositories\InformationRepository;
use Modules\Company\Traits\BusinessData\BusinessDataValidator;
use Modules\Company\Traits\BusinessData\BusinessDataAttributes;
use Modules\Company\Transformers\BusinessInfoTransformer;
use Modules\Company\Transformers\MarketingTransformer;
use Modules\Company\Transformers\InformationTransformer;
use Modules\Infrastructure\Services\ValidateIsExists;

/**
 * BusinessData Controller
 *
 * @name     BusinessDataController
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class BusinessDataController extends Controller
{

    private $_businessInfoRepository;
    private $_marketingRepository;
    private $_informationRepository;
    private $_businessInfoTransformer;
    private $_marketingTransformer;
    private $_informationTransformer;

    use AddBusinessData;
    use UpdateBusinessData;
    use BusinessDataValidator;
    use BusinessDataAttributes;
    use ValidateIsExists;

    /**
     * Default constructor
     *
     * @param Obj $_businessInfoRepository  businessInfo Repository object
     * @param Obj $_marketingRepository     marketing repository object
     * @param Obj $_informationRepository   information Repository object
     * @param Obj $_businessInfoTransformer businessInfo Transformer object
     * @param Obj $_marketingTransformer    marketing Transformer object
     * @param Obj $_informationTransformer  information Transformer object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(BusinessInfoRepository $_businessInfoRepository, MarketingRepository $_marketingRepository, InformationRepository $_informationRepository, BusinessInfoTransformer $_businessInfoTransformer, MarketingTransformer $_marketingTransformer, InformationTransformer $_informationTransformer)
    {
        $this->_businessInfoRepository = $_businessInfoRepository;
        $this->_marketingRepository = $_marketingRepository;
        $this->_informationRepository = $_informationRepository;
        $this->_businessInfoTransformer = $_businessInfoTransformer;
        $this->_marketingTransformer = $_marketingTransformer;
        $this->_informationTransformer = $_informationTransformer;
    }

    /**
     * To post business data
     *
     * @param object $request      request object
     * @param object $strCompanyId Company id
     *
     * @return object returns json object
     *
     * @SWG\Post(
     *     path="/companies/{companyId}/businessdata",
     *     summary="Add Business Data",
     *     description="Add Business Data",
     *     operationId="addBusinessData",
     *     tags={"business-data"},
     * @SWG\Parameter(
     *         description="ID of company",
     *         format="string",
     *         in="path",
     *         name="companyId",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/AddBusinessInfo")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/AddBusinessInfo")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function store(Request $request, $strCompanyId)
    {
        return $this->addBusinessData($request, $strCompanyId);
    }

    /**
     * To update business data
     *
     * @param object  $request   Request object
     * @param integer $companyId Company Id
     *
     * @return object returns json object
     *
     * @SWG\Put(
     *     path="/companies/{companyId}/businessdata",
     *     summary="Edit Business Data",
     *     description="Edit Business Data",
     *     operationId="editBusinessInfo",
     *     tags={"business-data"},
     * @SWG\Parameter(
     *         description="ID of company",
     *         format="string",
     *         in="path",
     *         name="companyId",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/EditBusinessInfo")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/EditBusinessInfo")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function update(Request $request, $companyId)
    {
        return $this->updateBusinessData($request, $companyId);
    }
}
