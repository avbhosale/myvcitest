<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Controller
 * @package  AssignGroup
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Controllers;

use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Modules\Company\Repositories\AssignGroupRepository;
use Modules\Company\Transformers\AssignGroupTransformer;
use Modules\Company\Traits\Group\AssignGroupValidator;
use Modules\Company\Traits\Group\AddAssignGroup;
use Modules\Company\Traits\Group\AssignGroupAttributes;
use Modules\Company\Traits\Group\ShowAssignGroup;
use Modules\Infrastructure\Services\ValidateIsExists;

/**
 * Controller assigns groups to merchant and ISO
 *
 * @name     AssignGroupController
 * @category Controller
 * @package  AssignGroupController
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AssignGroupController extends Controller
{

    private $_assignGroupRepository;
    private $_assignGroupTransformer;

    use ValidateIsExists;
    use AssignGroupAttributes;
    use AssignGroupValidator;
    use AddAssignGroup;
    use ShowAssignGroup;

    /**
     * Default parameterized constructor accepts transformer and assign-group object
     *
     * @param Obj $_assignGroupRepository  AssignGroup repository object
     * @param Obj $_assignGroupTransformer AssignGroup Transformer object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(AssignGroupRepository $_assignGroupRepository, AssignGroupTransformer $_assignGroupTransformer)
    {
        $this->_assignGroupRepository = $_assignGroupRepository;
        $this->_assignGroupTransformer = $_assignGroupTransformer;
        parent::__construct();
    }

    /**
     * Create/Update merchant/iso groups using group id
     *
     * @param object $request request object
     *
     * @return object returns json object
     *
     * @SWG\Post(
     *     path="/assigngroups",
     *     summary="Add/Remove Merchant's/ISO's assigned groups",
     *     description="Add/Remove Merchants/ISO's assigned groups",
     *     operationId="addAssignGroup",
     *     tags={"assign-groups"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/AssignGroupPostData")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/AssignGroupPostData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function store(Request $request)
    {
        return $this->addAssignGroup($request);
    }

    /**
     * To get assign-group details of merchants/iso using group ID
     *
     * @param string $id id as parameter
     *
     * @return object returns json object
     *
     * @SWG\Get(
     *     path="/assigngroups/{id}",
     *     summary="Get iso/merchants assigned groups by using Group ID",
     *     description="Returns a user based on a single ID, if the user does not have access to the pet",
     *     operationId="getAssignGroupById",
     *     tags={"assign-groups"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="ID of AssignGroup to fetch",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/AssignedGroupList")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function show(string $id)
    {
        return $this->showAssignGroup($id);
    }
}
