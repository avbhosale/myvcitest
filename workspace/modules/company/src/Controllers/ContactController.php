<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Company\Models\Address;
use Modules\Company\Models\Company;
use Modules\Company\Models\ContactAddress;
use Modules\Company\Repositories\AddressRepository;
use Modules\Company\Repositories\CompanyRepository;
use Modules\Company\Repositories\ContactRepository;
use Modules\Company\Traits\Contact\AddContact;
use Modules\Company\Traits\Contact\ContactAttributes;
use Modules\Company\Traits\Contact\ContactValidator;
use Modules\Company\Traits\Contact\EditContact;
use Modules\Company\Transformers\AddressTransformer;
use Modules\Company\Transformers\ContactTransformer;
use Modules\Company\Traits\Contact\DeleteContact;
use Modules\Infrastructure\Services\ValidateIsExists;

/**
 * Contact Controller has all CRUD methods
 *
 * @name     ContactController
 * @category Controller
 * @package  ContactController
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class ContactController extends Controller
{

    private $_contactRepository;
    private $_contactTransformer;
    private $_addressRepository;
    private $_addressTransformer;
    private $_contactAddressModel;
    private $_companyRepository;

    use ContactValidator;
    use ContactAttributes;
    use AddContact;
    use EditContact;
    use DeleteContact;
    use ValidateIsExists;

    /**
     * Default constructor
     *
     * @param Obj $_contactRepository  Contact repository object
     * @param Obj $_contactTransformer Contact Transformer object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(ContactRepository $_contactRepository, ContactTransformer $_contactTransformer)
    {
        $this->_contactRepository = $_contactRepository;
        $this->_contactTransformer = $_contactTransformer;
        $this->_addressRepository = new AddressRepository(new Address());
        $this->_addressTransformer = new AddressTransformer();
        $this->_contactAddressModel = new ContactAddress();
        $this->_companyRepository = new CompanyRepository(new Company());
        parent::__construct();
    }

    /**
     * To store contact data
     *
     * @param object $request      request object
     * @param string $strCompanyId Company ID
     *
     * @return object returns json object
     *
     * @SWG\Post(
     *     path="/companies/{companyId}/contacts",
     *     summary="Add Company Contacts",
     *     description="Create new contact data",
     *     operationId="addContactId",
     *     tags={"contacts"},
     * @SWG\Parameter(
     *         description="ID of company to fetch",
     *         format="string",
     *         in="path",
     *         name="companyId",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/ContactPostData")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/ContactGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the ‘Accept’ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function store(Request $request, $strCompanyId)
    {
        return $this->addContact($request, $strCompanyId);
    }

    /**
     * To update contact data
     *
     * @param object $request      request object
     * @param string $strCompanyId Company ID
     * @param string $strContactId Contact ID
     *
     * @return object returns json object
     *
     * @SWG\Put(
     *     path="/companies/{companyId}/contacts/{contactId}",
     *     summary="Edit Company Contacts",
     *     description="Update contact data",
     *     operationId="editContactId",
     *     tags={"contacts"},
     * @SWG\Parameter(
     *         description="ID of company to fetch",
     *         format="string",
     *         in="path",
     *         name="companyId",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         description="ID of Contact to update",
     *         format="string",
     *         in="path",
     *         name="contactId",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/ContactPutData")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/ContactGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the ‘Accept’ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function update(Request $request, $strCompanyId, $strContactId)
    {
        return $this->editContact($request, $strCompanyId, $strContactId);
    }

    /**
     * To delete contact data
     *
     * @param string $strCompanyId Company ID
     * @param string $strContactId Contact ID
     *
     * @return object returns json object
     *
     * @SWG\Delete(
     *     path="/companies/{companyId}/contacts/{contactId}",
     *     summary="Delete Company Contacts",
     *     description="Delete contact data",
     *     operationId="deleteContactId",
     *     tags={"contacts"},
     * @SWG\Parameter(
     *         description="ID of company to fetch",
     *         format="string",
     *         in="path",
     *         name="companyId",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         description="ID of Contact to delete",
     *         format="string",
     *         in="path",
     *         name="contactId",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/ContactDeleteData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the ‘Accept’ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="422",
     *         description="Un-processable entity"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function destroy($strCompanyId, $strContactId)
    {
        return $this->deleteContact($strCompanyId, $strContactId);
    }
}
