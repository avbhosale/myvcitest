<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Company\Repositories\CompanyRepository;
use Modules\Company\Repositories\VelocityCheckRepository;
use Modules\Company\Transformers\VelocityCheckTransformer;
use Modules\Company\Traits\VelocityCheck\AddVelocityCheck;
use Modules\Company\Traits\VelocityCheck\VelocityCheckAttributes;
use Modules\Company\Traits\VelocityCheck\VelocityCheckValidator;
use Modules\Infrastructure\Services\ValidateIsExists;

/**
 * Controller class for Company Module
 *
 * @name     VelocityCheckController
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class VelocityCheckController extends Controller
{

    private $_velocitycheckRepository;
    private $_companyRepository;
    private $_velocitycheckTransformer;

    use VelocityCheckValidator;
    use VelocityCheckAttributes;
    use AddVelocityCheck;
    use ValidateIsExists;

    /**
     * Default constructor function
     *
     * @param Obj $_companyRepository        Repository
     * @param Obj $_velocitycheckRepository  Repository
     * @param Obj $_velocitycheckTransformer Transformer
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct(CompanyRepository $_companyRepository, VelocityCheckRepository $_velocitycheckRepository, VelocityCheckTransformer $_velocitycheckTransformer)
    {
        $this->_velocitycheckRepository = $_velocitycheckRepository;
        $this->_velocitycheckTransformer = $_velocitycheckTransformer;
        $this->_companyRepository = $_companyRepository;
        parent::__construct();
    }

    /**
     * To Add Velocity Checks
     *
     * @param object $request request object
     * @param string $id      id
     *
     * @return object returns json object
     *
     * @SWG\Post(
     *     path="/companies/{id}/velocitychecks",
     *     summary="Add Velocity Checks",
     *     description="Velocity Checks",
     *     operationId="VelocityChecksPostData",
     *     tags={"velocitychecks"},     *
     * @SWG\Parameter(
     *         description="ID of Company to fetch",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/VelocityChecksPutData")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/VelocityChecksGetData")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function store(Request $request, $id)
    {
        //TODO : Same should be updated in cosmos db
        return $this->addVelocityCheck($request, $id);
    }
}
