<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category UnderWritingController
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Controllers;

use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Modules\Company\Traits\Underwriting\AddEditUnderWriting;
use Modules\Company\Traits\Underwriting\ShowUnderWriting;
use Modules\Company\Traits\Underwriting\UnderWritingValidator;
use Modules\Company\Traits\Underwriting\UnderWritingAttributes;
use Modules\Company\Repositories\CompanyRepository;
use Modules\Company\Transformers\UnderWritingTransformer;
use Modules\Infrastructure\Services\ValidateIsExists;

/**
 * Company underwriting created/updated
 * Company Underwriting displayed using company ID
 *
 * @name     UnderWritingController.php
 * @category Controller
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class UnderWritingController extends Controller
{

    private $_companyRepository;
    private $_underWritingTransformer;

    use UnderWritingValidator;
    use UnderWritingAttributes;
    use AddEditUnderWriting;
    use ShowUnderWriting;
    use ValidateIsExists;

    /**
     * Default constructor for create/update of underwriting
     *
     * @param Obj $companyRepository       Company       repository object
     * @param Obj $underWritingTransformer Underwriting transformer object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(CompanyRepository $companyRepository, UnderwritingTransformer $underWritingTransformer)
    {
        $this->_companyRepository = $companyRepository;
        $this->_underWritingTransformer = $underWritingTransformer;
        parent::__construct();
    }

    /**
     * Create/Update Underwriting details using company id
     *
     * @param String $id      request as Underwriting id
     * @param object $request request object
     *
     * @return object returns json object
     *
     * @SWG\POST(
     *     path="/companies/{id}/underwritings",
     *     summary="Add/Update company (Merchant/ISO) Underwriting",
     *     description="Add/Update company (Merchant/ISO) Underwriting",
     *     operationId="addUnderwriting",
     *     tags={"underwritings"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="ID of company",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Parameter(
     *         name="body",
     *         in="body",
     * @SWG\Schema(ref="#/definitions/CompanyUnderwriting")
     *     ),
     *     produces={
     *         "application/json"
     *     },
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/CompanyUnderwriting")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function store($id, Request $request)
    {
        return $this->addEditUnderWriting($id, $request);
    }

    /**
     * Get Company Underwriting details using company id
     *
     * @param String $id      request as Underwriting id
     * @param object $request request object
     *
     * @return object returns json object
     *
     * @SWG\GET(
     *     path="/companies/{id}/underwritings",
     *     summary="Get company (Merchant/ISO) Underwriting",
     *     description="Get company (Merchant/ISO) Underwriting",
     *     operationId="show",
     *     tags={"underwritings"},
     * @SWG\Parameter(
     *         in="header",
     *         name="VeriCheck-Version",
     *         type="string",
     *         default="1.0.0",
     *     ),
     * @SWG\Parameter(
     *         description="ID of company",
     *         format="string",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     * @SWG\Response(
     *         response=200,
     *         description="OK - Everything worked as expected",
     * @SWG\Schema(
     *         type="object",
     *         ref="#/definitions/GetCompanyUnderwriting")
     *     ),
     * @SWG\Response(
     *         response="400",
     *         description="Bad Request - Often due to a missing request parameter"
     *     ),
     * @SWG\Response(
     *         response="401",
     *         description="Unauthorized - An invalid element token, user secret and/or org secret provided"
     *     ),
     * @SWG\Response(
     *         response="403",
     *         description="Forbidden - Access to the resource by the provider is forbidden"
     *     ),
     * @SWG\Response(
     *         response="404",
     *         description="Not found - The requested resource is not found"
     *     ),
     * @SWG\Response(
     *         response="405",
     *         description="Method not allowed - Incorrect HTTP verb used, e.g., GET used when POST expected"
     *     ),
     * @SWG\Response(
     *         response="406",
     *         description="Not acceptable - The response content type does not match the â€˜Acceptâ€™ header value"
     *     ),
     * @SWG\Response(
     *         response="409",
     *         description="Conflict - If a resource being created already exists"
     *     ),
     * @SWG\Response(
     *         response="415",
     *         description="Unsupported media type - The server cannot handle the requested Content-Type"
     *     ),
     * @SWG\Response(
     *         response="500",
     *         description="Server error - Something went wrong on the Cloud Elements server"
     *     )
     * )
     */
    public function show($id, Request $request)
    {
        return $this->showUnderWriting($id, $request);
    }
}
