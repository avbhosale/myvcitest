<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Route
 * @package  Authentication
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
$api = app('Dingo\Api\Routing\Router');
$defaultApiVersion = config('api.version');

$api->version($defaultApiVersion, function ($api) {
    $limit = config('api.throttling.limit');
    $expires = config('api.throttling.expires');

    $api->group(['namespace' => '\Modules\Company\Controllers', 'middleware' => 'api.throttle', 'limit' => $limit, 'expires' => $expires], function () use ($api) {
        // api for company
        $api->get('/companies', ['uses' => 'CompanyController@index',]);
        $api->post('/companies', ['uses' => 'CompanyController@store',]);
        $api->get('/companies/{id}', ['uses' => 'CompanyController@show',]);
        $api->put('/companies/{id}', ['uses' => 'CompanyController@update',]);
        $api->delete('/companies/{id}', ['uses' => 'CompanyController@destroy',]);
        $api->put('/companies/{id}/activate', ['uses' => 'CompanyController@activate',]);
        $api->put('/companies/{id}/deactivate', ['uses' => 'CompanyController@deactivate',]);


        //API for Company Contact
        $api->post('companies/{companyId}/contacts', ['uses' => 'ContactController@store',]);
        $api->put('companies/{companyId}/contacts/{id}', ['uses' => 'ContactController@update',]);
        $api->delete('companies/{companyId}/contacts/{id}', ['uses' => 'ContactController@destroy',]);

        //API for Company Velocity Checks
        $api->post('companies/{companyId}/velocitychecks', ['uses' => 'VelocityCheckController@store',]);

        //API for company address
        $api->post('companies/{companyId}/addresses', ['uses' => 'CompanyAddressController@store',]);
        $api->put('companies/{companyId}/addresses/{id}', ['uses' => 'CompanyAddressController@update',]);
        $api->delete('companies/{companyId}/addresses/{id}', ['uses' => 'CompanyAddressController@destroy',]);

        //API for assign group
        $api->post('/assigngroups', ['uses' => 'AssignGroupController@store',]);
        $api->get('/assigngroups/{id}', ['uses' => 'AssignGroupController@show',]);

        //API for company operations
        $api->post('/companies/{id}/operations', ['uses' => 'OperationController@store',]);
        $api->get('/companies/{id}/operations', ['uses' => 'OperationController@show',]);

        // API for company underwriting
        $api->post('/companies/{id}/underwritings', ['uses' => 'UnderWritingController@store',]);
        $api->get('/companies/{id}/underwritings', ['uses' => 'UnderWritingController@show',]);

        //API for Company ODFIs
        $api->get('companies/{companyId}/odfis', ['uses' => 'OdfiController@show',]);
        $api->post('companies/{companyId}/odfis', ['uses' => 'OdfiController@store',]);

        // API for company bank
        $api->post('/companies/{id}/banks', ['uses' => 'CompanyBankController@store',]);
        $api->get('/companies/{id}/banks', ['uses' => 'CompanyBankController@show',]);

        // API for business data
        $api->post('/companies/{id}/businessdata', ['uses' => 'BusinessDataController@store',]);
        $api->put('/companies/{id}/businessdata', ['uses' => 'BusinessDataController@update',]);


        // API for business data
        $api->get('/companies/{id}/amendmentnote', ['uses' => 'AmendmentNoteController@show',]);
        $api->post('/companies/{id}/amendmentnote', ['uses' => 'AmendmentNoteController@store',]);
    });
});
