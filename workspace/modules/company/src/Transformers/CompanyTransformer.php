<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  CompanyTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Modules\Infrastructure\Services\TransformRequest;
use Modules\Company\Models\Company;
use League\Fractal\TransformerAbstract;

/**
 * Two line description of class
 *
 * @name     CompanyTransformer.php
 * @category Transformer
 * @package  CompanyTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CompanyTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $company Company model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Company $company)
    {
        $formattedCompany = [
            'id' => $company->CompanyId,
            'parent_id' => $company->ParentId,
            'type' => $company->Type,
            'odfi_id' => $company->OdfiId,
            'name' => $company->Name,
            'doing_business_as' => $company->DoingBusinessAs,
            'tax_id' => $company->TaxId,
            'website_url' => $company->WebsiteUrl,
            'pin_number' => $company->PinNumber,
            'uuid' => $company->Uuid,
            'gateway_id' => $company->GatewayId,
            'can_issue_credit' => $company->CanIssueCredit,
            'can_issue_credit_approved' => $company->CanIssueCreditApproved,
            'can_issue_credit_active' => $company->CanIssueCreditActive,
            'has_bankstatements' => $company->HasBankstatements,
            'has_driver_license' => $company->HasDriverLicense,
            'has_business_license' => $company->HasBusinessLicense,
            'has_other' => $company->HasOther,
            'other' => $company->Other,
            'reason_for_deactivation' => $company->ReasonForDeactivation,
            'status' => $company->Status,
            'created_by' => $company->CreatedBy,
            'created_at' => $company->CreatedAt,
            'etag' => $company->Etag,
            'deleted_at' => $company->DeletedAt
        ];

        return $formattedCompany;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Obj $input input
     *
     * @name   transformRequestParameters
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input)
    {
        $companyData = [];
        $companyData = $this->getTransformRequest($input, $this->_transformRequest());
        return $companyData;
    }

    /**
     * Function is used to declare company table fields for transformer request
     *
     * @name   _transformRequest
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _transformRequest()
    {
        return $arrTransformRequest = [
            'id' => 'CompanyId',
            'parent_id' => 'ParentId',
            'type' => 'Type',
            'odfi_id' => 'OdfiId',
            'name' => 'Name',
            'doing_business_as' => 'DoingBusinessAs',
            'tax_id' => 'TaxId',
            'website_url' => 'WebsiteUrl',
            'pin_number' => 'PinNumber',
            'uuid' => 'Uuid',
            'gateway_id' => 'GatewayId',
            'can_issue_credit' => 'CanIssueCredit',
            'can_issue_credit_approved' => 'CanIssueCreditApproved',
            'can_issue_credit_active' => 'CanIssueCreditActive',
            'has_bankstatements' => 'HasBankstatements',
            'has_driver_license' => 'HasDriverLicense',
            'has_business_license' => 'HasBusinessLicense',
            'has_other' => 'HasOther',
            'other' => 'Other',
            'reason_for_deactivation' => 'ReasonForDeactivation',
            'status' => 'Status',
            'created_by' => 'CreatedBy',
            'created_at' => 'CreatedAt',
            'etag' => 'Etag',
            'deleted_at' => 'DeletedAt'
        ];
    }
}
