<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  BusinessInfoTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;
use Modules\Company\Models\BusinessInfo;
use Modules\Infrastructure\Services\TransformRequest;

/**
 * Principal Transformer to transform database fields to API
 *
 * @name     BusinessInfoTransformer
 * @category Transformer
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class BusinessInfoTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $businessInfo businessInfo model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(BusinessInfo $businessInfo)
    {
        $formattedBusinessInfo = [
            'id' => $businessInfo->BusinessInfoId,
            'description' => $businessInfo->Description,
            'business_type' => $businessInfo->BusinessType,
            'product_service' => $businessInfo->ProductService,
            'naics_code' => $businessInfo->NaicsCode,
            'corporate_structure_id' => $businessInfo->CorporateStructureId,
            'business_start_date' => $businessInfo->BusinessStartDate,
            'company_id' => $businessInfo->CompanyId,
            'effective_start_date' => $businessInfo->EffectiveStartDate,
            'effective_end_date' => $businessInfo->EffectiveEndDate,
            'expected_total_transactions_per_month' => $businessInfo->expected_total_transactions_per_month,
            'expected_total_dollars_per_month' => $businessInfo->expected_total_dollars_per_month,
            'expected_txn_per_month' => $businessInfo->expected_txn_per_month,
            'expected_amount_per_month' => $businessInfo->expected_amount_per_month,
            'average_txn_amount' => $businessInfo->average_txn_amount,
            'etag' => $businessInfo->Etag,
        ];

        return $formattedBusinessInfo;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input)
    {
        $businessInfoData = $this->getTransformRequest($input, $this->businessInfoTransformRequest());
        return $businessInfoData;
    }

    /**
     * Function is used to declare BusinessInfo table fields for transformer request
     *
     * @name   businessInfoTransformRequest
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function businessInfoTransformRequest()
    {
        return $arrTransformRequest = [
            'id' => 'BusinessInfoId',
            'description' => 'Description',
            'business_type' => 'BusinessType',
            'product_service' => 'ProductService',
            'naics_code' => 'NaicsCode',
            'corporate_structure_id' => 'CorporateStructureId',
            'business_start_date' => 'BusinessStartDate',
            'company_id' => 'CompanyId',
            'effective_start_date' => 'EffectiveStartDate',
            'effective_end_date' => 'EffectiveEndDate',
            'etag' => 'Etag'
        ];
    }
}
