<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  InformationTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;
use Modules\Company\Models\Information;
use Modules\Infrastructure\Services\TransformRequest;

/**
 * Principal Transformer to transform database fields to API
 *
 * @name     InformationTransformer
 * @category Transformer
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class InformationTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $information information model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Information $information)
    {
        $formattedInformation = [
            'id' => $information->InformationId,
            'company_id' => $information->CompanyId,
            'attribute' => $information->Attribute,
            'value' => $information->Value,
            'type' => $information->Type
        ];

        return $formattedInformation;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $informationData
     */
    public function transformRequestParameters($input)
    {
        $informationData = $this->getTransformRequest($input, $this->informationTransformRequest());
        return $informationData;
    }

    /**
     * Function is used to declare information table fields for transformer request
     *
     * @name   informationTransformRequest
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function informationTransformRequest()
    {
        return $arrTransformRequest = [
            'InformationId' => 'InformationId',
            'company_id' => 'CompanyId',
            'attribute' => 'Attribute',
            'value' => 'Value',
            'type' => 'Type'
        ];
    }
}
