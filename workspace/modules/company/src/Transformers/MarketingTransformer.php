<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  MarketingTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;
use Modules\Company\Models\Marketing;
use Modules\Infrastructure\Services\TransformRequest;

/**
 * Principal Transformer to transform database fields to API
 *
 * @name     MarketingTransformer
 * @category Transformer
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class MarketingTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $marketing marketing model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Marketing $marketing)
    {
        $formattedMarketing = [
            'id' => $marketing->MarketingId,
            'company_id' => $marketing->CompanyId,
            'target_market' => $marketing->TargetMarket,
            'projected_applications' => $marketing->ProjectedApplications,
            'expected_txn_per_month' => $marketing->ExpectedTxnPerMonth,
            'expected_amount_per_month' => $marketing->ExpectedAmountPerMonth,
            'average_txn_amount' => $marketing->AverageTxnAmount,
            'portfolio_low' => $marketing->PortfolioLow,
            'portfolio_high' => $marketing->PortfolioHigh,
            'portfolio_internet' => $marketing->PortfolioInternet,
            'geographic_area' => $marketing->GeographicArea,
            'marketing_method' => $marketing->MarketingMethod,
            'portfolio_other' => $marketing->PortfolioOther,
            'site_visit_performed' => $marketing->SiteVisitPerformed,
            'etag' => $marketing->Etag,
        ];

        return $formattedMarketing;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input)
    {
        $marketingData = $this->getTransformRequest($input, $this->marketingTransformRequest());
        return $marketingData;
    }

    /**
     * Function is used to declare marketing table fields for transformer request
     *
     * @name   marketingTransformRequest
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function marketingTransformRequest()
    {
        return $arrTransformRequest = [
            'marketing_id' => 'MarketingId',
            'company_id' => 'CompanyId',
            'target_market' => 'TargetMarket',
            'projected_applications' => 'ProjectedApplications',
            'expected_txn_per_month' => 'ExpectedTxnPerMonth',
            'expected_amount_per_month' => 'ExpectedAmountPerMonth',
            'average_txn_amount' => 'AverageTxnAmount',
            'portfolio_low' => 'PortfolioLow',
            'portfolio_high' => 'PortfolioHigh',
            'portfolio_internet' => 'PortfolioInternet',
            'portfolio_other' => 'PortfolioOther',
            'geographic_area' => 'GeographicArea',
            'marketing_method' => 'MarketingMethod',
            'site_visit_performed' => 'SiteVisitPerformed',
            'etag' => 'Etag'
        ];
    }
}
