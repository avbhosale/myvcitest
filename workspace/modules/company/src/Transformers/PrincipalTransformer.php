<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  ContactTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;
use Modules\Company\Models\Principal;
use Modules\Infrastructure\Services\TransformRequest;

/**
 * Principal Transformer to transform database fields to API
 *
 * @name     PrincipalTransformer.php
 * @category Transformer
 * @package  PrincipalTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class PrincipalTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $principal principal model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Principal $principal)
    {
        $formattedPrincipal = [
            'id' => $principal->PrincipalId,
            'contact_id' => $principal->ContactId,
            'ssn' => $principal->SocialSecurityNumber,
            'driver_license_number' => $principal->DriverLicenseNumber,
            'driver_license_state' => $principal->DriverLicenseState,
            'ownership_percentage' => $principal->OwnershipPercentage,
            'effective_start_date' => $principal->EffectiveStartDate,
            'effective_end_date' => $principal->EffectiveEndDate,
            'etag' => $principal->Etag
        ];

        return $formattedPrincipal;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input)
    {
        $principalData = $this->getTransformRequest($input, $this->principalTransformRequest());
        return $principalData;
    }

    /**
     * Function is used to declare principal table fields for transformer request
     *
     * @name   principalTransformRequest
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function principalTransformRequest()
    {
        return $arrTransformRequest = [
            'id' => 'PrincipalId',
            'contact_id' => 'ContactId',
            'ssn' => 'SocialSecurityNumber',
            'driver_license_number' => 'DriverLicenseNumber',
            'driver_license_state' => 'DriverLicenseState',
            'ownership_percentage' => 'OwnershipPercentage',
            'effective_start_date' => 'EffectiveStartDate',
            'effective_end_date' => 'EffectiveEndDate',
            'etag' => 'Etag'
        ];
    }
}
