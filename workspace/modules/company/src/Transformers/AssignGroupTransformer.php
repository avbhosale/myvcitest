<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  AssignGroupTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Modules\Company\Models\AssignGroup;
use League\Fractal\TransformerAbstract;

/**
 * Two line description of class
 *
 * @name     AssignGroupTransformer.php
 * @category Transformer
 * @package  AssignGroupTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AssignGroupTransformer extends TransformerAbstract
{

    /**
     * Function to set transform format
     *
     * @param Model $assignGroup AssignGroup model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(AssignGroup $assignGroup)
    {
        $formattedAssignGroup = [
            'id' => $assignGroup->CompanyGroupId,
            'group_id' => $assignGroup->GroupId,
            'company_id' => $assignGroup->CompanyId,
            'company_name' => $assignGroup->Name,
        ];

        return $formattedAssignGroup;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array  $input  array of input received
     * @param String $action action string create or update
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input, $action = 'update')
    {
        $assignGroupData = [
            'GroupId' => $input['group_id'],
            'CompanyId' => $input['company_ids']
        ];
        return $assignGroupData;
    }
}
