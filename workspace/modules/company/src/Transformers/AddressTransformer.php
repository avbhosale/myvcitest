<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  AddressTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Modules\Infrastructure\Services\TransformRequest;
use Modules\Company\Models\Address;
use League\Fractal\TransformerAbstract;

/**
 * Address Transformer to transform DB fields to API and vice versa
 *
 * @name     AddressTransformer.php
 * @category Transformer
 * @package  Company_Address
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AddressTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $address Address model
     *
     * @name   addressTransform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Address $address)
    {
        $formattedAddress = [
            'id' => $address->AddressId,
            'address_line1' => $address->AddressLine1,
            'address_line2' => $address->AddressLine2,
            'city' => $address->City,
            'state' => $address->State,
            'zip' => $address->Zip,
            'primary_phone' => $address->PrimaryPhone,
            'secondary_phone' => $address->SecondaryPhone,
            'primary_email' => $address->PrimaryEmail,
            'secondary_email' => $address->SecondaryEmail,
            'fax' => $address->Fax,
            'address_type' => $address->Type,
            'created_at' => $address->CreatedAt,
            'etag' => $address->Etag
        ];

        return $formattedAddress;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input)
    {
        $addressData = [];
        $addressData = $this->getTransformRequest($input, $this->_addressTransformRequest());
        return $addressData;
    }

    /**
     * Function is used to declare address table fields for transformer request
     *
     * @name   _addressTransformRequest
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _addressTransformRequest()
    {
        return $arrTransformRequest = [
            'id' => 'AddressId',
            'address_line1' => 'AddressLine1',
            'address_line2' => 'AddressLine2',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip',
            'primary_phone' => 'PrimaryPhone',
            'secondary_phone' => 'SecondaryPhone',
            'primary_email' => 'PrimaryEmail',
            'secondary_email' => 'SecondaryEmail',
            'fax' => 'Fax',
            'address_type' => 'Type',
            'etag' => 'Etag',
            'created_at' => 'CreatedAt',
        ];
    }
}
