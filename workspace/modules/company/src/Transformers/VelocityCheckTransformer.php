<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  VelocityCheckTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Modules\Infrastructure\Services\TransformRequest;
use Modules\Company\Models\VelocityChecks;
use League\Fractal\TransformerAbstract;

/**
 * Two line description of class
 *
 * @name     VelocityCheckTransformer.php
 * @category Transformer
 * @package  VelocityCheckTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class VelocityCheckTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $velocityChecks VelocityChecks model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(VelocityChecks $velocityChecks)
    {
        $formattedCompany = [
            'id' => $velocityChecks->AchVelocityCheckId,
            'company_id' => $velocityChecks->CompanyId,
            'has_approved' => $velocityChecks->HasApproved,
            'has_check_duplicate' => $velocityChecks->HasCheckDuplicate,
            'has_nsf_decline' => $velocityChecks->HasNsfDecline,
            'has_default_velocity' => $velocityChecks->HasDefaultVelocity,
            'max_checks_per_day' => $velocityChecks->MaxChecksPerDay,
            'max_amount_per_day' => $velocityChecks->MaxAmountPerDay,
            'day_period_merchant' => $velocityChecks->DayPeriodMerchant,
            'max_checks_per_period' => $velocityChecks->MaxChecksPerPeriod,
            'max_amount_per_period' => $velocityChecks->MaxAmountPerPeriod,
            'max_checks_per_day_per_acc' => $velocityChecks->MaxChecksPerPeriodPerAcc,
            'max_amount_per_day_per_acc' => $velocityChecks->MaxAmountPerDayPerAcc,
            'day_period_account' => $velocityChecks->DayPeriodAcc,
            'max_checks_per_period_per_acc' => $velocityChecks->MaxChecksPerPeriodPerAcc,
            'max_amount_per_period_per_acc' => $velocityChecks->MaxAmountPerPeriodPerAcc,
            'heighest_dollar_amount_per_txn' => $velocityChecks->HighestDollarAmountPerTxn,
            'funding_time_id' => $velocityChecks->FundingTimeId,
            'type' => $velocityChecks->Type,
            'effective_start_date' => $velocityChecks->EffectiveStartDate,
            'effective_end_date' => $velocityChecks->EffectiveEndDate,
            'etag' => $velocityChecks->Etag
        ];

        return $formattedCompany;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Obj $input  input
     * @param Obj $action action
     *
     * @name   transformRequestParameters
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input, $action = 'update')
    {
        $velocityCheckData = [];
        $velocityCheckData = $this->getTransformRequest($input, $this->_transformVelocityRequest());
        return $velocityCheckData;
    }

    /**
     * Function is used to declare company table fields for transformer request
     *
     * @name   _transformVelocityRequest
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _transformVelocityRequest()
    {
        return $arrTransformRequest = [
            'id' => 'AchVelocityCheckId',
            'company_id' => 'CompanyId',
            'has_approved' => 'HasApproved',
            'has_check_duplicate' => 'HasCheckDuplicate',
            'has_nsf_decline' => 'HasNsfDecline',
            'has_default_velocity' => 'HasDefaultVelocity',
            'max_checks_per_day' => 'MaxChecksPerDay',
            'max_amount_per_day' => 'MaxAmountPerDay',
            'day_period_merchant' => 'DayPeriodMerchant',
            'max_checks_per_period' => 'MaxChecksPerPeriod',
            'max_amount_per_period' => 'MaxAmountPerPeriod',
            'max_checks_per_day_per_acc' => 'MaxChecksPerDayPerAcc',
            'max_amount_per_day_per_acc' => 'MaxAmountPerDayPerAcc',
            'day_period_account' => 'DayPeriodAcc',
            'max_checks_per_period_per_acc' => 'MaxChecksPerPeriodPerAcc',
            'max_amount_per_period_per_acc' => 'MaxAmountPerPeriodPerAcc',
            'heighest_dollar_amount_per_txn' => 'HighestDollarAmountPerTxn',
            'funding_time_id' => 'FundingTimeId',
            'type' => 'Type',
            'effective_start_date' => 'EffectiveStartDate',
            'effective_end_date' => 'EffectiveEndDate',
            'etag' => 'Etag'
        ];
    }
}
