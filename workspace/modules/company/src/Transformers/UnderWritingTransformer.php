<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  UnderWritingTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Modules\Infrastructure\Services\TransformRequest;
use Modules\Company\Models\Company;
use League\Fractal\TransformerAbstract;

/**
 * Company UnderWritingTransformer exchanges input and output request params
 *
 * @name     UnderWritingTransformer.php
 * @category Transformer
 * @package  UnderWritingTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class UnderWritingTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $underWriting Operation model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Company $underWriting)
    {
        $formattedOperation = [
            'id' => $underWriting->CompanyId,
            'has_bank_statements' => $underWriting->HasBankstatements,
            'has_driver_license' => $underWriting->HasDriverLicense,
            'has_business_license' => $underWriting->HasBusinessLicense,
            'has_other' => $underWriting->HasOther,
            'created_at' => $underWriting->CreatedAt,
            'etag' => $underWriting->Etag
        ];
        return $formattedOperation;
    }

    /**
     * Function is used to transform operation fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $underWritingData
     */
    public function transformRequestParameters($input = [])
    {
        $arrTransformRequest = [
            'has_bank_statements' => 'HasBankstatements',
            'has_driver_license' => 'HasDriverLicense',
            'has_business_license' => 'HasBusinessLicense',
            'has_other' => 'HasOther'
        ];
        $underWritingData = $this->getTransformRequest($input, $arrTransformRequest);
        return $underWritingData;
    }
}
