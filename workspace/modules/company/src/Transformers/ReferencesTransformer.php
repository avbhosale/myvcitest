<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  ContactTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;
use Modules\Company\Models\References;
use Modules\Infrastructure\Services\TransformRequest;

/**
 * References Transformer to transform database fields to API
 *
 * @name     ReferencesTransformer
 * @category Transformer
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class ReferencesTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $references references model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(References $references)
    {
        $formattedReferences = [
            'id' => $references->ReferencesId,
            'contact_id' => $references->ContactId,
            'account_number' => $references->AccountNumber,
            'has_approved' => $references->HasApproved,
            'approved_by' => $references->ApprovedBy,
            'approved_at' => $references->ApprovedAt,
            'etag' => $references->Etag
        ];

        return $formattedReferences;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input)
    {
        $referencesData = $this->getTransformRequest($input, $this->referencesTransformRequest());
        return $referencesData;
    }

    /**
     * Function is used to declare references table fields for transformer request
     *
     * @name   referencesTransformRequest
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function referencesTransformRequest()
    {
        return $arrTransformRequest = [
            'id' => 'ReferencesId',
            'contact_id' => 'ContactId',
            'account_number' => 'AccountNumber',
            'has_approved' => 'HasApproved',
            'approved_by' => 'ApprovedBy',
            'approved_at' => 'ApprovedAt',
            'etag' => 'Etag'
        ];
    }
}
