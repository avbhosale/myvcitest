<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  OdfiTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Modules\Infrastructure\Services\TransformRequest;
use Modules\Company\Models\Odfi;
use League\Fractal\TransformerAbstract;

/**
 * Two line description of class
 *
 * @name     OdfiTransformer.php
 * @category Transformer
 * @package  OdfiTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class OdfiTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $odfi Odfi model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Odfi $odfi)
    {
        $formattedOdfi = [
            'id' => $odfi->FedWindowId,
            'company_id' => $odfi->CompanyId,
            'fed_window_data' => $odfi->OdfiFedWindowId,
            'has_enable' => $odfi->HasEnable,
            'type' => $odfi->Type,
            'etag' => $odfi->Etag
        ];

        return $formattedOdfi;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Obj $input  input
     * @param Obj $action action
     *
     * @name   transformRequestParameters
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input, $action = 'update')
    {
        $odfiData = [];
        $odfiData = $this->getTransformRequest($input, $this->_transformOdfiRequest());
        return $odfiData;
    }

    /**
     * Function is used to declare company table fields for transformer request
     *
     * @name   _transformOdfiRequest
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _transformOdfiRequest()
    {
        return $arrTransformRequest = [
            'id' => 'FedWindowId',
            'company_id' => 'CompanyId',
            'fed_window_data' => 'OdfiFedWindowId',
            'has_enable' => 'HasEnable',
            'type' => 'Type',
            'etag' => 'Etag'
        ];
    }
}
