<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  OperationTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Modules\Infrastructure\Services\TransformRequest;
use Modules\Company\Models\Company;
use League\Fractal\TransformerAbstract;

/**
 * Two line description of class
 *
 * @name     OperationTransformer.php
 * @category Transformer
 * @package  OperationTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class OperationTransformer extends TransformerAbstract
{

    use TransformRequest;

    /**
     * Function to set transform format
     *
     * @param Model $operation Operation model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Company $operation)
    {
        $formattedOperation = [
            'id' => $operation->CompanyId,
            'gateway_id' => $operation->GatewayId,
            'can_issue_credit' => $operation->CanIssueCredit,
            'can_issue_credit_approved' => $operation->CanIssueCreditApproved,
            'can_issue_credit_active' => $operation->CanIssueCreditActive,
            'reason_for_deactivation' => $operation->ReasonForDeactivation,
            'created_at' => $operation->CreatedAt,
            'etag' => $operation->Etag
        ];
        return $formattedOperation;
    }

    /**
     * Function is used to transform operation fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $operationData
     */
    public function transformRequestParameters($input = [])
    {
        $arrTransformRequest = [
            'gateway_id' => 'GatewayId',
            'can_issue_credit' => 'CanIssueCredit',
            'can_issue_credit_approved' => 'CanIssueCreditApproved',
            'can_issue_credit_active' => 'CanIssueCreditActive',
            'reason_for_deactivation' => 'ReasonForDeactivation'
        ];
        $operationData = $this->getTransformRequest($input, $arrTransformRequest);
        return $operationData;
    }
}
