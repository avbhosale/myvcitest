<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Transformer
 * @package  ContactTransformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;
use Modules\Company\Models\Contact;
use Modules\Company\Traits\Contact\ContactRelationships;
use Modules\Infrastructure\Services\TransformRequest;

/**
 * Contact Transformer to transform DB fields to API and vice versa
 *
 * @name     ContactTransformer.php
 * @category Transformer
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class ContactTransformer extends TransformerAbstract
{

    use TransformRequest;
    use ContactRelationships;

    /**
     * Function to set transform format
     *
     * @param Model $contact Contact model
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function transform(Contact $contact)
    {
        $formattedContact = [
            'id' => $contact->ContactId,
            'company_id' => $contact->CompanyId,
            'first_name' => $contact->FirstName,
            'last_name' => $contact->LastName,
            'phone' => $contact->Phone,
            'mobile' => $contact->Mobile,
            'primary_email' => $contact->PrimaryEmail,
            'secondary_email' => $contact->SecondaryEmail,
            'fax' => $contact->Fax,
            'email_format' => $contact->EmailFormat,
            'contact_type' => $contact->Type,
            'created_at' => $contact->CreatedAt,
            'etag' => $contact->Etag,
            'address' => $this->includeAddresses($contact)
        ];

        switch ($contact->Type) {
            case 'principal':
                $formattedContact = array_merge($formattedContact, ['principal' => $this->includePrincipal($contact)]);
                break;
            case 'references':
                $formattedContact = array_merge($formattedContact, ['references' => $this->includeReferences($contact)]);
                break;
        }

        return $formattedContact;
    }

    /**
     * Function is used to transform user fields to table fields
     *
     * @param Array $input array of input received
     *
     * @name   transformRequestParameters
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transformRequestParameters($input)
    {
        $contactData = $this->getTransformRequest($input, $this->contactTransformRequest());
        return $contactData;
    }

    /**
     * Function is used to declare address table fields for transformer request
     *
     * @name   contactTransformRequest
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function contactTransformRequest()
    {
        return $arrTransformRequest = [
            'id' => 'ContactId',
            'company_id' => 'CompanyId',
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'primary_email' => 'PrimaryEmail',
            'secondary_email' => 'SecondaryEmail',
            'fax' => 'Fax',
            'email_format' => 'EmailFormat',
            'contact_type' => 'Type',
            'created_at' => 'CreatedAt',
            'etag' => 'Etag'
        ];
    }
}
