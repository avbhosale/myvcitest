<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category VelocityCheckValidator
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Odfi;

use Modules\Company\Models\Company;

/**
 * Master group module methods
 *
 * @name     OdfiValidator
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait OdfiValidator
{

    /**
     * This function is used for API validation
     *
     * @param array $action request array
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules($action)
    {
        $table = config('database.default') . '.' . $this->_companyRepository->getTableName();
        $rules = [];

        switch ($action) {
            case 'create':
                $rules = [
                    'odfi_fed_window_id' => 'bail|required|regex:' . static::$REGEX_UUID . '|max:64',
                    'has_enable' => 'bail|required|numeric'
                ];
                break;
            case 'update':
                $rules = [
                    'odfi_fed_window_id' => 'bail|required|regex:' . static::$REGEX_UUID . '|max:64',
                    'has_enable' => 'bail|required|numeric'
                ];
                break;
        }

        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @param array $action request array
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage($action)
    {
        $messages = [];
        switch ($action) {
            case 'create':
                $messages = [
                    'type.min' => 'The :attribute must be either 1 or 2.',
                    'type.max' => 'The :attribute must be either 1 or 2.',
                    'status.min' => 'The :attribute must be either 1 or 0.',
                    'status.max' => 'The :attribute must be either 1 or 0.',
                ];
                break;
            case 'update':
                $messages = [
                    'type.min' => 'The :attribute must be either 1 or 2.',
                    'type.max' => 'The :attribute must be either 1 or 2.',
                    'status.min' => 'The :attribute must be either 1 or 0.',
                    'status.max' => 'The :attribute must be either 1 or 0.',
                ];
                break;
        }
        return $messages;
    }
}
