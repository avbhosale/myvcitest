<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Odfi;

use Modules\Company\Models\Odfi;

/**
 * Master Company module methods
 *
 * @name     OdfiAttributes
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait OdfiAttributes
{

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributes()
    {
        $attributes = [];
        $attributes = [
            'CompanyId' => 'Company Id',
            'OdfiFedWindowId' => 'Odfi fed window Id',
            'HasEnable' => 'Has Enable',
            'Type' => 'Type',
            'Etag' => 'Etag'
        ];

        return $attributes;
    }

    /**
     * Function set attribute codes for validation messages
     *
     * @name   _setAttributeCode
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributeCode()
    {
        $odfis = new Odfi();
        return $odfis->odfiErrCodes;
    }
}
