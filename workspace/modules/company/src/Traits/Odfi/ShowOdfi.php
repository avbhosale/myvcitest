<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Odfi;

use Illuminate\Database\QueryException;
use Modules\Company\Models\Company;
use Modules\Company\Models\Odfi;

/**
 * Master company module show methods
 *
 * @name     showOdfi
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ShowOdfi
{

    /**
     * Show ODFI by Company ID
     *
     * @param String $id Company ID
     *
     * @name   showOdfi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function showOdfi($id)
    {
        try {
            // check for valid company id
            $odfi = $this->checkRecordExistsById(new Company(), $id);
            $odfi = $this->_odfiRepository->findBy(['CompanyId' => $id]);
            return $this->response->item($odfi, $this->_odfiTransformer);
        } catch (QueryException $exception) {
            return $this->response->errorInternal();
        }
    }
}
