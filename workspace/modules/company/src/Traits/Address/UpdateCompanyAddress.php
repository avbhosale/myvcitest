<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Address;

use Illuminate\Database\ClientException;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Modules\Company\Models\Address;
use Modules\Company\Models\CompanyAddress;
use Modules\Company\Models\Company;

/**
 * Company Address module methods
 *
 * @name     UpdateAddress
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait UpdateCompanyAddress
{

    /**
     * Update Company Address Method
     *
     * @param Obj    $request      Request Object
     * @param Obj    $id           id integer
     * @param String $strCompanyId company id
     *
     * @name   updateCompanyAddress
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function updateCompanyAddress(Request $request, $id, $strCompanyId)
    {
        try {
            // check for valid company id
            $objCompany = $this->checkRecordExistsById(new Company(), $strCompanyId);

            // check for valid address id
            $address = $this->checkRecordExistsById(new Address(), $id);

            $validatorResponse = $this->validateRequest($request, $this->_addressValidationRules($id), $this->_addressValidationMessage(), $this->_setAddressAttributes(), $this->_setAddressAttributeCode());

            $objCompanyAddress = CompanyAddress::Where('CompanyId', '=', $objCompany["CompanyId"])->Where('AddressId', '=', $id)->first();

            if (!$objCompanyAddress instanceof CompanyAddress) {
                return $this->errorNotFound("The company address doesn't exist.");
            }

            $address = $this->_addressRepository->update($address, $this->_addressTransformer->transformRequestParameters($request->all()));
            return $this->response->item($address, $this->_addressTransformer, ['key' => 'address']);
        } catch (ClientException $exception) {
            $this->errorUnauthorized('Invalid Access Token');
        }
    }
}
