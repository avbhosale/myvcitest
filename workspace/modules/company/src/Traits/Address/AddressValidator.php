<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Address;

use Illuminate\Validation\Rule;
use Modules\Company\Models\Address;

/**
 * Master group module methods
 *
 * @name     Address
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddressValidator
{

    /**
     * This function is used for API validation for Address
     *
     * @param array $addressAction request array
     *
     * @name   _addressValidationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _addressValidationRules($addressAction)
    {
        $objAddress = new Address();

        $addressRules = [];
        $addressRules = [
            'address_line1' => 'bail|required|string|max:1024',
            'address_line2' => 'bail|required|string|max:1024',
            'city' => 'bail|required|string|max:64',
            'state' => 'bail|required|alpha|max:2',
            'zip' => 'bail|required|numeric|digits:5',
            'primary_phone' => 'bail|digits:10|numeric|nullable',
            'secondary_phone' => 'bail|digits:10|numeric|nullable',
            'primary_email' => 'bail|email|max:256|nullable',
            'secondary_email' => 'bail|email|max:256|nullable',
            'fax' => 'bail|numeric|digits_between:0,16|nullable',
            'address_type' => 'bail|required|string|max:16|' . Rule::in(array_keys($objAddress->addressType))
        ];

        return $addressRules;
    }

    /**
     * This function is used for API validation message for Address
     *
     * @name   _addressValidationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _addressValidationMessage()
    {
        $messages = [];

        $messages = [
            'fax.digits_between' => 'The :attribute must be maximum 16 digits.',
        ];
        return $messages;
    }
}
