<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Address;

use Modules\Company\Models\Company;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

/**
 * Master group module methods
 *
 * @name     AddCompanyAddress
 * @category Trait
 * @package  Address
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddCompanyAddress
{

    /**
     * Add Company Address Method
     *
     * @param Obj $request      Request $request
     * @param Obj $strCompanyId Request $strCompanyId
     *
     * @name   addAddress
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addCompanyAddress(Request $request, $strCompanyId)
    {
        try {
            // check for valid company id
            $objCompany = $this->checkRecordExistsById(new Company(), $strCompanyId);
            $requestAddress = $request->all();
            $requestAddress['company_id'] = $strCompanyId;
            // Validation
            $this->validateRequest($request, $this->_addressValidationRules($requestAddress), $this->_addressValidationMessage(), $this->_setAddressAttributes(), $this->_setAddressAttributeCode());
            $objResult = $this->_addCompanyAddressTransaction($requestAddress);
            return $this->response->item($objResult, $this->_addressTransformer, ['key' => 'address']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }

    /**
     * Add Company Address DB Transactions
     *
     * @param Array $requestAddress Request Array
     *
     * @name   _addCompanyAddressTransaction
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return Object
     */
    private function _addCompanyAddressTransaction($requestAddress)
    {
        return
            DB::transaction(function () use ($requestAddress) {

                // Add Address data
                $address = $this->_addressRepository->save($this->_addressTransformer->transformRequestParameters($requestAddress));

                // Add company address mapping
                $companyAddress['CompanyAddressId'] = Uuid::generate()->string;
                $companyAddress['CompanyId'] = $requestAddress["company_id"];
                $companyAddress['AddressId'] = $address->AddressId;

                $objCompanyAddress = $this->_companyAddressModel->create($companyAddress);

                return $this->response->item($address, $this->_addressTransformer);
            });
    }
}
