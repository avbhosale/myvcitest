<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Operations
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Underwriting;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Company\Models\Company;

/**
 * Master group module methods
 *
 * @name     AddEditOperation
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddEditUnderWriting
{

    /**
     * Add/Update company operation tab
     *
     * @param String $id      company id
     * @param Obj    $request Request Object
     *
     * @name   addEditOperation
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addEditUnderWriting($id, Request $request)
    {
        try {
            // check for valid company id
            $company = $this->checkRecordExistsById(new Company(), $id);
            $this->validateRequest($request, $this->_validationRules(), $this->_validationMessage(), $this->_setAttributes(), $this->_setAttributeCode());
            $underWriting = $this->_companyRepository->update($company, $this->_underWritingTransformer->transformRequestParameters($request->all()));
            return $this->response->item($underWriting, $this->_underWritingTransformer, ['key' => 'underwriting']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
