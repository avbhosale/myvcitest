<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Underwriting;

/**
 * Underwriting module methods
 *
 * @name     UnderWritingValidator
 * @category Validator
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait UnderWritingValidator
{

    /**
     * This function is used for API validation
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules()
    {

        $rules = [];
        $rules = [
            'has_bank_statements' => 'bail|numeric|min:0|max:1',
            'has_driver_license' => 'bail|numeric|min:0|max:1]',
            'has_business_license' => 'bail|numeric|min:0|max:1',
            'has_other' => 'bail|numeric|min:0|max:1'
        ];
        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage()
    {
        $messages = [
            'has_bank_statements.min' => 'The :attribute must be either 1 or 0.',
            'has_driver_license.max' => 'The :attribute must be either 1 or 0.',
            'has_business_license.min' => 'The :attribute must be either 1 or 0.',
            'has_business_license.max' => 'The :attribute must be either 1 or 0.',
            'has_other.min' => 'The :attribute must be either 1 or 0.',
            'has_other.max' => 'The :attribute must be either 1 or 0.',
        ];
        return $messages;
    }
}
