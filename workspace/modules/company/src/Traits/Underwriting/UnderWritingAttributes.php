<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Underwriting;

/**
 * Master group module methods
 *
 * @name     UnderWritingAttributes
 * @category UnderWriting
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait UnderWritingAttributes
{

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributes()
    {
        $attributes = [];
        $attributes = [
            'has_bank_statements' => 'Has bank statements',
            'has_driver_license' => 'Has driver license',
            'has_business_license' => 'Has business license',
            'has_other' => 'Has other',
        ];
        return $attributes;
    }

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributeCode()
    {
        $errorCodes = [];
        $errorCodes = [
            'has_bank_statements' => 'CO014',
            'has_driver_license' => 'CO015',
            'has_business_license' => 'CO016',
            'has_other' => 'CO017',
        ];
        return $errorCodes;
    }
}
