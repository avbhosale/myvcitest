<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Operation
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Underwriting;

use Illuminate\Database\QueryException;
use Modules\Company\Models\Company;

/**
 * Display company underwriting using company ID
 *
 * @name     ShowUnderWriting
 * @category Trait
 * @package  Underwriting
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ShowUnderWriting
{

    /**
     * Show company underwriting using company ID
     *
     * @param String $id      id integer
     * @param Obj    $request Request Object
     *
     * @name   ShowUnderWriting
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function showUnderWriting($id, $request)
    {
        try {
            // check for valid company id
            $company = $this->checkRecordExistsById(new Company(), $id);
            return $this->response->item($company, $this->_underWritingTransformer, ['key' => 'underwriting']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
