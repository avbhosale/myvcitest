<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\AmendmentNote;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Modules\Company\Models\Company;

/**
 * Master group module methods
 *
 * @name     AddAmendmentNote
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddAmendmentNote
{

    /**
     * Add amendment note Method
     *
     * @param Obj $request      Request Object
     * @param Obj $strCompanyId Company Id
     *
     * @name   addAmendmentNote
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addAmendmentNote(Request $request, $strCompanyId)
    {
        try {
            // check for valid company id
            $objCompany = $this->checkRecordExistsById(new Company(), $strCompanyId);

            $this->validateRequest($request, $this->_validationRules('create'), $this->_validationMessage('create'), $this->_setAttributes(), $this->_setAttributeCode());

            $arrRequest = $request->all();
            $arrRequest['company_id'] = $strCompanyId;

            $amendmentNote = $this->_amendmentNoteRepository->save($this->_amendmentNoteTransformer->transformRequestParameters($arrRequest, 'create'));

            return $this->response->item($amendmentNote, $this->_amendmentNoteTransformer, ['key' => 'amendment_note']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
