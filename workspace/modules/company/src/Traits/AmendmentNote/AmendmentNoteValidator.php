<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\AmendmentNote;

use Illuminate\Validation\Rule;
use Modules\Company\Models\Company;
use Modules\Company\Models\AmendmentNote;

/**
 * All AmendmentNote validate methods in a trait
 *
 * @name     AmendmentNoteValidator
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AmendmentNoteValidator
{

    /**
     * This function is used for API validation
     *
     * @param array $arrRequest Request data
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules($arrRequest)
    {
        // $objContact = new AmendmentNote();

        $rules = [
            'note_id' => 'bail|required|regex:' . static::$REGEX_UUID,
            'note' => 'bail|required|max:255',
            'field_name' => 'bail|required|max:64',
            'old_value' => 'bail|required|max:1024',
            'new_value' => 'bail|required|max:1024',
            'type' => 'bail|required||max:8',
            'created_by' => 'bail|required|regex:' . static::$REGEX_UUID,
        ];

        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage()
    {
        $messages = [];
        return $messages;
    }
}
