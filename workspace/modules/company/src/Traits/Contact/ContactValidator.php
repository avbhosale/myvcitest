<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Contact;

use Illuminate\Validation\Rule;
use Modules\Company\Models\Company;
use Modules\Company\Models\Contact;

/**
 * All Contact validate methods in a trait
 *
 * @name     ContactValidator
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ContactValidator
{

    /**
     * This function is used for API validation
     *
     * @param array $arrRequest Request data
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules($arrRequest)
    {
        $objContact = new Contact();

        $rules = [
            'first_name' => 'bail|required|max:64|regex:' . static::$REGEX_ALPHA_NUMERIC_SPACE,
            'last_name' => 'bail|nullable|max:64|regex:' . static::$REGEX_ALPHA_NUMERIC_SPACE,
            'phone' => 'bail|required|digits:10',
            'mobile' => 'bail|nullable|string|digits:10',
            'primary_email' => 'bail|required|email|max:256',
            'secondary_email' => 'bail|nullable|email|max:256',
            'fax' => 'bail|numeric|digits_between:0,16|nullable',
            'email_format' => 'bail|required|string|max:4|' . Rule::in(array_keys($objContact->emailFormat)),
            'contact_type' => 'bail|required|string|max:16|' . Rule::in(array_keys($objContact->contactType)),
            "address.address_line1" => "bail|required|string|max:1024",
            "address.address_line2" => "bail|required|string|max:1024",
            "address.city" => "bail|required|string|max:64",
            "address.state" => "bail|required|alpha|max:2",
            "address.zip" => "bail|required|digits:5",
        ];

        if (isset($arrRequest['id'])) {
            $rules['address.id'] = 'bail|required|regex:' . static::$REGEX_UUID;
        }

        if (isset($arrRequest['contact_type'])) {
            switch ($arrRequest['contact_type']) {
                case 'principal':
                    $principalRules = [
                        'principal.ssn' => 'bail|required|digits:9|numeric',
                        'principal.driver_license_number' => 'bail|max:64|string|nullable',
                        'principal.driver_license_state' => 'bail|max:2|alpha|nullable',
                        'principal.ownership_percentage' => 'bail|numeric|min:0|max:1|nullable',
                    ];

                    $rules = array_merge($rules, $principalRules);
                    break;

                case 'references':
                    $referenceRules = [
                        'references.account_number' => 'bail|required|numeric|digits_between:0,16',
                    ];

                    if (isset($arrRequest['id'])) {
                        $referenceRules['references.id'] = 'bail|required|regex:' . static::$REGEX_UUID;
                    }

                    $rules = array_merge($rules, $referenceRules);

                    break;
            }
        }

        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage()
    {
        $messages = [];
        $messages = [
            'principal.ownership_percentage.min' => 'The :attribute must be either 1 or 0.',
            'principal.ownership_percentage.max' => 'The :attribute must be either 1 or 0.',
            'address.zip.digits' => 'The :attribute must be 5 digits.',
            'digits_between' => 'The :attribute must be maximum 16 digits.',
        ];
        return $messages;
    }
}
