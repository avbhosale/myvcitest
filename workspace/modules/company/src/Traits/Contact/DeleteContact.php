<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Contact;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Modules\Company\Models\Address;
use Modules\Company\Models\Company;
use Modules\Company\Models\Contact;
use Modules\Company\Models\Principal;
use Modules\Company\Models\References;
use Modules\Company\Repositories\AddressRepository;
use Modules\Company\Repositories\PrincipalRepository;
use Modules\Company\Repositories\ReferencesRepository;

/**
 * Delete Contact methods
 *
 * @name     DeleteContact
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait DeleteContact
{

    /**
     * Delete Contact by Contact ID
     *
     * @param string $strCompanyId Company ID
     * @param string $strContactId Contact ID
     *
     * @name   deleteContact
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function deleteContact($strCompanyId, $strContactId)
    {
        try {
            // check for valid company id
            $company = $this->checkRecordExistsById(new Company(), $strCompanyId);

            // check for valid contact id
            $contact = $this->checkRecordExistsById(new Contact(), $strContactId);

            // save data in multiple tables using DB transactions
            $objResult = $this->_processDeleteContactTransaction($contact);

            return $this->response->array(['id' => $strContactId, 'status' => 'deleted']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }

    /**
     * Delete Contact DB Transactions
     *
     * @param Object $objContact Contact Object data
     *
     * @name   _processDeleteContactTransaction
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return Object
     */
    private function _processDeleteContactTransaction($objContact)
    {
        return
            DB::transaction(function () use ($objContact) {
                $this->_contactRepository->delete($objContact);

                $contactAddress = new AddressRepository(new Address());
                $arrContactAddress = $contactAddress->getAllAddressesByContactId($objContact->ContactId);

                if (!empty($arrContactAddress->all())) {
                    foreach ($arrContactAddress->all() as $arrContactAddress) {
                        $contactAddress->delete($arrContactAddress);
                    }
                }

                $principalRepository = new PrincipalRepository(new Principal());

                $principal = $principalRepository->findOneBy(['ContactId' => $objContact->ContactId, 'EffectiveEndDate' => null]);

                if (!is_null($principal)) {
                    $principalRepository->delete($principal);
                }
            });
    }
}
