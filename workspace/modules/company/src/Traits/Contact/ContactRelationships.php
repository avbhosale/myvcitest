<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Contact;

use Illuminate\Database\QueryException;
use Modules\Company\Models\Address;
use Modules\Company\Models\Principal;
use Modules\Company\Models\References;
use Modules\Company\Repositories\AddressRepository;
use Modules\Company\Repositories\PrincipalRepository;
use Modules\Company\Repositories\ReferencesRepository;
use Modules\Company\Transformers\AddressTransformer;
use Modules\Company\Transformers\PrincipalTransformer;
use Modules\Company\Transformers\ReferencesTransformer;

/**
 * Contact Relationships Trait have all relationship methods
 *
 * @name     ContactRelationships
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ContactRelationships
{

    /**
     * Get Contact addresses by contact ID
     *
     * @param Obj $contact Contact object
     *
     * @name   includeAddresses
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function includeAddresses($contact)
    {
        try {
            $addressRepository = new AddressRepository(new Address());

            $arrobjAddress = $addressRepository->getAllAddressesByContactId($contact->ContactId);

            $addressTransformer = new AddressTransformer();

            $arrAddress = [];
            if (!empty($arrobjAddress->all())) {
                foreach ($arrobjAddress->all() as $address) {
                    $arrAddress[] = $addressTransformer->transform($address);
                }
            }

            return $arrAddress;
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }

    /**
     * Get all contact principal information by contact ID
     *
     * @param Obj $contact Contact object
     *
     * @name   includePrincipal
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function includePrincipal($contact)
    {
        try {
            $principalRepository = new PrincipalRepository(new Principal());

            $objPrincipal = $principalRepository->findOneBy(['ContactId' => $contact->ContactId]);

            $principalTransformer = new PrincipalTransformer();

            return $principalTransformer->transform($objPrincipal);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }

    /**
     * Get all contact references by contact ID
     *
     * @param Obj $contact Contact object
     *
     * @name   includeReferences
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function includeReferences($contact)
    {
        try {
            $referencesRepository = new ReferencesRepository(new References());

            $objReferences = $referencesRepository->findOneBy(['ContactId' => $contact->ContactId]);

            $referencesTransformer = new ReferencesTransformer();

            return $referencesTransformer->transform($objReferences);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
