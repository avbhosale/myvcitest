<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Contact;

use Modules\Company\Models\Address;
use Modules\Company\Models\Contact;
use Modules\Company\Models\Principal;
use Modules\Company\Models\References;

/**
 * Contact module attributes methods
 *
 * @name     ContactAttributes
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ContactAttributes
{

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributes()
    {
        $attributes = [];
        $attributes = [
            'company_id' => 'Company',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'primary_email' => 'Primary Email',
            'secondary_email' => 'Secondary Email',
            'fax' => 'Fax',
            'email_format' => 'Email Format',
            'contact_type' => 'Type',
            'created_at' => 'Created At',
            'address.id' => 'Address ID',
            'address.address_line1' => 'Address Line1',
            'address.address_line2' => 'Address Line2',
            'address.city' => 'City',
            'address.state' => 'State',
            'address.zip' => 'Zip',
            'principal.ssn' => 'Social Security Number',
            'principal.driver_license_number' => 'Driver License Number',
            'principal.driver_license_state' => 'Driver License State',
            'principal.ownership_percentage' => 'Ownership Percentage',
            'references.id' => 'Reference ID',
            'references.account_number' => 'Account Number',
        ];

        return $attributes;
    }

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributeCode()
    {
        $contact = new Contact();
        $address = new Address();
        $principal = new Principal();
        $references = new References();

        // get address error codes
        $addressCodes = [];
        foreach ($address->addressErrorCodes as $key => $addressErrorCode) {
            $addressCodes['address.' . $key] = $addressErrorCode;
        }

        $attributeCodes = array_merge($contact->contactErrorCodes, $addressCodes);

        // get principal error code
        $principalCodes = [];
        foreach ($principal->principalErrorCodes as $key => $principalErrorCode) {
            $principalCodes['principal.' . $key] = $principalErrorCode;
        }

        $attributeCodes = array_merge($attributeCodes, $principalCodes);

        // get references error code
        $referencesCodes = [];
        foreach ($references->referencesErrorCodes as $key => $referencesErrorCode) {
            $referencesCodes['references.' . $key] = $referencesErrorCode;
        }

        $attributeCodes = array_merge($attributeCodes, $referencesCodes);
        return $attributeCodes;
    }
}
