<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Contact;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Company\Models\Company;
use Modules\Company\Models\Principal;
use Modules\Company\Models\References;
use Modules\Company\Repositories\PrincipalRepository;
use Modules\Company\Repositories\ReferencesRepository;
use Modules\Company\Transformers\PrincipalTransformer;
use Modules\Company\Transformers\ReferencesTransformer;
use Webpatser\Uuid\Uuid;

/**
 * Add Contact Trait to store company contacts
 *
 * @name     AddContact
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddContact
{

    /**
     * Add Company Contact by type
     *
     * @param Object $request      Request Object
     * @param String $strCompanyId Company Id
     *
     * @name   addContact
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addContact(Request $request, $strCompanyId)
    {
        try {
            // check for valid company id
            $objCompany = $this->checkRecordExistsById(new Company(), $strCompanyId);

            $arrRequest = $request->all();
            $arrRequest['company_id'] = $strCompanyId;

            // Validation
            $this->validateRequest($request, $this->_validationRules($arrRequest), $this->_validationMessage(), $this->_setAttributes(), $this->_setAttributeCode());

            // save data in multiple tables using DB transactions
            $objResult = $this->_processAddContactTransaction($arrRequest);

            return $this->response->item($objResult, $this->_contactTransformer, ['key' => 'contact']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }

    /**
     * Add Contact DB Transactions
     *
     * @param Array $arrRequest Request Array
     *
     * @name   _processAddContactTransaction
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return Object
     */
    private function _processAddContactTransaction($arrRequest)
    {
        return
            DB::transaction(function () use ($arrRequest) {

                // add contact data
                $objContact = $this->_contactRepository->save($this->_contactTransformer->transformRequestParameters($arrRequest));

                // add address data
                $addressData = $arrRequest['address'];
                $objAddress = $this->_addressRepository->save($this->_addressTransformer->transformRequestParameters($addressData));

                // add contact address mapping
                $contactAddress['ContactAddressId'] = Uuid::generate()->string;
                $contactAddress['ContactId'] = $objContact->ContactId;
                $contactAddress['AddressId'] = $objAddress->AddressId;
                $objContactAddress = $this->_contactAddressModel->create($contactAddress);

                switch ($arrRequest['contact_type']) {
                    case 'principal':
                        // add contact principal mapping
                        $principalRepository = new PrincipalRepository(new Principal());
                        $principalTransformer = new PrincipalTransformer();

                        $principal = $principalRepository->findOneBy(['ContactId' => $objContact->ContactId, 'EffectiveEndDate' => null]);

                        $time = time();
                        if (!is_null($principal)) {
                            $updatePrincipal = $principalRepository->update($principal, ['EffectiveEndDate' => $time]);
                        }

                        $arrPrincipal = $arrRequest['principal'];
                        $arrPrincipal['contact_id'] = $objContact->ContactId;
                        $arrPrincipal['effective_start_date'] = $time;
                        $arrPrincipal['etag'] = $objContact->Etag;

                        $objPrincipal = $principalRepository->save($principalTransformer->transformRequestParameters($arrPrincipal));

                        break;
                    case 'references':
                        // add contact references mapping
                        $referencesRepository = new ReferencesRepository(new References());
                        $referencesTransformer = new ReferencesTransformer();

                        $arrReferences = $arrRequest['references'];
                        $arrReferences['contact_id'] = $objContact->ContactId;
                        $arrReferences['etag'] = $objContact->Etag;

                        $objReferences = $referencesRepository->save($referencesTransformer->transformRequestParameters($arrReferences));

                        break;
                }

                return $objContact;
            });
    }
}
