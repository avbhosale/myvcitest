<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company_Contact
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Contact;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Company\Models\Company;
use Modules\Company\Models\Contact;
use Modules\Company\Models\Address;
use Modules\Company\Models\ContactAddress;
use Modules\Company\Models\Principal;
use Modules\Company\Models\References;
use Modules\Company\Repositories\PrincipalRepository;
use Modules\Company\Repositories\ReferencesRepository;
use Modules\Company\Transformers\PrincipalTransformer;
use Modules\Company\Transformers\ReferencesTransformer;

/**
 * Add Contact Trait to store company contacts
 *
 * @name     EditContact
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait EditContact
{

    /**
     * Add Company Contact by type
     *
     * @param Object $request      Request Object
     * @param String $strCompanyId Company Id
     * @param String $strContactId Contact Id
     *
     * @name   addContact
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function editContact(Request $request, $strCompanyId, $strContactId)
    {
        try {
            $arrRequest = $request->all();
            $arrRequest['company_id'] = $strCompanyId;
            $arrRequest['id'] = $strContactId;

            // check for valid company id
            $objCompany = $this->checkRecordExistsById(new Company(), $strCompanyId);

            // check for valid company id
            $objContact = $this->checkRecordExistsById(new Contact(), $strContactId);

            // check for valid company address
            $contactAddress = new ContactAddress();
            $objContactAddress = $contactAddress->where(['ContactId' => $objContact->ContactId])->first();

            if (strtoupper($arrRequest['address']['id']) != strtoupper($objContactAddress->AddressId)) {
                return $this->errorBadRequest('The contact address with ID ' . $arrRequest['address']['id'] . ' does not exists.');
            }

            // check for valid address id
            $objAddress = $this->checkRecordExistsById(new Address(), $arrRequest['address']['id']);

            // Validation
            $this->validateRequest($request, $this->_validationRules($arrRequest), $this->_validationMessage(), $this->_setAttributes(), $this->_setAttributeCode());

            // update data in multiple tables using DB transactions
            $objResult = $this->_processEditContactTransaction($arrRequest, $objContact, $objAddress);

            return $this->response->item($objResult, $this->_contactTransformer, ['key' => 'contact']);
        } catch (QueryException $exception) {
            dd($exception);
            $this->errorInternal();
        }
    }

    /**
     * Update Contact DB Transactions
     *
     * @param Array  $arrRequest Request Array
     * @param Object $objContact Contact Object
     * @param Object $objAddress Address Object
     *
     * @name   _processEditContactTransaction
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return Object
     */
    private function _processEditContactTransaction($arrRequest, $objContact, $objAddress)
    {
        return
            DB::transaction(function () use ($arrRequest, $objContact, $objAddress) {

                // update contact data
                $objContact = $this->_contactRepository->update($objContact, $this->_contactTransformer->transformRequestParameters($arrRequest));

                // update address data
                $addressData = $arrRequest['address'];

                $objAddress = $this->_addressRepository->update($objAddress, $this->_addressTransformer->transformRequestParameters($addressData));

                switch ($arrRequest['contact_type']) {
                    case 'principal':
                        // add contact principal mapping
                        $principalRepository = new PrincipalRepository(new Principal());
                        $principalTransformer = new PrincipalTransformer();

                        $principal = $principalRepository->findOneBy(['ContactId' => $objContact->ContactId, 'EffectiveEndDate' => null]);

                        $time = time();

                        $arrPrincipal = $arrRequest['principal'];
                        $arrPrincipal['contact_id'] = $objContact->ContactId;
                        $arrPrincipal['effective_start_date'] = $time;
                        $arrPrincipal['etag'] = $objContact->Etag;

                        if (!is_null($principal)) {
                            $updatePrincipal = $principalRepository->update($principal, ['EffectiveEndDate' => $time]);
                            $objPrincipal = $principalRepository->save($principalTransformer->transformRequestParameters($arrPrincipal));
                        } else {
                            $objPrincipal = $principalRepository->save($principalTransformer->transformRequestParameters($arrPrincipal));
                        }

                        break;
                    case 'references':
                        // add contact references mapping
                        $referencesRepository = new ReferencesRepository(new References());
                        $referencesTransformer = new ReferencesTransformer();

                        $arrReferences = $arrRequest['references'];

                        // check for valid supplier reference
                        $references = $referencesRepository->findOneBy(['ContactId' => $objContact->ContactId]);

                        if (strtoupper($arrReferences['id']) != strtoupper($references->ReferencesId)) {
                            return $this->errorBadRequest('The supplier references with ID ' . $arrReferences['id'] . ' does not exists.');
                        }

                        $arrReferences['contact_id'] = $objContact->ContactId;
                        $arrReferences['etag'] = $objContact->Etag;

                        $objReferences = $referencesRepository->update($references, $referencesTransformer->transformRequestParameters($arrReferences));

                        break;
                }

                return $objContact;
            });
    }
}
