<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Operations
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Operation;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Company\Models\Company;
use Modules\Gateway\Models\Gateway;

/**
 * Company operation created/updated using company ID
 *
 * @name     AddEditOperation
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddEditOperation
{

    /**
     * Add/Update company operation tab using company ID
     *
     * @param String $id      company id
     * @param Obj    $request Request Object
     *
     * @name   addEditOperation
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addEditOperation($id, Request $request)
    {
        try {
            // check for valid company id
            $company = $this->checkRecordExistsById(new Company(), $id, 'company id');

            $this->validateRequest($request, $this->_validationRules(), $this->_validationMessage(), $this->_setAttributes(), $this->_setAttributeCode());
            $postParams = $request->all();
            //dd($postParams);
            $gateway = $this->checkRecordExistsById(new Gateway(), $postParams['gateway_id'], 'gateway id');
            $operation = $this->_companyRepository->update($company, $this->_operationTransformer->transformRequestParameters($request->all()));
            return $this->response->item($operation, $this->_operationTransformer, ['key' => 'operation']);
        } catch (QueryException $exception) {
            dd($exception);
            $this->errorInternal();
        }
    }
}
