<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Operation
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Operation;

use Illuminate\Database\QueryException;
use Modules\Company\Models\Company;

/**
 * Display company operations tab details using company ID
 *
 * @name     ShowOperation.php
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ShowOperation
{

    /**
     * Show operation method defination
     *
     * @param String $id      Company ID
     * @param Obj    $request Request Object
     *
     * @name   showOperation
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function showOperation($id, $request)
    {
        try {
            // check for valid company id
            $company = $this->checkRecordExistsById(new Company(), $id);

            return $this->response->item($company, $this->_operationTransformer, ['key' => 'operation']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
