<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Operation;

/**
 * Master group module methods
 *
 * @name     OperationValidator
 * @category Validator
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait OperationValidator
{

    /**
     * This function is used for API validation
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules()
    {

        $rules = [];
        $rules = [
            'gateway_id' => 'bail|required|max:64|regex:' . static::$REGEX_UUID,
            'can_issue_credit' => 'bail|numeric|min:0|max:1',
            'can_issue_credit_approved' => 'bail|numeric|min:0|max:1',
            'can_issue_credit_active' => 'bail|numeric|min:0|max:1',
            'reason_for_deactivation' => 'bail|max:100'
        ];
        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage()
    {
        $messages = [
            'can_issue_credit.min' => 'The :attribute must be either 1 or 0.',
            'can_issue_credit.max' => 'The :attribute must be either 1 or 0.',
            'can_issue_credit_approved.min' => 'The :attribute must be either 1 or 0.',
            'can_issue_credit_approved.max' => 'The :attribute must be either 1 or 0.',
            'can_issue_credit_active.min' => 'The :attribute must be either 1 or 0.',
            'can_issue_credit_active.max' => 'The :attribute must be either 1 or 0.',
        ];
        return $messages;
    }
}
