<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Group;

use Illuminate\Database\QueryException;
use Modules\Company\Transformers\AssignGroupTransformer;
use Modules\Group\Models\Group;

/**
 * Displayed all allocated merchants to the group
 *
 * @name     ShowAssignGroup
 * @category ShowAssignGroup
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ShowAssignGroup
{

    /**
     * Show assigned merchants to group using Group ID
     *
     * @param String $id Group ID
     *
     * @name   showAssignGroup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function showAssignGroup($id)
    {
        try {
            // check for valid company id
            $group = $this->checkRecordExistsById(new Group(), $id, 'group id');

            $assignGroup = $this->_assignGroupRepository->getGroupCompainesByGroupId(['GroupId' => $id]);
            return $this->response()->collection($assignGroup, new AssignGroupTransformer, ['key' => 'Assigned Group']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
