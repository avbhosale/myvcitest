<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Group;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Group\Models\Group;
use Webpatser\Uuid\Uuid;

/**
 * Master group module methods
 *
 * @name     AddAssignGroup
 * @category AssignGroup
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddAssignGroup
{

    /**
     * Create/update merchant/iso groups using group ID
     *
     * @param Obj $request Request Object
     *
     * @name   addAssignGroup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addAssignGroup(Request $request)
    {
        try {
            $saveAssignGroupArr = [];

            $this->validateRequest($request, $this->_validationRules('create'), $this->_validationMessage('create'), $this->_setAttributes(), $this->_setAttributeCode());
            $inputs = $this->_assignGroupTransformer->transformRequestParameters($request->all());

            $this->checkRecordExistsById(new Group(), $inputs['GroupId'], 'group id');
            $deleteGroup = $this->_assignGroupRepository->bulkDeleteCompanyGroupByGroupId($inputs['GroupId']);
            $assignementArray = array_filter($inputs['CompanyId'], function ($value) {
                return $value !== '';
            });
            if (count($assignementArray) > 0) {
                foreach ($inputs['CompanyId'] as $companyKey => $companyId) {
                    $saveAssignGroupArr[$companyKey]['CompanyGroupId'] = Uuid::generate()->string;
                    $saveAssignGroupArr[$companyKey]['GroupId'] = $inputs['GroupId'];
                    $saveAssignGroupArr[$companyKey]['CompanyId'] = $companyId;
                }
                $groupArr = [];
                $this->_assignGroupRepository->bulkInsert($saveAssignGroupArr);
                return $this->response->array(['status_code' => 200, 'message' => 'Groups successfully assigned']);
            }
            return $this->response->array(['status_code' => 200, 'message' => 'Assigned groups successfully removed']);
        } catch (QueryException $exception) {
            dd($exception);
            $this->errorInternal();
        }
    }
}
