<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Group;

use Modules\Group\Models\Group;
use Modules\Infrastructure\Services\ValidateIsExists;

/**
 * Master group module methods
 *
 * @name     GroupController
 * @category Controller
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AssignGroupValidator
{

    /**
     * This function is used for API validation
     *
     * @param array $action request array
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules($action)
    {
        $rules = [];
        $objGroup = new Group();
        $groupTable = config('database.default') . '.' . $objGroup->getTable();
        $rules = [
            'group_id' => 'bail|required|max:64|regex:' . static::$REGEX_UUID,
            'company_ids' => 'bail|required'
        ];
        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @param array $action request array
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage($action)
    {
        $messages = [];
        return $messages;
    }
}
