<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category VelocityCheckValidator
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\VelocityCheck;

use Illuminate\Validation\Rule;
use Modules\Company\Models\Company;
use Modules\Company\Models\VelocityChecks;

/**
 * Master group module methods
 *
 * @name     VelocityCheckValidator
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait VelocityCheckValidator
{

    /**
     * This function is used for API validation
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules()
    {
        $table = config('database.default') . '.' . $this->_velocitycheckRepository->getTableName();
        $objCompany = new Company();
        $objVelocitychecks = new VelocityChecks();
        $companyTable = config('database.default') . '.' . $objCompany->getTable();
        $rules = [];

        $rules = [
            'has_approved' => 'bail|required|numeric|min:0|max:1',
            'has_check_duplicate' => 'bail|required|numeric|min:0|max:1',
            'has_nsf_decline' => 'bail|required|numeric|min:0|max:1',
            'has_default_velocity' => 'bail|required|numeric|min:0|max:1',
            'max_checks_per_day' => 'bail|required|numeric|min:1',
            'max_amount_per_day' => 'bail|required|numeric|regex:' . static::$REGEX_DECIMAL . '|min:1',
            'day_period_merchant' => 'bail|required|numeric|min:1',
            'max_checks_per_period' => 'bail|required|numeric|min:1',
            'max_amount_per_period' => 'bail|required|numeric|regex:' . static::$REGEX_DECIMAL . '|min:1',
            'max_checks_per_day_per_acc' => 'bail|required|numeric',
            'max_amount_per_day_per_acc' => 'bail|required|numeric|regex:' . static::$REGEX_DECIMAL . '|min:1',
            'day_period_account' => 'bail|required|numeric|max:64',
            'max_checks_per_period_per_acc' => 'bail|required|numeric|min:1',
            'max_amount_per_period_per_acc' => 'bail|required|numeric|regex:' . static::$REGEX_DECIMAL . '|min:1',
            'highest_dollar_amount_per_txn' => 'bail|required|numeric|regex:' . static::$REGEX_DECIMAL . '|min:1',
            'funding_time_id' => 'bail|required|regex:' . static::$REGEX_UUID . '|max:64',
            'type' => 'bail|required|string|max:16|' . Rule::in(array_keys($objVelocitychecks->velocityCheckType)),
            'effective_start_date' => 'bail|required|numeric',
            'effective_end_date' => 'bail'
        ];

        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage()
    {
        return $messages = [
            'has_approved.min' => 'The :attribute must be either 1 or 0.',
            'has_approved.max' => 'The :attribute must be either 1 or 0.',
            'has_check_approved.min' => 'The :attribute must be either 1 or 0.',
            'has_check_approved.max' => 'The :attribute must be either 1 or 0.',
            'has_check_duplicate.min' => 'The :attribute must be either 1 or 0.',
            'has_check_duplicate.max' => 'The :attribute must be either 1 or 0.',
            'has_nsf_decline.min' => 'The :attribute must be either 1 or 0.',
            'has_nsf_decline.max' => 'The :attribute must be either 1 or 0.',
            'has_default_velocity.min' => 'The :attribute must be either 1 or 0.',
            'has_default_velocity.max' => 'The :attribute must be either 1 or 0.',
            'max_amount_per_day.min' => 'The :attribute must be greater than 0.',
        ];
    }
}
