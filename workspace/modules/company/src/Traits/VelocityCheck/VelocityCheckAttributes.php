<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\VelocityCheck;

use Modules\Company\Models\VelocityChecks;

/**
 * Master Company module methods
 *
 * @name     CompanyAttributes
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait VelocityCheckAttributes
{

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributes()
    {
        $attributes = [];
        $attributes = [
            'AchVelocityCheckId' => 'Ach Velocity Check Id',
            'CompanyId' => 'Company Id',
            'HasApproved' => 'Has Approved',
            'HasCheckDuplicate' => 'Has Check Duplicate',
            'HasNsfDecline' => 'Has Nsf Decline',
            'HasDefaultVelocity' => 'Has Default Velocity',
            'MaxChecksPerDay' => 'Max Checks PerDay',
            'MaxAmountPerDay' => 'Max Amount PerDay',
            'DayPeriodMerchant' => 'Day Period Merchant',
            'MaxChecksPerPeriod' => 'Max Checks Per Period',
            'MaxAmountPerPeriod' => 'Max Amount Per Period',
            'MaxChecksPerDayPerAcc' => 'Max Checks Per Day Per Acc',
            'MaxAmountPerDayPerAcc' => 'Max Amount Per Day Per Acc',
            'DayPeriodAcc' => 'Day Period Acc',
            'MaxChecksPerPeriodPerAcc' => 'Max Checks Per Period Per Acc',
            'MaxAmountPerPeriodPerAcc' => 'Max Amount Per Period Per Acc',
            'HighestDollarAmountPerTxn' => 'HighestD ollar Amount Per Txn',
            'FundingTimeId' => 'Funding Time Id',
            'Type' => 'Type',
            'EffectiveStartDate' => 'Effective Start Date',
            'EffectiveEndDate' => 'Effective End Date',
            'Etag' => 'Etag'
        ];

        return $attributes;
    }

    /**
     * Function set attribute codes for validation messages
     *
     * @name   _setAttributeCode
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributeCode()
    {
        $velocityChecks = new VelocityChecks();
        return $velocityChecks->velocityErrCodes;
    }
}
