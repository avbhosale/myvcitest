<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\VelocityCheck;

use Illuminate\Database\QueryException;
use Modules\Company\Repositories\CompanyRepository;
use Modules\Company\Models\Company;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

/**
 * Master group module methods
 *
 * @name     AddVelocityCheck
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddVelocityCheck
{

    /**
     * Add VelocityCheck Method
     *
     * @param object $request   request object
     * @param string $companyId id
     *
     * @name   addVelocityCheck
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addVelocityCheck(Request $request, $companyId)
    {
        try {
            // check for valid company id
            $this->checkRecordExistsById(new Company(), $companyId);

            $arrRequest = $request->all();
            $arrRequest['company_id'] = $companyId;
            $this->validateRequest($request, $this->_validationRules(), $this->_validationMessage(), $this->_setAttributes(), $this->_setAttributeCode());
            $velocityCheck = $this->_velocitycheckRepository->save($this->_velocitycheckTransformer->transformRequestParameters($arrRequest, 'create'));

            return $this->response->item($velocityCheck, $this->_velocitycheckTransformer);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
