<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Company;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Company\Models\Company;

/**
 * Activate or deactivate the company
 *
 * @name     CompanyActivateDeactivate
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait CompanyActivateDeactivate
{

    /**
     * Activate/Deactivate company
     *
     * @param Obj    $request   Request Object
     * @param String $companyId Company Id
     * @param String $action    activate/deactivate action
     *
     * @name   activateOrDeactivate
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function activateOrDeactivate(Request $request, $companyId, $action)
    {
        try {
            $actionArr = [];
            // check for valid company id
            $company = $this->checkRecordExistsById(new Company(), $companyId, 'company id');
            (isset($action) && $action == 'activate') ? $actionArr['status'] = 1 : $actionArr['status'] = 0;
            $company = $this->_companyRepository->update($company, $this->_companyTransformer->transformRequestParameters($actionArr));
            return $this->response->item($company, $this->_companyTransformer, ['key' => 'company']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
