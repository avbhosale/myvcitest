<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Company;

use Modules\Company\Models\Company;

/**
 * Master Company module methods
 *
 * @name     CompanyAttributes
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait CompanyAttributes
{

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributes()
    {
        $attributes = [];
        $attributes = [
            'ParentId' => 'Parent Id',
            'Type' => 'Type',
            'OdfiId' => 'Odfi Id',
            'Name' => 'Name',
            'DoingBusinessAs' => 'Doing Business As',
            'TaxId' => 'Tax Id',
            'WebsiteUrl' => 'Website Url',
            //'PinNumber' => 'Pin Number',
            'Uuid' => 'Uuid',
            'GatewayId' => 'Gate Way Id',
            'CanIssueCredit' => 'Can Issue Credit',
            'CanIssueCreditApproved' => 'Can Issue Credit Approved',
            'CanIssueCreditActive' => 'Can Issue Credit Active',
            'HasBankstatements' => 'Has Bank Statements',
            'HasDriverLicense' => 'Has Driver License',
            'HasBusinessLicense' => 'Has Business License',
            'HasOther' => 'Has Other',
            'Other' => 'Other',
            'ReasonForDeactivation' => 'Reason For Deactivation',
            'Status' => 'Status',
            'CreatedBy' => 'Created By',
            'CreatedAt' => 'Created At'
        ];

        return $attributes;
    }

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributeCode()
    {
        $assignCompany = new Company();
        return $assignCompany->companyErrCodes;
    }
}
