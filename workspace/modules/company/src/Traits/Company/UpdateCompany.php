<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Company;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Modules\Company\Models\Company;

/**
 * Master group module methods
 *
 * @name     UpdateCompany
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait UpdateCompany
{

    /**
     * Add Company Method
     *
     * @param Obj $request Request Object
     * @param Obj $id      id integer
     *
     * @name   updateCompany
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function updateCompany(Request $request, $id)
    {
        try {
            // check for valid company id
            $this->checkRecordExistsById(new Company(), $id);
            $company = $this->_companyRepository->findOne($id);
            $validatorResponse = $this->validateRequest($request, $this->_validationRules('update', $id), $this->_validationMessage('update'), $this->_setAttributes(), $this->_setAttributeCode());
            $company = $this->_companyRepository->update($company, $this->_companyTransformer->transformRequestParameters($request->all()));

            return $this->response->item($company, $this->_companyTransformer, ['key' => 'company']);
        } catch (ClientException $exception) {
            $this->errorUnauthorized('Invalid Access Token');
        }
    }
}
