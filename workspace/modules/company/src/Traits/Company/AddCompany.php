<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Company;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Modules\Infrastructure\Traits\ManageToken;

/**
 * Master group module methods
 *
 * @name     AddCompany
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddCompany
{

    use ManageToken;

    /**
     * Add Company Method
     *
     * @param Obj $request Request Object
     *
     * @name   addCompany
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addCompany(Request $request)
    {
        try {
            $this->validateRequest($request, $this->_validationRules('create'), $this->_validationMessage('create'), $this->_setAttributes(), $this->_setAttributeCode());

            $arrRequest = $request->all();
            $arrRequest['tax_id'] = $this->setToken($arrRequest['tax_id']);
            $arrRequest['uuid'] = $this->setToken(Uuid::generate()->string);
            $arrRequest['pin_number'] = $this->setToken($arrRequest['pin_number']);

            $company = $this->_companyRepository->save($this->_companyTransformer->transformRequestParameters($arrRequest, 'create'));

            $company['TaxId'] = $this->getToken($company['TaxId']);
            $company['Uuid'] = $this->getToken($company['Uuid']);
            $company['PinNumber'] = $this->getToken($company['PinNumber']);

            return $this->response->item($company, $this->_companyTransformer, ['key' => 'company']);
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }
}
