<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\Company;

use Illuminate\Validation\Rule;
use Modules\Company\Models\Company;

/**
 * Master group module methods
 *
 * @name     Company
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait CompanyValidator
{

    /**
     * This function is used for API validation
     *
     * @param array   $action request array
     * @param integer $id     request integer
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules($action, $id = null)
    {
        $table = config('database.default') . '.' . $this->_companyRepository->getTableName();
        $objCompany = new Company();
        $rules = [];

        if ($action == 'create') {
            $rules = [
                'parent_id' => 'bail|required|regex:' . static::$REGEX_UUID . '|max:64',
                'type' => 'bail|required|string|max:16|' . Rule::in(array_keys($objCompany->companyType)),
                'odfi_id' => 'bail|regex:' . static::$REGEX_UUID,
                'name' => 'bail|required|unique:' . $table . ',Name,NULL,CompanyId,DeletedAt,NULL|regex:' . static::$REGEX_ALPHA_SPACE . '|max:255',
                'doing_business_as' => 'bail|required|string|max:1024',
                'tax_id' => 'bail|required|string|max:64|regex:' . static::$REGEX_TIN,
                'website_url' => 'bail|url|string|max:1024',
                'pin_number' => 'bail|numeric|digits:3',
                'uuid' => 'bail|regex:' . static::$REGEX_UUID . '|max:64',
                'gateway_id' => 'bail|max:64',
                'can_issue_credit' => 'bail|numeric|min:0|max:1',
                'can_issue_credit_approved' => 'bail|numeric|min:0|max:1',
                'can_issue_credit_active' => 'bail|numeric|min:0|max:1',
                'has_bankstatements' => 'bail|numeric|min:0|max:1',
                'has_driver_license' => 'bail|numeric|min:0|max:1',
                'has_business_license' => 'bail|numeric|min:0|max:1',
                'has_other' => 'bail|numeric|min:0|max:1',
                'other' => 'bail|string|max:64',
                'status' => 'bail|required|numeric|min:0|max:1',
                'created_by' => 'bail|required|max:64'
            ];
        } else {
            $rules = [
                'parent_id' => 'bail|required|regex:' . static::$REGEX_UUID . '|max:64',
                'type' => 'bail|required|string|max:16|' . Rule::in(array_keys($objCompany->companyType)),
                'odfi_id' => 'bail|regex:' . static::$REGEX_UUID,
                'name' => 'bail|unique:' . $table . ',Name,' . $id . ',CompanyId,DeletedAt,NULL|regex:' . static::$REGEX_ALPHA_SPACE . '|max:255',
                'doing_business_as' => 'bail|string|max:1024',
                'tax_id' => 'bail|string|max:64|regex:' . static::$REGEX_TIN,
                'website_url' => 'bail|url|string|max:1024',
                'pin_number' => 'bail|numeric|digits:3',
                'uuid' => 'bail|regex:' . static::$REGEX_UUID . '|max:64',
                'gateway_id' => 'bail|max:64',
                'can_issue_credit' => 'bail|numeric|min:0|max:1',
                'can_issue_credit_approved' => 'bail|numeric|min:0|max:1',
                'can_issue_credit_active' => 'bail|numeric|min:0|max:1',
                'has_bankstatements' => 'bail|numeric|min:0|max:1',
                'has_driver_license' => 'bail|numeric|min:0|max:1',
                'has_business_license' => 'bail|numeric|min:0|max:1',
                'has_other' => 'bail|numeric|min:0|max:1',
                'other' => 'bail|string|max:64',
                'status' => 'bail|numeric|min:0|max:1',
                'created_by' => 'bail|max:64'
            ];
        }

        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @param array $action request array
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage($action)
    {
        $messages = [];
        switch ($action) {
            case 'create':
                $messages = [
                    'can_issue_credit.min' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit.max' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit_approved.min' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit_approved.max' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit_active.min' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit_active.max' => 'The :attribute must be either 1 or 0.',
                    'has_bankstatements.min' => 'The :attribute must be either 1 or 0.',
                    'has_bankstatements.max' => 'The :attribute must be either 1 or 0.',
                    'has_driver_license.min' => 'The :attribute must be either 1 or 0.',
                    'has_driver_license.max' => 'The :attribute must be either 1 or 0.',
                    'has_other.min' => 'The :attribute must be either 1 or 0.',
                    'has_other.max' => 'The :attribute must be either 1 or 0.',
                    'status.min' => 'The :attribute must be either 1 or 0.',
                    'status.max' => 'The :attribute must be either 1 or 0.',
                ];
                break;
            case 'update':
                $messages = [
                    'can_issue_credit.min' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit.max' => 'The :attribute must be either 1 or 0.',
                    'pin_number.min' => 'The :attribute must be of 3 digit.',
                    'pin_number.max' => 'The :attribute must be of 3 digit.',
                    'can_issue_credit_approved.min' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit_approved.max' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit_active.min' => 'The :attribute must be either 1 or 0.',
                    'can_issue_credit_active.max' => 'The :attribute must be either 1 or 0.',
                    'has_bankstatements.min' => 'The :attribute must be either 1 or 0.',
                    'has_bankstatements.max' => 'The :attribute must be either 1 or 0.',
                    'has_driver_license.min' => 'The :attribute must be either 1 or 0.',
                    'has_driver_license.max' => 'The :attribute must be either 1 or 0.',
                    'has_other.min' => 'The :attribute must be either 1 or 0.',
                    'has_other.max' => 'The :attribute must be either 1 or 0.',
                    'status.min' => 'The :attribute must be either 1 or 0.',
                    'status.max' => 'The :attribute must be either 1 or 0.',
                ];
                break;
        }
        return $messages;
    }
}
