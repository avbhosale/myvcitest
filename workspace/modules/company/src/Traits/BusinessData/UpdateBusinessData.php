<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Traits
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\BusinessData;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Company\Models\Company;

/**
 * Update business data trait
 *
 * @name     UpdateBusinessData
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait UpdateBusinessData
{

    /**
     * Edit Business Data
     *
     * @param Obj    $request      Request Object
     * @param String $strCompanyId Id of company
     *
     * @name   updateBusinessData
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function updateBusinessData(Request $request, $strCompanyId)
    {
        try {
            // check for valid company id
            $objCompany = $this->checkRecordExistsById(new Company(), $strCompanyId);

            $this->validateRequest($request, $this->_validationRules('edit'), $this->_validationMessage('edit'), $this->_setAttributes(), $this->_setAttributeCode());
            $arrRequest = $request->all();
            $arrRequest['company_id'] = $strCompanyId;

            // save data in multiple tables using DB transactions
            $objResult = $this->_processUpdateBusinessDataTransaction($arrRequest);

            return $objResult;
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }

    /**
     * Function to save data of business info, marketing and information data
     *
     * @param Array $arrRequest Request array
     *
     * @name   _processAddBusinessDataTransaction
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return Object
     */
    private function _processUpdateBusinessDataTransaction($arrRequest)
    {
        $companyId = $arrRequest['company_id'];

        return
            DB::transaction(function () use ($arrRequest, $companyId) {

                //get bussiness info data
                $objBusinessData = $this->_businessInfoRepository->findOneBy(['CompanyId' => $companyId]);

                //update bussiness info data
                $objUpdatedBusinessInfo = $this->_businessInfoRepository->update($objBusinessData, $this->_businessInfoTransformer->transformRequestParameters($arrRequest));

                //get marketing data
                $objMarketingData = $this->_marketingRepository->findOneBy(['CompanyId' => $companyId]);

                //update marketing info data
                $objUpdatedMarketingData = $this->_marketingRepository->update($objMarketingData, $this->_marketingTransformer->transformRequestParameters($arrRequest));

                $objBusinessData->expected_txn_per_month = $objUpdatedMarketingData->ExpectedTxnPerMonth;
                $objBusinessData->expected_amount_per_month = $objUpdatedMarketingData->ExpectedAmountPerMonth;
                $objBusinessData->average_txn_amount = $objUpdatedMarketingData->AverageTxnAmount;

                //update inofromation data
                if (true == isset($arrRequest['expected_total_transactions_per_month']) && '' != $arrRequest['expected_total_transactions_per_month']) {
                    $infoETTPM01 = $this->_updateInformationData($companyId, "ETTPM01", $arrRequest['expected_total_transactions_per_month'], "per_month");
                    $objBusinessData->expected_total_transactions_per_month = $infoETTPM01->Value;
                }

                //update inofromation data
                if (true == isset($arrRequest['expected_total_dollars_per_month']) && '' != $arrRequest['expected_total_dollars_per_month']) {
                    $infoETTPM02 = $this->_updateInformationData($companyId, "ETDPM02", $arrRequest['expected_total_transactions_per_month'], "per_month");
                    $objBusinessData->expected_total_dollars_per_month = $infoETTPM02->Value;
                }

                return $this->response->item($objBusinessData, $this->_businessInfoTransformer, ['key' => 'business_info']);
            });
    }

    /**
     * Function to update information data
     *
     * @param Integer $companyId is used to get company id
     * @param Mixed   $attribute is used to get attribute
     * @param Mixed   $value     is used to get value
     * @param Mixed   $type      is used to get type
     *
     * @name   _updateInformationData
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return Object
     */
    private function _updateInformationData($companyId, $attribute, $value, $type)
    {
        //get information data
        $objInformationData = $this->_informationRepository->findOneBy(['CompanyId' => $companyId, 'Attribute' => $attribute]);

        $arrUpdateInfo = array();

        $arrUpdateInfo["company_id"] = $companyId;
        $arrUpdateInfo["attribute"] = $attribute;
        $arrUpdateInfo["value"] = $value;
        $arrUpdateInfo["type"] = $type;

        //update information data
        $objInformation = $this->_informationRepository->update($objInformationData, $this->_informationTransformer->transformRequestParameters($arrUpdateInfo));

        return $objInformation;
    }
}
