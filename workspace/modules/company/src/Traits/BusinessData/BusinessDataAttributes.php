<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\BusinessData;

use Modules\Company\Models\BusinessInfo;
use Modules\Company\Models\Marketing;
use Modules\Company\Models\Information;

/**
 * Master Company module methods
 *
 * @name     BusinessDataAttributes
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait BusinessDataAttributes
{

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributes()
    {
        $attributes = [];
        $attributes = [
            'description' => 'Description',
            'business_type' => 'Business Type',
            'product_service' => 'Product Service',
            'naics_code' => 'NAICS Code',
            'corporate_structure_id' => 'Corporate Structure',
            'business_start_date' => 'Business Start Date',
            'expected_txn_per_month' => 'Expected ACH Transactions Per Month',
            'expected_amount_per_month' => 'Expected ACH Dollars Per Month',
            'average_txn_amount' => 'Average ACH Transaction Amount',
            'company_id' => 'Company',
            'expected_total_dollars_per_month' => 'Expected total dollars per month',
            'expected_total_transactions_per_month' => 'Expected total transactions per month'
        ];

        return $attributes;
    }

    /**
     * Function set attributes for validation messages
     *
     * @name   _setAttributes
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _setAttributeCode()
    {
        $objBusinessInfo = new BusinessInfo();
        $objMarketing = new Marketing();
        $objInformation = new Information();

        //marketing error codes
        $arrMarketing['expected_txn_per_month'] = $objMarketing->marketingErrCodes['expected_txn_per_month'];
        $arrMarketing['expected_amount_per_month'] = $objMarketing->marketingErrCodes['expected_amount_per_month'];
        $arrMarketing['average_txn_amount'] = $objMarketing->marketingErrCodes['average_txn_amount'];

        //information error codes
        $arrMarketing['expected_total_transactions_per_month'] = $objInformation->informationErrCodes['expected_total_transactions_per_month'];
        $arrMarketing['expected_total_dollars_per_month'] = $objInformation->informationErrCodes['expected_total_dollars_per_month'];

        //business error codes merge
        $attributeCodes = array_merge($objBusinessInfo->businessInfoErrCodes, $arrMarketing);

        return $attributeCodes;
    }
}
