<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Traits
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\BusinessData;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Company\Models\Company;

/**
 * Add business data trait
 *
 * @name     AddBusinessData
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddBusinessData
{

    /**
     * Add Business Data
     *
     * @param Obj $request      Request Object
     * @param Obj $strCompanyId Company id
     *
     * @name   addBusinessData
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addBusinessData(Request $request, $strCompanyId)
    {
        try {
            // check for valid company id
            $objCompany = $this->checkRecordExistsById(new Company(), $strCompanyId);

            $this->validateRequest($request, $this->_validationRules('create'), $this->_validationMessage('create'), $this->_setAttributes(), $this->_setAttributeCode());
            $arrRequest = $request->all();
            $arrRequest['company_id'] = $strCompanyId;

            //get bussiness info data
            $objBusinessData = $this->_businessInfoRepository->findOneBy(['CompanyId' => $strCompanyId]);

            //check if data is already exist
            if (is_null($objBusinessData)) {
                // save data in multiple tables using DB transactions
                $objResult = $this->_processAddBusinessDataTransaction($arrRequest);

                return $objResult;
            } else {
                return $this->errorBadRequest("Company business data already exist.");
            }
        } catch (QueryException $exception) {
            $this->errorInternal();
        }
    }

    /**
     * Add Business data DB Transactions
     *
     * @param Array $arrRequest Request Array
     *
     * @name   _processAddBusinessDataTransaction
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return Object
     */
    private function _processAddBusinessDataTransaction($arrRequest)
    {
        return
            DB::transaction(function () use ($arrRequest) {
                //add business info data
                $objBusinessInfo = $this->_businessInfoRepository->save($this->_businessInfoTransformer->transformRequestParameters($arrRequest));

                //add marketing info data
                $objMarketing = $this->_marketingRepository->save($this->_marketingTransformer->transformRequestParameters($arrRequest));

                //assign marketing data to object
                $objBusinessInfo->expected_txn_per_month = $objMarketing->ExpectedTxnPerMonth;
                $objBusinessInfo->expected_amount_per_month = $objMarketing->ExpectedAmountPerMonth;
                $objBusinessInfo->average_txn_amount = $objMarketing->AverageTxnAmount;

                //add inofromation data
                if (true == isset($arrRequest['expected_total_transactions_per_month']) && '' != $arrRequest['expected_total_transactions_per_month']) {
                    $infoETTPM01 = $this->_saveInformationData($arrRequest['company_id'], "ETTPM01", $arrRequest['expected_total_transactions_per_month'], "per_month");
                    $objBusinessInfo->expected_total_transactions_per_month = $infoETTPM01->Value;
                }

                //add inofromation data
                if (true == isset($arrRequest['expected_total_dollars_per_month']) && '' != $arrRequest['expected_total_dollars_per_month']) {
                    $infoETTPM02 = $this->_saveInformationData($arrRequest['company_id'], "ETDPM02", $arrRequest['expected_total_transactions_per_month'], "per_month");
                    $objBusinessInfo->expected_total_dollars_per_month = $infoETTPM02->Value;
                }

                return $this->response->item($objBusinessInfo, $this->_businessInfoTransformer, ['key' => 'business_info']);
            });
    }

    /**
     * Function to save information data
     *
     * @param Obj $companyId is used to get company id
     * @param Obj $attribute is used to get attribute
     * @param Obj $value     is used to get value
     * @param Obj $type      is used to get type
     *
     * @name   _saveInformationData
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return Object
     */
    private function _saveInformationData($companyId, $attribute, $value, $type)
    {
        $arrAddInfo = array();
        $arrAddInfo["company_id"] = $companyId;
        $arrAddInfo["attribute"] = $attribute;
        $arrAddInfo["value"] = $value;
        $arrAddInfo["type"] = $type;

        //save information data
        $objInformation = $this->_informationRepository->save($this->_informationTransformer->transformRequestParameters($arrAddInfo));

        return $objInformation;
    }
}
