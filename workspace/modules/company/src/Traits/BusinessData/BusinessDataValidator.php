<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Traits\BusinessData;

/**
 * Master group module methods
 *
 * @name     BusinessDataValidator
 * @category Trait
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait BusinessDataValidator
{

    /**
     * This function is used for API validation
     *
     * @param array $action request array
     *
     * @name   _validationRules
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationRules($action)
    {
        $rules = [];
        $rules = [
            'description' => 'bail|required|string|max:2048',
            'business_type' => 'bail|required|string|max:16',
            'product_service' => 'bail|required|string|max:16',
            'naics_code' => 'bail|required|string|max:6',
            'corporate_structure_id' => 'bail|required|string',
            'business_start_date' => 'bail|numeric',
            'expected_txn_per_month' => 'bail|numeric',
            'expected_amount_per_month' => 'bail|numeric',
            'average_txn_amount' => 'bail|numeric',
            'effective_start_date' => 'bail|numeric',
            'effective_end_date' => 'bail|numeric',
            'expected_total_transactions_per_month' => 'bail|numeric',
            'expected_total_dollars_per_month' => 'bail|numeric',
        ];

        return $rules;
    }

    /**
     * This function is used for API validation message
     *
     * @param array $action request array
     *
     * @name   _validationMessage
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    private function _validationMessage($action)
    {
        $messages = [];

        return $messages;
    }
}
