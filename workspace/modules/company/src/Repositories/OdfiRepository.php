<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Repository
 * @package  OdfiRepository
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Repositories;

use Illuminate\Database\Eloquent\Model;
use Modules\Company\Repositories\Odfis\OdfiInterface;
use Modules\Company\Models\Odfi;
use Webpatser\Uuid\Uuid;

/**
 * Two line description of class
 *
 * @name     OdfiRepository.php
 * @category Repository
 * @package  OdfiRepository
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class OdfiRepository implements OdfiInterface
{

    protected $model;

    /**
     * Constructor
     *
     * @param Obj $odfi odfi model object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(Odfi $odfi)
    {
        $this->model = $odfi;
    }

    /**
     * Find all resources using pagination
     *
     * @param array $searchCriteria array of search criteria
     *
     * @name   all
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function all(array $searchCriteria)
    {
        return $this->model->all();
    }

    /**
     * Save a resource
     *
     * @param array $data array of values
     *
     * @name   save
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function save(array $data)
    {
        $dataArr = array(
            array(
                'FedWindowId' => Uuid::generate()->string,
                'CompanyId' => $data['CompanyId'],
                'OdfiFedWindowId' => $data['OdfiFedWindowId'][0]['odfi_fed_window_id'],
                'HasEnable' => $data['OdfiFedWindowId'][0]['has_enable'],
                'Etag' => $data['Etag']
            )
        );

        return $this->model->insert($dataArr);
    }

    /**
     * Update a resource
     *
     * @param Model $velocityCheck model
     * @param array $data          array of resource data
     *
     * @name   update
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function update(Model $velocityCheck, array $data)
    {
        echo '<pre>';
        print_r('In Update Repository');
        exit;
    }

    /**
     * Description
     *
     * @param array $searchCriteria array of resource data
     *
     * @name   findBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findBy(array $searchCriteria = [])
    {
        $limit = !empty($searchCriteria['per_page']) ? (int) $searchCriteria['per_page'] : $this->model->perPage; // it's needed for pagination
        $queryBuilder = $this->model->where(function ($query) use ($searchCriteria) {
            $this->applySearch($query, $searchCriteria);
        });
        return $queryBuilder->paginate($limit);
    }

    /**
     * Description
     *
     * @param Object $queryBuilder   query Builder
     * @param array  $searchCriteria search Criteria
     *
     * @name   applySearch
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    protected function applySearch($queryBuilder, array $searchCriteria = [])
    {
        foreach ($searchCriteria as $key => $value) {
            //skip pagination related query params
            if (in_array($key, ['page', 'per_page'])) {
                continue;
            }
            //we can pass multiple params for a filter with commas
            $allValues = explode(',', $value);
            if (count($allValues) > 1) {
                $queryBuilder->whereIn($key, $allValues);
            } else {
                $operator = '=';
                $queryBuilder->where($key, $operator, $value);
            }
        }
        return $queryBuilder;
    }

    /**
     * Find a resource by id
     *
     * @param integer $id id of resource
     *
     * @name   findOne
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOne($id)
    {
        return $this->findOneBy(['CompanyId' => $id]);
    }

    /**
     * Delete a resource
     *
     * @param Model $velocityCheck model
     *
     * @name   delete
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function delete(Model $velocityCheck)
    {
        return $velocityCheck->delete();
    }

    /**
     * Find a resource by criteria
     *
     * @param array $criteria array of search criteria
     *
     * @name   findOneBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOneBy(array $criteria)
    {
        return $this->model->where($criteria)->first();
    }

    /**
     * Search All resources by any values of a key
     *
     * @param string $key    key
     * @param array  $values array
     *
     * @name   findIn
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findIn($key, array $values)
    {
        return $this->model->whereIn($key, $values)->get();
    }

    /**
     * Get model table name
     *
     * @name   getTableName
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->model->getTable();
    }
}
