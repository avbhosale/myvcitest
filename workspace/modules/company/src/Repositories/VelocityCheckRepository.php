<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Repository
 * @package  VelocityCheckRepository
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Repositories;

use Illuminate\Database\Eloquent\Model;
use Modules\Company\Repositories\VelocityChecks\VelocityCheckInterface;
use Modules\Company\Models\VelocityChecks;

/**
 * Two line description of class
 *
 * @name     VelocityCheckRepository.php
 * @category Repository
 * @package  VelocityCheckRepository
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class VelocityCheckRepository implements VelocityCheckInterface
{

    protected $model;

    /**
     * Constructor
     *
     * @param Obj $velocityCheck VelocityChecks model object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(VelocityChecks $velocityCheck)
    {
        $this->model = $velocityCheck;
    }

    /**
     * Find all resources using pagination
     *
     * @param array $searchCriteria array of search criteria
     *
     * @name   all
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function all(array $searchCriteria)
    {
        return $this->model->all();
    }

    /**
     * Save a resource
     *
     * @param array $data array of values
     *
     * @name   save
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function save(array $data)
    {
        $search = ['CompanyId' => $data['CompanyId'], 'Type' => $data['Type'], 'EffectiveEndDate' => null];
        $time = time();
        $companyId = $this->findOneBy($search);
        if (!isset($companyId)) {
            // add new velocitycheck
            $data['EffectiveStartDate'] = $time;
            $data['EffectiveEndDate'] = null;
            return $this->model->create($data);
        } else {
            //update old velocitycheck
            $companyId->EffectiveEndDate = $time;
            $companyId->save();
            // add new velocitycheck
            $data['EffectiveStartDate'] = $time;
            $data['EffectiveEndDate'] = null;
            return $this->model->create($data);
        }
    }

    /**
     * Update a resource
     *
     * @param Model $velocityCheck model
     * @param array $data          array of resource data
     *
     * @name   update
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function update(Model $velocityCheck, array $data)
    {
        $fillAbleProperties = $velocityCheck->getFillable();
        foreach ($data as $key => $value) {
            if (in_array($key, $fillAbleProperties)) {
                $velocityCheck->$key = $value;
            }
        }

        $velocityCheck->save();

        $updateVelocityCheck = $this->findOne($velocityCheck->AchVelocityCheckId);
        return $updateVelocityCheck;
    }

    /**
     * Find a resource by id
     *
     * @param integer $id id of resource
     *
     * @name   findOne
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOne($id)
    {
        return $this->findOneBy(['AchVelocityCheckId' => $id]);
    }

    /**
     * Delete a resource
     *
     * @param Model $velocityCheck model
     *
     * @name   delete
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function delete(Model $velocityCheck)
    {
        return $velocityCheck->delete();
    }

    /**
     * Find a resource by criteria
     *
     * @param array $criteria array of search criteria
     *
     * @name   findOneBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOneBy(array $criteria)
    {
        return $this->model->where($criteria)->first();
    }

    /**
     * Search All resources by any values of a key
     *
     * @param string $key    key
     * @param array  $values array
     *
     * @name   findIn
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findIn($key, array $values)
    {
        return $this->model->whereIn($key, $values)->get();
    }

    /**
     * Get model table name
     *
     * @name   getTableName
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->model->getTable();
    }
}
