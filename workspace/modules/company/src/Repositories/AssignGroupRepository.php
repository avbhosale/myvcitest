<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Repository
 * @package  AssignGroupRepository
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Repositories;

use Illuminate\Database\Eloquent\Model;
use Modules\Company\Models\AssignGroup;
use Modules\Company\Repositories\Contracts\AssignGroupInterface;

/**
 * Two line description of class
 *
 * @name     AssignGroupRepository.php
 * @category Repository
 * @package  AssignGroupRepository
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AssignGroupRepository implements AssignGroupInterface
{

    protected $model;
    private $_companyRepository;

    /**
     * Constructor
     *
     * @param Obj $assignGroup        AssignGroup model object
     * @param Obj $_companyRepository CompanyRepository model object
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(AssignGroup $assignGroup, CompanyRepository $_companyRepository)
    {
        $this->model = $assignGroup;
        $this->_companyRepository = $_companyRepository;
    }

    /**
     * Find all resources using pagination
     *
     * @param array $searchCriteria array of search criteria
     *
     * @name   all
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function all(array $searchCriteria)
    {
    }

    /**
     * Save a resource
     *
     * @param array $data array of values
     *
     * @name   save
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function save(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update a resource
     *
     * @param Model $assignGroup model
     * @param array $data        array of resource data
     *
     * @name   update
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function update(Model $assignGroup, array $data)
    {
        $fillAbleProperties = $assignGroup->getFillable();
        foreach ($data as $key => $value) {
            if (in_array($key, $fillAbleProperties)) {
                $assignGroup->$key = $value;
            }
        }

        $assignGroup->save();

        $updateAssignGroup = $this->findOne($assignGroup->CompanyGroupId);
        return $updateAssignGroup;
    }

    /**
     * Description
     *
     * @param array $searchCriteria array of resource data
     *
     * @name   findBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findBy(array $searchCriteria = [])
    {
    }

    /**
     * Find a resource by id
     *
     * @param integer $id id of resource
     *
     * @name   findOne
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOne($id)
    {
        return $this->findOneBy(['CompanyGroupId' => $id]);
    }

    /**
     * Delete a resource
     *
     * @param Model $assignGroup model
     *
     * @name   delete
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function delete(Model $assignGroup)
    {
    }

    /**
     * Find a resource by criteria
     *
     * @param array $criteria array of search criteria
     *
     * @name   findOneBy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findOneBy(array $criteria)
    {
    }

    /**
     * Search All resources by any values of a key
     *
     * @param string $key    key
     * @param array  $values array
     *
     * @name   findIn
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function findIn($key, array $values)
    {
    }

    /**
     * Save a resource using bulk insert
     *
     * @param array $data array of values
     *
     * @name   bulkInsert
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function bulkInsert(array $data)
    {
        return $this->model->insert($data);
    }

    /**
     * Bulk Delete AroAco table records if User is deleted from Active Directory
     *
     * @param string $strGroupId group-id is single group entry primary key
     *
     * @name   bulkDeleteCompanyGroupByGroupId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return obj
     */
    public function bulkDeleteCompanyGroupByGroupId($strGroupId)
    {
        return $this->model->where('GroupId', $strGroupId)->delete();
    }

    /**
     * Description
     *
     * @param String $strGroupId is used to get assigned iso/merchants from group id
     *
     * @name   getGroupCompainesByGroupId
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function getGroupCompainesByGroupId($strGroupId)
    {
        $strCompanyTableName = $this->_companyRepository->getTableName();
        return $this->model->join($strCompanyTableName, $strCompanyTableName . '.CompanyId', '=', $this->model->getTable() . '.CompanyId')->select($this->model->getTable() . '.*', $strCompanyTableName . '.Name')->where('GroupId', $strGroupId)->get();
    }
}
