<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Unit_Test_Case
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Tests\OperationTest;

use Modules\Company\Models\Company;
use TestCase;

/**
 * Test case for Master Group Module
 *
 * @name     OperationTest
 * @category Operation
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class OperationTest extends TestCase
{

    protected $model;

    /**
     * Setup repository
     *
     * @name   setup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function setup()
    {
        parent::setUp();
        $this->model = new Company();
    }

    /**
     * Assign/Remove company operation
     *
     * @name   testCompanyOperation
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCompanyOperation()
    {
        /*
         * Create company
         */
        $arrFakeCompany = factory(Company::class)->make()->toArray();
        $addCompanyResponse = $this->call('POST', 'companies', $arrFakeCompany);
        $this->refreshApplication();
        $this->assertEquals(200, $addCompanyResponse->status());
        $intCompanyId = json_decode($addCompanyResponse->getContent())->data->id;
        /*
         * Update company operation
         */
        $gateway = \Modules\Gateway\Models\Gateway::get()->where('DeletedAt', null)->first();

        $arrTestOperationData = [
            "gateway_id" => $gateway->GatewayId,
            "can_issue_credit" => 1,
            "can_issue_credit_approved" => 1,
            "can_issue_credit_active" => 1,
            "reason_for_deactivation" => 'fdfdafa'
        ];
        $addOperationResponse = $this->call('POST', '/companies/' . $intCompanyId . '/operations', $arrTestOperationData);
        $this->assertEquals(200, $addOperationResponse->status());
        $this->refreshApplication();
        /*
         * Get company operation using company ID
         */
        $getOperationResponse = $this->call('GET', '/companies/' . $intCompanyId . '/operations');
        $this->assertEquals(200, $getOperationResponse->status());
        $this->refreshApplication();
        /*
         * Force Delete company created for the testing
         */
        $objCompany = $this->model::withTrashed()->find($intCompanyId);
        $objCompany->forceDelete();
    }
}
