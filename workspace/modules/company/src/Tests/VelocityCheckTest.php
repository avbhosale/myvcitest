<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category UnitTestCase
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Tests\VolocityCheckTest;

use Modules\Company\Models\VelocityChecks;
use Modules\Company\Repositories\VelocityCheckRepository;
use Modules\Company\Transformers\VelocityCheckTransformer;
use Modules\Company\Models\Company;
use Webpatser\Uuid\Uuid;
use TestCase;

/**
 * Class for test cases
 *
 * @name     UnitTestCase
 * @category TestCase
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class VolocityCheckTest extends TestCase
{

    protected $velocitycheckRepository;
    protected $velocitycheckTransformer;
    protected $model;

    /**
     * Setup repository
     *
     * @name   setup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function setup()
    {
        parent::setUp();
        $this->velocitycheckRepository = new VelocityCheckRepository(new VelocityChecks());
        $this->velocitycheckTransformer = new VelocityCheckTransformer(new VelocityChecks());
        $this->model = new VelocityChecks();
    }

    /**
     * Test CRUD operation on API
     *
     * @name   testCRUDVelocityChecksApi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDVelocityChecksApi()
    {
        // Add company
        $arrFakeCompany = factory(Company::class)->make()->toArray();
        $addCompanyResponse = $this->call('POST', 'companies', $arrFakeCompany);
        $this->assertEquals(200, $addCompanyResponse->status());
        $intCompanyId = json_decode($addCompanyResponse->getContent())->data->id;

        //Add / Edit VelocityCheck Test Cases
        $arrFakeVelocityChecks = factory(VelocityChecks::class)->make()->toArray();
        $addVelocityChecksResponse = $this->call('POST', 'companies/' . $intCompanyId . '/velocitychecks', $arrFakeVelocityChecks);
        $this->assertEquals(200, $addVelocityChecksResponse->status());
        $this->refreshApplication();
    }

    /**
     * Test CRUD operation on API for failure
     *
     * @name   testCRUDVelocityChecksApiFail
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDVelocityChecksApiFail()
    {
        // Add company
        $arrFakeCompany = factory(Company::class)->make()->toArray();
        $addCompanyResponse = $this->call('POST', 'companies', $arrFakeCompany);
        $this->assertEquals(200, $addCompanyResponse->status());
        $intCompanyId = json_decode($addCompanyResponse->getContent())->data->id;

        //Add / Edit VelocityCheck with blank data
        $arrFakeVelocityChecks = [];
        $addVelocityChecksResponse = $this->call('POST', 'companies/' . $intCompanyId . '/velocitychecks', $arrFakeVelocityChecks);
        $this->assertEquals(422, $addVelocityChecksResponse->status());
        $this->refreshApplication();

        //Add / Edit VelocityCheck with invalid company id
        $arrFakeVelocityChecks = factory(VelocityChecks::class)->make()->toArray();
        $intCompanyId = Uuid::generate(4);
        $addVelocityChecksResponse = $this->call('POST', 'companies/' . $intCompanyId . '/velocitychecks', $arrFakeVelocityChecks);
        $this->assertEquals(404, $addVelocityChecksResponse->status());
        $this->refreshApplication();
    }
}
