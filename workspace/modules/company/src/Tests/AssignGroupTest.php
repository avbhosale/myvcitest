<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Unit_Test_Case
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Tests\GroupTest;

use Modules\Company\Models\Company;
use Modules\Group\Models\Group;
use TestCase;

/**
 * Test case for Master Group Module
 *
 * @name     AssignGroupTest
 * @category UnitTestCase
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class AssignGroupTest extends TestCase
{

    /**
     * Assign/Remove companies from particular groups
     * View Assigned companies for a perticular group
     *
     * @name   testAssignGroupApi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testAssignGroupApi()
    {
        /*
         * Create First company
         */
        $arrFakeCompany = factory(Company::class)->make()->toArray();
        $addCompanyResponse = $this->call('POST', 'companies', $arrFakeCompany);
        $this->refreshApplication();
        $this->assertEquals(200, $addCompanyResponse->status());
        $intFirstCompanyId = json_decode($addCompanyResponse->getContent())->data->id;

        /*
         * Create Second company
         */
        $addCompanyResponse = [];
        $arrFakeCompany = [];
        $arrFakeCompany = factory(Company::class)->make()->toArray();
        $addCompanyResponse = $this->call('POST', 'companies', $arrFakeCompany);
        $this->refreshApplication();
        $this->assertEquals(200, $addCompanyResponse->status());
        $intSecondCompanyId = json_decode($addCompanyResponse->getContent())->data->id;

        /*
         * Create Group
         */
        $arrFakeGroup = factory(Group::class)->make()->toArray();
        $addResponse = $this->call('POST', 'groups', $arrFakeGroup);
        $this->refreshApplication();
        $this->assertEquals(200, $addResponse->status());
        $intGroupId = json_decode($addResponse->getContent())->data->id;


        /*
         * Assign Group to both Companies
         */
        $groupArr = [];
        $groupArr['group_id'] = $intGroupId;
        $groupArr['company_ids'] = array($intSecondCompanyId, $intFirstCompanyId);
        $response = $this->call('POST', 'assigngroups', $groupArr);
        $this->refreshApplication();
        $this->assertEquals(200, $response->status());

        /*
         * View assigned groups
         */
        $response = $this->call('GET', 'assigngroups/' . $intGroupId);
        $this->refreshApplication();
        $this->assertEquals(200, $response->status());
    }
}
