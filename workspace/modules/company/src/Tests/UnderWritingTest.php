<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Unit_Test_Case
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Group\Tests\UnderwritingTest;

use Modules\Company\Models\Company;
use TestCase;

/**
 * Test case for Master Group Module
 *
 * @name     UnderwritingTest
 * @category Underwriting
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class UnderwritingTest extends TestCase
{

    protected $model;

    /**
     * Setup repository
     *
     * @name   setup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function setup()
    {
        parent::setUp();
        $this->model = new Company();
    }

    /**
     * Assign/Remove company operation
     *
     * @name   testCompanyOperation
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCompanyOperation()
    {
        /*
         * Create company
         */
        $arrFakeCompany = factory(Company::class)->make()->toArray();
        $addCompanyResponse = $this->call('POST', 'companies', $arrFakeCompany);
        $this->refreshApplication();
        $this->assertEquals(200, $addCompanyResponse->status());
        $intCompanyId = json_decode($addCompanyResponse->getContent())->data->id;
        /*
         * Update company underwriting
         */
        $arrayTestUnderwritingData = [
            "has_bank_statements" => 1,
            "has_driver_license" => 1,
            "has_business_license" => 1,
            "has_other" => 1
        ];
        $addUnderWritingResponse = $this->call('POST', '/companies/' . $intCompanyId . '/underwritings', $arrayTestUnderwritingData);
        $this->assertEquals(200, $addUnderWritingResponse->status());
        $this->refreshApplication();
        /*
         * Get company underwriting using company ID
         */
        $getUnderWritingResponse = $this->call('GET', '/companies/' . $intCompanyId . '/underwritings');
        $this->assertEquals(200, $getUnderWritingResponse->status());
        $this->refreshApplication();
        /*
         * Force Delete company created for the testing
         */
        $objCompany = $this->model::withTrashed()->find($intCompanyId);
        $objCompany->forceDelete();
    }
}
