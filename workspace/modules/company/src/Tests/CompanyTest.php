<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category UnitTestCase
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Tests\CompanyTest;

use Modules\Company\Models\Company;
use Modules\Company\Repositories\CompanyRepository;
use Modules\Company\Transformers\CompanyTransformer;
use TestCase;

/**
 * Class for test cases
 *
 * @name     UnitTestCase
 * @category TestCase
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CompanyTest extends TestCase
{

    protected $companyRepository;
    protected $companyTransformer;
    protected $model;

    /**
     * Setup repository
     *
     * @name   setup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function setup()
    {
        parent::setUp();
        $this->companyRepository = new CompanyRepository(new Company());
        $this->companyTransformer = new CompanyTransformer(new Company());
        $this->model = new Company();
    }

    /**
     * Test CRUD operation on API
     *
     * @name   testCRUDCompanyApi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDCompanyApi()
    {
        // list companies
        $listResponse = $this->call('GET', 'companies');
        $this->assertEquals(200, $listResponse->status());
        $this->refreshApplication();

        // list companies paginationation and sorting
        $listResponse = $this->call('GET', 'companies?page=3&limit=5&sort=-Name');

        $this->assertEquals(200, $listResponse->status());
        $this->refreshApplication();

        // add company
        $arrFakeCompany = factory(Company::class)->make()->toArray();
        $addCompanyResponse = $this->call('POST', 'companies', $arrFakeCompany);
        $this->assertEquals(200, $addCompanyResponse->status());

        $intCompanyId = json_decode($addCompanyResponse->getContent())->data->id;

        //edit company
        $arrEditFakeCompany = factory(Company::class)->make()->toArray();
        $editCompanyResponse = $this->call('PUT', 'companies/' . $intCompanyId, $arrEditFakeCompany);
        $this->assertEquals(200, $editCompanyResponse->status());

        //activate company
        $activateCompanyResponse = $this->call('PUT', 'companies/' . $intCompanyId . '/activate', []);
        $this->assertEquals(200, $activateCompanyResponse->status());

        //De-activate company
        $deactivateCompanyResponse = $this->call('PUT', 'companies/' . $intCompanyId . '/deactivate', []);
        $this->assertEquals(200, $deactivateCompanyResponse->status());
    }
}
