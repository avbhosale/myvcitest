<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category UnitTestCase
 * @package  Company
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Company\Tests\CompanyAddressTest;

use Modules\Company\Models\Address;
use Modules\Company\Traits\Address\AddCompanyAddress;
use Modules\Company\Traits\Address\UpdateCompanyAddress;
use Modules\Company\Traits\Address\DeleteCompanyAddress;
use Modules\Company\Traits\Address\AddressAttributes;
use Modules\Company\Traits\Address\AddressValidator;
use TestCase;

/**
 * Class for test cases
 *
 * @name     UnitTestCase
 * @category TestCase
 * @package  Address
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CompanyAddressTest extends TestCase
{

    protected $model;

    use AddCompanyAddress;
    use AddressAttributes;
    use AddressValidator;
    use UpdateCompanyAddress;
    use DeleteCompanyAddress;

    /**
     * Setup repository
     *
     * @name   setup
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function setup()
    {
        parent::setUp();
        $this->model = new Address();
    }

    /**
     * Test CRUD operation on API
     *
     * @name   testCRUDAddressApi
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDAddressApi()
    {
        $arrFakeAddress = factory(Address::class)->make()->toArray();
        $companyId = $arrFakeAddress["company_id"];
        $this->refreshApplication();

        // add address
        $addResponse = $this->call('POST', 'companies/' . $companyId . '/addresses', $arrFakeAddress);
        $this->assertEquals(200, $addResponse->status());
        $this->refreshApplication();


        // get last inserted row by id
        $intAddressId = json_decode($addResponse->getContent())->data->id;
        $this->refreshApplication();


        // edit address by ID

        $arrFakeAddress = factory(Address::class)->make()->toArray();
        $companyId = $arrFakeAddress["company_id"];
        $this->refreshApplication();
        $arrFakeAddr = factory(Address::class)->make()->toArray();
        $arrFakeAddr['id'] = $intAddressId;

        $editAddrResponse = $this->call('PUT', 'companies/' . $companyId . '/addresses/' . $intAddressId, $arrFakeAddr);
        $this->assertEquals(200, $editAddrResponse->status());
        $this->refreshApplication();


        // delete address by ID
        $intAddressIds = json_decode($editAddrResponse->getContent())->data->id;
        $deleteResponse = $this->call('DELETE', 'companies/' . $companyId . '/addresses/' . $intAddressIds);
        $this->assertEquals(200, $deleteResponse->status());
        $this->refreshApplication();


        // hard delete last inserted record from database
        $objAddress = $this->model::withTrashed()->find($intAddressIds);
        $objAddress->forceDelete();
    }

    /**
     * Test CRUD operation on API for failure
     *
     * @name   testCRUDAddressFail
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function testCRUDAddressFail()
    {
        $arrFakeAddress = [];
        $arrFakeAddress = factory(Address::class)->make()->toArray();
        $companyId = $arrFakeAddress["company_id"];
        $this->refreshApplication();

        $intFakeID = factory(Address::class)->make()->toArray()['created_at'];
        $this->refreshApplication();

        // add address
        $addResponse = $this->call('POST', 'companies/' . $arrFakeAddress['id'] . '/addresses', $arrFakeAddress);
        $this->assertEquals(404, $addResponse->status());
        $this->refreshApplication();


        // blank ID in edit
        $intAddressId = '';
        $editResponse = $this->call('PUT', 'companies/' . $companyId . '/addresses/' . $intAddressId, $arrFakeAddress);
        $this->assertEquals(405, $editResponse->status());
        $this->refreshApplication();


        // invalid ID in edit
        $intFakeID = 'fdfsfsdfdsf';
        $editResponse = $this->call('PUT', 'companies/' . $companyId . '/addresses/' . $intFakeID, factory(Address::class)->make()->toArray());
        $this->assertEquals(500, $editResponse->status());
        $this->refreshApplication();


        //delete address by ID
        $deleteResponse = $this->call('DELETE', 'companies/' . $companyId . '/addresses/' . $intFakeID);
        $this->assertEquals(500, $deleteResponse->status());
        $this->refreshApplication();


        //delete address by invalid ID
        $intTestFakeID = 'ab7ad310-6985-11e8-9b53-213c65f8c3b7';
        $deleteResponse = $this->call('DELETE', 'companies/' . $companyId . '/addresses/' . $intTestFakeID);
        $this->assertEquals(404, $deleteResponse->status());
        $this->refreshApplication();
    }
}
