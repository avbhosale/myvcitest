<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Model
 * @package  Bank
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Bank\Models;

use App\BaseModel;

/**
 * Bank Model
 *
 * @name     Odfi.php
 * @category Model
 * @package  Bank
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class Odfi extends BaseModel
{

    protected $keyType = 'string';
    protected $tablePrefix = 'Bank';
    protected $table = 'Odfi';
    protected $primaryKey = 'OdfiId';
    protected $perPage;
    protected $fillable = [
        'OdfiId',
        'RoutingNumber',
        'Abbreviation',
        'Code',
        'SftpSendHost',
        'SftpSendUsername',
        'SftpSendKey ',
        'SftpSendPort',
        'SftpReceiveHost',
        'SftpReceiveUsername',
        'SftpReceiveKey',
        'SftpReceivePort',
        'Status ',
        'Etag',
        'DeletedAt'
    ];
    public $timestamps = false;
    public $odfiErrCodes = [
        'routing_number' => 'OD001',
        'abbreviation' => 'OD002',
        'code' => 'OD003',
        'sftp_send_host' => 'OD004',
        'sftp_send_username' => 'OD006',
        'sftp_send_key' => 'OD006',
        'sftp_send_port' => 'OD006',
        'sftp_receive_host' => 'OD004',
        'sftp_receive_username' => 'OD006',
        'sftp_receive_key' => 'OD006',
        'sftp_receive_port' => 'OD006',
        'status' => 'OD006'
    ];

    /**
     * Default Constructor
     *
     * @param array $attributes request array
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $this->perPage = config('app.records_per_page');
    }
}
