<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Swagger
 * @package  Bank
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Bank\Models;

/**
 * Output response in json format
 *
 * @SWG\Definition(
 *  definition="BankGetData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="array",
 *  @SWG\Items(ref="#/definitions/Data")),
 *  @SWG\Property(property="meta",
 *  type="object", ref="#/definitions/Meta"),
 *  @SWG\Property(property="links",
 *  type="object", ref="#/definitions/Links")
 * )
 */


/**
 * Swagger Definition
 *
 * @SWG\Definition(
 *  definition="BankData",
 *  type="object",
 *  @SWG\Property(property="data",
 *  type="object",  ref="#/definitions/Data")
 * )
 */


/**
 * Create api for Bank
 *
 * @SWG\Definition(
 *  definition="BankPostData",
 *  type="object",
 *  @SWG\Property(property="parent_id",                 type="string",  description="parent_id"),
 *  @SWG\Property(property="type",                      type="string",  description="type"),
 *  @SWG\Property(property="odfi_id",                   type="string",  description="odfi_id"),
 *  @SWG\Property(property="name",                      type="string",  description="name"),
 *  @SWG\Property(property="doing_business_as",         type="string",  description="doing_business_as"),
 *  @SWG\Property(property="tax_id",                    type="string",  description="tax_id"),
 *  @SWG\Property(property="pin_number",                type="string",  description="pin_number"),
 *  @SWG\Property(property="uuid",                      type="string",  description="uuid"),
 *  @SWG\Property(property="gateway_id",                type="string",  description="gateway_id"),
 *  @SWG\Property(property="can_issue_credit",          type="integer", description="can_issue_credit"),
 *  @SWG\Property(property="can_issue_credit_approved", type="integer", description="can_issue_credit_approved"),
 *  @SWG\Property(property="can_issue_credit_active",   type="integer", description="can_issue_credit_active"),
 *  @SWG\Property(property="has_bankstatements",        type="integer", description="has_bankstatements"),
 *  @SWG\Property(property="has_driver_license",        type="integer", description="has_driver_license"),
 *  @SWG\Property(property="has_business_license",      type="integer", description="has_business_license"),
 *  @SWG\Property(property="has_other",                 type="integer", description="has_other"),
 *  @SWG\Property(property="status",                    type="integer", description="status"),
 *  @SWG\Property(property="created_by",                type="string",  description="created_by"),
 *  @SWG\Property(property="created_at",                type="string",  description="created_at"),
 * )
 */

/**
 * Update api for Bank .
 *
 * @SWG\Definition(
 *  definition="BankPutData",
 *  type="object",
 *  @SWG\Property(property="parent_id",                 type="string",  description="parent_id"),
 *  @SWG\Property(property="type",                      type="string",  description="type"),
 *  @SWG\Property(property="odfi_id",                   type="string",  description="odfi_id"),
 *  @SWG\Property(property="name",                      type="string",  description="name"),
 *  @SWG\Property(property="doing_business_as",         type="string",  description="doing_business_as"),
 *  @SWG\Property(property="tax_id",                    type="string",  description="tax_id"),
 *  @SWG\Property(property="pin_number",                type="string",  description="pin_number"),
 *  @SWG\Property(property="uuid",                      type="string",  description="uuid"),
 *  @SWG\Property(property="gateway_id",                type="string",  description="gateway_id"),
 *  @SWG\Property(property="can_issue_credit",          type="integer", description="can_issue_credit"),
 *  @SWG\Property(property="can_issue_credit_approved", type="integer", description="can_issue_credit_approved"),
 *  @SWG\Property(property="can_issue_credit_active",   type="integer", description="can_issue_credit_active"),
 *  @SWG\Property(property="has_bankstatements",        type="integer", description="has_bankstatements"),
 *  @SWG\Property(property="has_driver_license",        type="integer", description="has_driver_license"),
 *  @SWG\Property(property="has_business_license",      type="integer", description="has_business_license"),
 *  @SWG\Property(property="has_other",                 type="integer", description="has_other"),
 *  @SWG\Property(property="status",                    type="integer", description="status"),
 *  @SWG\Property(property="created_by",                type="string",  description="created_by"),
 *  @SWG\Property(property="created_at",                type="string",  description="created_at"),
 * )
 */

/**
 * Delete Bank data
 *
 * @SWG\Definition(
 *  definition="BankDeleteData",
 *  type="object",
 *  @SWG\Property(property="id", type="string", description="id"),
 * )
 */

/**
 * Api for Get all Bank data
 *
 * @SWG\Definition(
 *  definition="ListBankData",
 *  type="object",
 *          @SWG\Property(property="id",                        type="string",  description="id"),
 *          @SWG\Property(property="type",                      type="string",  description="type"),
 *  @SWG\Property(
 *          property="attributes",
 *          type="object",
 *          @SWG\Property(property="parent_id",                 type="string",  description="parent_id"),
 *          @SWG\Property(property="type",                      type="string",  description="type"),
 *          @SWG\Property(property="odfi_id",                   type="string",  description="odfi_id"),
 *          @SWG\Property(property="name",                      type="string",  description="name"),
 *          @SWG\Property(property="doing_business_as",         type="string",  description="doing_business_as"),
 *          @SWG\Property(property="tax_id",                    type="string",  description="tax_id"),
 *          @SWG\Property(property="website_url",               type="string",  description="website_url"),
 *          @SWG\Property(property="pin_number",                type="string",  description="pin_number"),
 *          @SWG\Property(property="uuid",                      type="string",  description="uuid"),
 *          @SWG\Property(property="gateway_id",                type="string",  description="gateway_id"),
 *          @SWG\Property(property="can_issue_credit",          type="integer", description="can_issue_credit"),
 *          @SWG\Property(property="can_issue_credit_approved", type="integer", description="can_issue_credit_approved"),
 *          @SWG\Property(property="can_issue_credit_active",   type="integer", description="can_issue_credit_active"),
 *          @SWG\Property(property="has_bankstatements",        type="integer", description="has_bankstatements"),
 *          @SWG\Property(property="has_driver_license",        type="integer", description="has_driver_license"),
 *          @SWG\Property(property="has_business_license",      type="integer", description="has_business_license"),
 *          @SWG\Property(property="has_other",                 type="integer", description="has_other"),
 *          @SWG\Property(property="other",                     type="string",  description="other"),
 *          @SWG\Property(property="reason_for_deactivation",   type="string",  description="reason_for_deactivation"),
 *          @SWG\Property(property="status",                    type="integer", description="status"),
 *          @SWG\Property(property="created_by",                type="string",  description="created_by"),
 *          @SWG\Property(property="created_at",                type="string",  description="created_at"),
 *      )
 * )
 */
