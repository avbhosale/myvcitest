<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateOdfiTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateOdfiTable class is used to create Bank.Odfi table
 *
 * @name     CreateOdfiTable.php
 * @category Migration
 * @package  CreateOdfiTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateOdfiTable extends Migration
{

    public $tableName;
    public $tablePrefix;
    public $seperator;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $this->tablePrefix = 'Bank'; // Schema Name to Identify Table Bank
        $tableName = 'Odfi'; // Table Name
        $this->seperator = config('app.db_schema_seperator');
        $this->tableName = $this->tablePrefix . $this->seperator . $tableName;
    }

    /**
     * Function used to Create table Bank.Odfi
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('OdfiId')->primary()->default(DB::raw('newsequentialid()')) : $table->bigIncrements('OdfiId')->unsigned()->primary();
            $table->bigInteger('RoutingNumber');
            $table->string('Abbreviation', '4')->unique()->nullable();
            $table->string('Code', '8')->unique()->nullable();
            $table->string('SftpSendHost', '16');
            $table->string('SftpSendUsername', '64');
            $table->longText('SftpSendKey');
            $table->smallInteger('SftpSendPort')->nullable();
            $table->string('SftpReceiveHost', '16');
            $table->string('SftpReceiveUsername', '64');
            $table->longText('SftpReceiveKey');
            $table->smallInteger('SftpReceivePort')->nullable();
            $table->tinyInteger('Status')->default(0);
            $table->integer('Etag')->default(0);
            $table->timestamp('DeletedAt')->nullable();
        });
    }

    /**
     * Function used to Drop table Bank.Odfi
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
