<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  CreateOdfiFedWindowTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateOdfiFedWindowTable class is used to create Bank.OdfiFedWindow table
 *
 * @name     CreateOdfiFedWindowTable.php
 * @category Migration
 * @package  CreateOdfiFedWindowTable
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateOdfiFedWindowTable extends Migration
{

    public $tableName;
    public $tablePrefix;
    public $seperator;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $this->tablePrefix = 'Bank'; // Schema Name to Identify Table Bank
        $tableName = 'OdfiFedWindow'; // Table Name
        $this->seperator = config('app.db_schema_seperator');
        $this->tableName = $this->tablePrefix . $this->seperator . $tableName;
    }

    /**
     * Function used to Create table  Bank.OdfiFedWindow
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('OdfiFedWindowId')->primary()->default(DB::raw('newsequentialid()')) : $table->bigIncrements('OdfiFedWindowId')->unsigned()->primary();
            config('database.default') == 'sqlsrv' ? $table->uuid('OdfiId')->index() : $table->bigInteger('OdfiId')->index()->unsigned();
            $table->time('FedStartTiming');
            $table->time('FedEndTiming');
            $table->tinyInteger('Type')->default(1);
            $table->tinyInteger('Status');
            $table->integer('Etag')->default(0);
            $table->timestamp('DeletedAt')->nullable();
            $table->foreign('OdfiId', 'OdfiFedWindow_Odfi_OdfiId')->references('OdfiId')->on('Bank.Odfi')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table  Bank.OdfiFedWindow
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function ($table) {
            $table->dropForeign('OdfiFedWindow_Odfi_OdfiId');
        });
        Schema::drop($this->tableName);
    }
}
