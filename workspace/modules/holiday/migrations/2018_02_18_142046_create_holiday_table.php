<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateFeeTable class is used to create Master.Fee table
 *
 * @name     CreateHolidayTable
 * @category Migration
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateHolidayTable extends Migration
{

    public $tableName;
    public $tablePrefix;
    public $seperator;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tablePrefix = 'Master'; // Schema Name to Identify Table group
        $tableName = 'Holiday'; // Table Name
        $this->seperator = config('app.db_schema_seperator');
        $this->tableName = $this->tablePrefix . $this->seperator . $tableName;
    }

    /**
     * Function used to Create table Master.Holiday
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('HolidayId')->primary() : $table->bigIncrements('HolidayId');
            $table->string('Name', 128);
            $table->string('Description', 256)->nullable();
            $table->date('ActualDate');
            $table->smallInteger('DayOfYear')->nullable();
            $table->string('Type', 16);
            $table->string('StateId', 2)->nullable();
            $table->tinyInteger('Status')->default(0);
            $table->integer('CreatedAt');
            $table->integer('Etag')->default(0);
            $table->dateTime('DeletedAt')->nullable();
        });
    }

    /**
     * Function used to Drop table Master.Holiday
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
