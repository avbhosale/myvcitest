<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Migration
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateFeeTable class is used to create Master.Fee table
 *
 * @name     CreateFeeTable
 * @category Migration
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateFeeTable extends Migration
{

    public $tableName;
    public $tablePrefix;
    public $seperator;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tablePrefix = 'Master'; // Schema Name to Identify Table group
        $tableName = 'Fee'; // Table Name
        $this->seperator = config('app.db_schema_seperator');
        $this->tableName = $this->tablePrefix . $this->seperator . $tableName;
    }

    /**
     * Function used to Create table Master.Fee
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('FeeId')->primary() : $table->bigIncrements('FeeId');
            $table->string('Name', 128);
            $table->string('Description', 1024)->nullable();
            $table->string('ApplicableOn', 64);
            config('database.default') == 'sqlsrv' ? $table->uuid('SecId')->index()->nullable() : $table->bigInteger('SecId')->index()->unsigned()->nullable();
            $table->string('Event', 128);
            $table->tinyInteger('HasAmount')->default(0);
            $table->tinyInteger('HasPercentage')->default(0);
            $table->string('Process', 4);
            $table->tinyInteger('HasScheduleA')->default(0);
            $table->tinyInteger('HasTiered')->default(0);
            $table->integer('EffectiveStartDate');
            $table->integer('EffectiveEndDate')->nullable();
            $table->tinyInteger('Status')->default(0);
            $table->integer('CreatedAt');
            $table->integer('Etag')->default(0);
            $table->dateTime('DeletedAt')->nullable();
            $table->foreign('SecId')->references('SecId')->on($this->tablePrefix . $this->seperator . 'Sec')->onDelete('cascade');
        });
    }

    /**
     * Function used to Drop table Master.Fee
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
