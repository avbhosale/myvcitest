<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Token
 * @package  TokenManagement
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Infrastructure\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Config;
use function config;
use function GuzzleHttp\json_decode;
use function version;

/**
 * Common TokenService trait class which covers all token management functions.
 * Class also verifies user identity for every user request and gives valid user response
 *
 * @name     TokenService
 * @category Token
 * @package  TokenManagement
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait TokenService
{

    protected $tablePrefix = 'Master';
    protected $table = 'Configuration';

    /**
     * Function handles JWT token decodes in three different categories like
     * Header/Payload/identity.
     * Header and payload contains user information and application information
     *
     * @param Obj $request is used to get data sent by client
     *
     * @name   decodeToken
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function decodeToken($request)
    {
        $token = '';
        $token = $request->header('Authorization', '');
        $tokenString = substr($token, 7);
        $response = [];
        if (isset($tokenString) && $tokenString <> '') {
            $decodedToken = explode('.', $tokenString);
            if ($decodedToken[1] <> null) {
                $response = json_decode(base64_decode($decodedToken[1]));
            }
        }
        return $response;
    }

    /**
     * Function saves admin token into the database
     *
     * @param string $token is used to get data sent by client
     *
     * @name   saveAdminToken
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return boolean
     */
    public function saveAdminToken($token)
    {
        /*
         * Todo this is going into azure key lock service
         * Creation of Configuration repository
         * work under construction
         */
        $table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $result = app('db')->update("UPDATE " . $table . " SET ConfigValue='" . $token . "'  where ConfigKey='Access-Token'");
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function for getAdminToken using client and secret id
     *
     * @name   getAdminToken
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $token
     */
    public function getAdminToken()
    {
        /*
         * Todo proper db call after seed
         * Creation of Configuration repository
         * under construction
         */
        $token = '';
        $table = $this->tablePrefix . config('app.db_schema_seperator') . $this->table;
        $tokenArr = app('db')->select("SELECT * FROM " . $table . " where ConfigKey='Access-Token'");
        $dbToken['accessToken'] = '';
        if (isset($tokenArr) && $tokenArr <> '') {
            foreach ($tokenArr as $tokenVal) {
                if ($tokenVal->ConfigKey == 'Access-Token') {
                    $token = $tokenVal->ConfigValue;
                }
            }
        }
        if (isset($token)) {
            $token = $this->validateJWT($token);
        }
        return $token;
    }

    /**
     * Function validate admin token life time and identity using graph api
     * Function re-generate if token is expired.
     *
     * @param string $accessToken is database admin token
     *
     * @name   validateJWT
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $accessToken
     */
    public function validateJWT($accessToken = '')
    {
        $client = new Client();
        $settings = Config::get('cloud.b2c_config');
        $userUrl = $settings['resource'] . $settings['tenant'] . '/me?api-version=' . $settings['version'];
        if ($accessToken) {
            $decodedToken = explode('.', $accessToken);
            if (isset($decodedToken[1]) && $decodedToken[1] <> null) {
                $tokenVal = json_decode(base64_decode($decodedToken[1]));
                if (isset($tokenVal->aud) && isset($tokenVal->exp)) {
                    if ($tokenVal->aud == $settings['resource'] && $tokenVal->exp > time()) {
                        return $accessToken;
                    } else {
                        $accessToken = $this->generateAdminToken();
                    }
                } else {
                    $accessToken = $this->generateAdminToken();
                }
            }
        } else {
            $accessToken = $this->generateAdminToken();
        }
        return $accessToken;
    }

    /**
     * Common function which generates admin access token using client_credentials
     *
     * @name   generateAdminToken
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $accessToken
     */
    public function generateAdminToken()
    {
        $guzzle = new Client();
        $accessToken = '';
        $azureConfig = Config::get('cloud.azure_store_config');
        $token = json_decode($guzzle->post($azureConfig['urlAccessToken'], ['form_params' => ['client_id' => $azureConfig['clientId'], 'client_secret' => $azureConfig['clientSecret'], 'resource' => 'https://graph.windows.net', 'grant_type' => 'client_credentials',]])->getBody()->getContents());
        $accessToken = $token->access_token;
        if (isset($accessToken) && $accessToken <> '') {
            if ($this->saveAdminToken($accessToken)) {
                $accessToken = $token->access_token;
            }
        }
        return $accessToken;
    }
}
