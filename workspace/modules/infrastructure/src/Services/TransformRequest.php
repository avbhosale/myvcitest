<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Transformer
 * @package  Infrastructure
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Infrastructure\Services;

use Illuminate\Http\Request;

/**
 * Common GetTransformRequest method to get input request from user and convert input to transform format
 *
 * @name     GetTransformRequest
 * @category Transformer
 * @package  Infrastructure
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait TransformRequest
{

    /**
     * Function get input request from user and convert input to transform format
     *
     * @param Array   $input               request Array
     * @param Array   $arrTransformRequest Transform array mapping
     * @param Bollaen $transform           Transform
     *
     * @name   getTransformRequest
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return array
     */
    public function getTransformRequest($input, $arrTransformRequest, $transform = true)
    {
        $transformData = [];
        foreach ($input as $key => $value) {
            if (isset($arrTransformRequest[$key])) {
                $transformData[$arrTransformRequest[$key]] = $value;
            }
        }

        if ($transform) {
            $transformData["Etag"] = time();

            if (!isset($input['id']) || '' == $input['id']) {
                $transformData['CreatedAt'] = time();
            }
        }

        return $transformData;
    }
}
