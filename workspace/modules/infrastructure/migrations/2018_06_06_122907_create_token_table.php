<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Migration
 * @package  Infrastructure
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration class for token table
 *
 * @name     CreateTokenTable
 * @category Migration
 * @package  Infrastructure
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateTokenTable extends Migration
{

    public $tableName;

    /**
     * Constructor Function
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'dbo'; // Schema Name to Identify Table Token
        $tableName = 'Token'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to Create table Token.Token
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Token', function (Blueprint $table) {
            config('database.default') == 'sqlsrv' ? $table->uuid('TokenId')->primary() : $table->bigIncrements('TokenId')->nullable()->primary()->unsigned();
            $table->string('Key', '64')->nullable()->default('NULL');
            $table->string('Value', '64')->nullable()->default('NULL');
            $table->string('Type', '64')->nullable()->default('NULL');
            $table->timestamps();
        });
    }

    /**
     * Function used to Drop table Token.Token
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
