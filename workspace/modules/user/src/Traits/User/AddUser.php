<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  User
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\User\Traits\User;

use Modules\User\Transformers\UserTransformer;

/**
 * User module Add new User
 *
 * @name     AddUser
 * @category Trait
 * @package  User
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait AddUser
{

    /**
     * Add User Method
     *
     * @param Request $request  is request object
     * @param Object  $cloudObj Object of Azure Cloud
     *
     * @name   addUser
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function addUser($request, $cloudObj)
    {
        $response = [];
        $validatorResponse = $this->validateRequest($request, $this->_userValidationRules('store'), $this->_userValidationMessage('store'), $this->_setUserAttributes(), $this->_setUserAttributeCode());
        if ($validatorResponse === true) {
            $userObj = $cloudObj->save($request);
            $decodeArr = json_decode($userObj, true);
            if (isset($decodeArr['data']['error']) && $decodeArr['data']['error'] <> '') {
                $response = $data['error']['message'] = "Validation error";
                if (isset($decodeArr['data']['error']['message']) && strpos($decodeArr['data']['error']['message'], 'mailNickname') !== false) {
                    $response = $data['error']['errors'] = $decodeArr['data']['error'];
                    $response['message'] = 'Invalid value specified for property first or last name. (Err Code: UR001)';
                } elseif (isset($decodeArr['data']['error']['message']) && strpos($decodeArr['data']['error']['message'], 'signInNames ') !== false) {
                    $response = $data['error']['errors'] = $decodeArr['data']['error'];
                    $response['message'] = 'An account for the specified email address already exists. Try another email address. (Err Code: UR003)';
                } else {
                    $response = $data['error']['errors'] = $decodeArr['data']['error'];
                }
            } else {
                $response = $this->response()->item(json_decode($userObj), new UserTransformer, ['key' => 'user']);
            }

            return $response;
        }
    }
}
