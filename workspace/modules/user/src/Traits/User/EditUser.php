<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  User
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\User\Traits\User;

use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * User module edit AD user
 *
 * @name     EditUser
 * @category Trait
 * @package  User
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait EditUser
{

    /**
     * Edit User Method from Active Directory
     *
     * @param String  $id       User Id
     * @param Request $request  is request object
     * @param Object  $cloudObj Object of Azure Cloud
     *
     * @name   deleteUser
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function editUser($id, $request, $cloudObj)
    {
        try {
            if (isset($id) && $id <> '') {
                $validatorResponse = $this->validateRequest($request, $this->_userValidationRules('update'), $this->_userValidationMessage('update'), $this->_setUserAttributes(), $this->_setUserAttributeCode());
                if ($validatorResponse === true) {
                    $response = $cloudObj->update($id, $request);
                    if (!empty($response)) {
                        $result = json_decode($response, true);
                    } else {
                        $name = $request->first_name . ' ' . $request->last_name;

                        $aro = $this->_aroRepository->updateUserNameByModelId($name, $id);

                        $result['data'] = array('message' => 'User sucessfully updated', 'status_code' => static::SUCCESS_CODE);
                    }
                    return $result['data'];
                } else {
                    throw new UnauthorizedHttpException('Restrict', 'Problem with user updation');
                }
            } else {
                throw new UnauthorizedHttpException('Restrict', 'Invalid user id');
            }
        } catch (QueryException $e) {
            $this->errorInternal();
        }
    }
}
