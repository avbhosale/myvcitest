<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  User
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\User\Traits\User;

use Modules\User\Transformers\UserTransformer;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * User module list AD user
 *
 * @name     ShowUser
 * @category Trait
 * @package  User
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ShowUser
{

    /**
     * Show User from Active Directory
     *
     * @param String $id       User Id
     * @param Object $cloudObj Object of Azure Cloud
     *
     * @name   showUser
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function showUser($id, $cloudObj)
    {
        $response = [];
        if (isset($id) && $id <> '') {
            $userObj = $cloudObj->show($id);
        } else {
            throw new UnauthorizedHttpException('Restrict', 'Invalid user id');
        }
        if (isset($userObj) && $userObj <> '') {
            if (isset($userObj['error']) && $userObj['error'] <> '') {
                return $userObj;
            } else {
                $response = $this->response()->item(json_decode($userObj), new UserTransformer, ['key' => 'user']);
            }
        } else {
            throw new UnauthorizedHttpException('Restrict', 'User with following details not present');
        }
        return $response;
    }
}
