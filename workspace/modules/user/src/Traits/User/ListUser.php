<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  User
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\User\Traits\User;

use Modules\User\Transformers\UserTransformer;

/**
 * List user details from AD
 *
 * @name     ListUser
 * @category Trait
 * @package  User
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
trait ListUser
{

    /**
     * List Users from Active Directory
     *
     * @param Object $request  Object of request class
     * @param Object $cloudObj Object of Azure Cloud
     *
     * @name   listUser
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return json
     */
    public function listUser($request, $cloudObj)
    {
        $response = [];
        $userObj = $cloudObj->all($request);
        $baseCollection = collect($userObj);
        $meta = [];
        $user = [];
        $tokenInfo = $this->decodeToken($request);
        if (isset($userObj->value)) {
            $userCollection = collect($userObj->value);
        } else {
            if (!empty($userObj)) {
                $userCollection = collect($userObj);
            } else {
                $userCollection = collect();
            }
        }
        if (isset($baseCollection['odata.nextLink']) && $baseCollection['odata.nextLink'] <> '') {
            $meta['next'] = $baseCollection['odata.nextLink'];
            $meta['prev'] = $baseCollection['odata.nextLink'];
        }
        if (isset($baseCollection['odata.metadata']) && $baseCollection['odata.metadata'] <> '') {
            $meta['url'] = $baseCollection['odata.metadata'];
        }

        $response = $this->response()->collection($userCollection, new UserTransformer, ['key' => 'user'])->setMeta($meta);
        return $response;
    }
}
