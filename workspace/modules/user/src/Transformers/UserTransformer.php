<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category Auth
 * @package  Authentication
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\User\Transformers;

use Illuminate\Support\Facades\Config;
use League\Fractal\TransformerAbstract;

/**
 * User transformer class for formatted user object
 *
 * @name     UserTransformer
 * @category Transformer
 * @package  Transformer
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class UserTransformer extends TransformerAbstract
{

    /**
     * Transformer function returns user object in formatted state
     *
     * @param Obj $user is used to transform the real user object
     *
     * @name   transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function transform($user)
    {
        if (Config::get('cloud.cloud_env') == 'Azure') {
            $companyId = Config::get('cloud.company_id_extension');
            $formattedUser = [
                "id" => $user->objectId,
                "first_name" => $user->givenName,
                "last_name" => $user->surname,
                "email" => isset($user->signInNames[0]->value) ? $user->signInNames[0]->value : 'NA',
                "phone" => $user->mobile,
                "address" => $user->streetAddress,
                "pincode" => $user->postalCode,
                'status' => $user->accountEnabled,
                "etag" => time(),
                "created_date" => isset($user->refreshTokensValidFromDateTime) ? $user->refreshTokensValidFromDateTime : 'NA',
            ];
            if (isset($user->$companyId)) {
                $formattedUser['company_id'] = $user->$companyId;
            }
        }
        return $formattedUser;
    }
}
