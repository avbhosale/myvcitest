<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category CloudFactory
 * @package  Factory
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\User\Factory;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Azure class loads secure cloud services platform using Microsoft Azure
 * to build sophisticated applications with increased flexibility,
 * scalability and reliability.
 *
 * @name     CloudAbstract.php
 * @category CloudFactory
 * @package  Factory
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
abstract class CloudAbstract extends Controller
{

    protected $signInPolicy;
    protected $adConfig;
    protected $b2cConfig;
    protected $companyExtension;
    protected $alterEmail;
    protected $tenent;
    protected $env;
    protected $envExtension;
    protected $top;

    /**
     * Default abstract class constructor defines all variables
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        $cloudConfig = Config::get('cloud');
        $this->signInPolicy = $cloudConfig['sign_up_sign_in_policy'];
        $this->adConfig = $cloudConfig['azure_store_config'];
        $this->b2cConfig = $cloudConfig['b2c_config'];
        $this->companyIdExtension = $cloudConfig['company_id_extension'];
        $this->alterEmail = $cloudConfig['alternate_email_extension'];
        $this->env = $cloudConfig['environment'];
        $this->envExtension = $cloudConfig['env_extension'];
        $this->top = 100;
    }

    /**
     * Returns the authorize url returned from azure AD
     *
     * @param string $redirectUrl used to pass runtime redirect url according to different portal
     * @param string $userType    used to set the login type
     *
     * @name   getAuthorizeUrl
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $authorizeUrl
     */
    public function getAuthorizeUrl($redirectUrl, $userType = 'company')
    {
        $response = [];
        if ($userType == 'company') {
            $azureConfig = $this->b2cConfig;
        }
        $azureConfig['redirectUri'] = $redirectUrl;
        $provider = new GenericProvider($azureConfig);
        $response['redirectUrl'] = $provider->getAuthorizationUrl([
            'prompt' => 'login',
            'p' => $this->signInPolicy
        ]);
        $response['statusCode'] = static::SUCCESS_CODE;
        $response['id'] = $provider->getState();
        return $response;
    }

    /**
     * Authenticate method is two step verification used to login the user
     * Method accepts code and validate with azure service
     *
     * @param Obj $request is used to get data sent by client
     *
     * @name   authenticate
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function authenticate($request)
    {
        $response = [];
        $azureConfig = $this->b2cConfig;
        $provider = new GenericProvider($azureConfig);
        $accessToken = $provider->getAccessToken('authorization_code', [
            'code' => $request->input('code'),
            'grant_type' => 'authorization_code',
            'response_type' => 'id_token'
        ]);
        if ($accessToken->getToken() !== null) {
            $response['accessToken'] = $accessToken->getToken();
            $response['expiresIn'] = $accessToken->getExpires();
            $response['refreshToken'] = $accessToken->getRefreshToken();
            $response['id'] = $request->input('code');
        } else {
            $response['error'] = "Invalid access token";
        }
        return $response;
    }

    /**
     * Function destroys user identity from Vericheck portal
     *
     * @param Obj $request is used to get data sent by client
     *
     * @name   logout
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function logout($request)
    {
        $azureConfig = $this->b2cConfig;
        $provider = new GenericProvider($azureConfig);
        $post_logout_redirect_uri = $this->b2cConfig['logoutUri']; // The logout destination after the user is logged out from their account.
        $logoutUrl['redirectUrl'] = $this->getLogoutUrl($post_logout_redirect_uri);
        $logoutUrl['id'] = $this->b2cConfig['clientId'];  // Todo add user id after accesstoken header
        return $logoutUrl;
    }

    /**
     * Destroys user access token from active directory
     *
     * @param String $post_logout_redirect_uri sent logout url logout from the system
     *
     * @name   getLogoutUrl
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function getLogoutUrl($post_logout_redirect_uri = '')
    {
        return 'https://login.microsoftonline.com/' . $this->tenent . '/oauth2/logout?post_logout_redirect_uri=' . rawurlencode($post_logout_redirect_uri);
    }

    /**
     * Create new access token when new environment build is created
     *
     * @name   getAdminAccessToken
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function getAdminAccessToken()
    {
        $guzzle = new Client();
        $response = [];
        $dbUpdate = false;
        $azureConfig = $this->adConfig;
        $token = json_decode($guzzle->post($azureConfig['urlAccessToken'], ['form_params' => ['client_id' => $azureConfig['clientId'], 'client_secret' => $azureConfig['clientSecret'], 'resource' => 'https://graph.windows.net', 'grant_type' => 'password', 'username' => $azureConfig['username'], 'Password' => $azureConfig['password']]])->getBody()->getContents());
        $accessToken = $token->access_token;
        $dbUpdate = $this->saveAdminToken($accessToken);
        if (isset($accessToken) && $accessToken <> '' && $dbUpdate) {
            $response['token'] = $accessToken;
        } else {
            $response['message'] = "problem with token updation";
        }
        return $response;
    }

    /**
     * Protected function which creates graph api user entity api routes
     *
     * @param String $entity postfix of the user graph api url
     *
     * @name   generateGraphApiUrl
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $url
     */
    public function generateGraphApiUrl($entity = '/users')
    {
        $settings = $this->b2cConfig;
        return $settings['resource'] . $settings['tenant'] . $entity . '?api-version=' . $settings['version'];
    }

    /**
     * Common attributes for the user creation
     *
     * @param string $method  identifies user operation like create/update
     * @param array  $userArr all user attributes submitted from the user create and update interface
     *
     * @name   _commonAttribute
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function commonAttribute($method = 'save', $userArr = [])
    {
        if ($method == 'save') {
            $userArr['accountEnabled'] = true;
            $userArr['creationType'] = 'LocalAccount';
            $userArr[$this->envExtension] = $this->env;
        }
        if (isset($userArr['givenName']) && isset($userArr['surname'])) {
            $userArr['givenName'] = ucfirst(strtolower($userArr['givenName']));
            $userArr['surname'] = ucfirst(strtolower($userArr['surname']));
            $userArr['displayName'] = ucwords(strtolower($userArr['givenName'] . " " . $userArr['surname']));
            $userArr['mailNickname'] = ucfirst(strtolower($userArr['givenName'])) . substr($userArr['surname'], 0, 1);
        }
        return $userArr;
    }
}
