<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * @category CloudFactory
 * @package  Factory
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\User\Factory;

use Modules\User\Factory\CloudAbstract;
use GuzzleHttp\Client;
use Modules\Infrastructure\Services\TokenService;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Azure class loads secure cloud services platform using Microsoft Azure
 * to build sophisticated applications with increased flexibility,
 * scalability and reliability.
 *
 * @name     Azure.php
 * @category CloudFactory
 * @package  Factory
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class Azure extends CloudAbstract
{

    use TokenService;

    protected $accessToken;

    /**
     * Default constructor if factory class loads all cloud factory configurations.
     * Constructor sets all private member parameters from configurations
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->accessToken = $this->getAdminToken();
        if (!isset($this->accessToken) && $this->accessToken == '') {
            throw new UnauthorizedHttpException('restrict', 'Invalid access token');
        }
    }

    /**
     * Get the list of users from Azure AD
     *
     * @param Obj $request is used to get data sent by client
     *
     * @name   all
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function all($request)
    {
        $response = [];
        $settings = $this->b2cConfig;
        $searchUrl = '';
        $tokenInfo = $this->decodeToken($request);
        $userUrl = $this->generateGraphApiUrl('/users');
        $params = $request->all();
        if (isset($userUrl) && $userUrl <> '') {
            $userUrl = $userUrl . '&$filter=accountEnabled eq true and ' . $this->envExtension . ' eq ' . "'$this->env'";
            $userUrl = $this->_commonSearch($tokenInfo, $userUrl, $params);
        }
        $response = $this->makeGraphApiCall('GET', $userUrl, $this->accessToken, 'application/json');
        return json_decode($response);
    }

    /**
     * Description
     *
     * @param Obj $tokenInfo is user information object retrieved from user access token
     * @param Obj $userUrl   is fixed search criteria for the user search
     * @param Obj $params    is used for search user api from provided search criteria
     *
     * @name   _commonSearch
     * @access private
     * @author VCI <info@vericheck.net>
     *
     * @return $userUrl
     */
    private function _commonSearch($tokenInfo, $userUrl = '', $params = [])
    {
        if (isset($tokenInfo->extension_CompanyId) && $tokenInfo->extension_CompanyId <> '') {
            $userUrl = $userUrl . ' and ' . $this->companyIdExtension . " eq  '" . $tokenInfo->extension_CompanyId . "' ";
        }
        if (isset($params['name']) && $params['name'] <> '') {
            $searchUrl = " startswith(givenName,'" . $params['name'] . "') or startswith(surname,'" . $params['name'] . "') or givenName eq '" . $params['name'] . "' or surname eq '" . $params['name'] . "' or $this->alterEmail eq '" . $params['name'] . "' ";
            $userUrl = $userUrl . " and (" . $searchUrl . ')';
        }
        $userUrl = $userUrl . '&$top=' . $this->top;
        if (isset($params['skiptoken']) && $params['skiptoken'] <> '') {
            $userUrl = $userUrl . '&$skiptoken=' . $params['skiptoken'];
        }
        return $userUrl;
    }

    /**
     * Display individual user information from active directory
     *
     * @param String $id is used to get data sent by client
     *
     * @name   show
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function show($id = '')
    {
        if ($id == '') {
            $this->errorUnauthorized('Invalid user id');
        }
        $userUrl = $this->generateGraphApiUrl('/users/' . $id);
        $response = $this->makeGraphApiCall('GET', $userUrl, $this->accessToken, 'application/json');
        return $response;
    }

    /**
     * Function create the user using azure active directory graph api
     *
     * @param Obj $request is used to get data sent by client
     *
     * @name   save
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function save($request)
    {
        $postArr = $this->_transform($request->all());
        $userUrl = $this->generateGraphApiUrl('/users');
        $userArr = $this->commonAttribute('save', $postArr);
        $response = $this->makeGraphApiCall('POST', $userUrl, $this->accessToken, 'application/json', json_encode($userArr));
        return $response;
    }

    /**
     * Function update user information into azure cloud AD
     *
     * @param String $id      is used to get data sent by client
     * @param Obj    $request is used to get data sent by client
     *
     * @name   update
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function update($id, $request)
    {
        $userArr = [];
        $postArr = $this->_transform($request->all());
        $userUrl = $this->generateGraphApiUrl('/users/' . $id);
        $userArr = $this->commonAttribute('update', $postArr);
        $response = $this->makeGraphApiCall('PATCH', $userUrl, $this->accessToken, 'application/json', json_encode($userArr));
        return $response;
    }

    /**
     * Disable user from azure active directory
     *
     * @param String $id      is used to get data sent by client
     * @param Obj    $request is used to get data sent by client
     *
     * @name   destroy
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function destroy($id, $request)
    {
        $userArr = [];
        $userUrl = $this->generateGraphApiUrl('/users/' . $id);
        $client = new Client(['base_uri' => $userUrl, 'verify' => false]);
        $userArr['accountEnabled'] = true;
        if (!$request->isMethod('patch')) {
            $userArr['accountEnabled'] = false;
        }
        $response = $this->makeGraphApiCall('PATCH', $userUrl, $this->accessToken, 'application/json', json_encode($userArr));
        return $response;
    }

    /**
     * Function convert form attributes into the azure portal attributes
     *
     * @param Obj $input is used to get data sent by client
     *
     * @name   _transform
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    private function _transform($input = [])
    {
        $companyId = $this->companyIdExtension;
        $alterEmail = $this->alterEmail;
        $azureUser = [
            'givenName' => trim($input['first_name']),
            'surname' => trim($input['last_name']),
            'streetAddress' => $input['address'],
            'postalCode' => trim($input['pincode']),
            'mobile' => isset($input['mobile_no']) ? trim($input['mobile_no']) : ''
        ];
        if (isset($input['company_id']) && $input['company_id'] <> '') {
            $azureUser[$companyId] = $input['company_id'];
        }
        if (isset($input['email']) && $input['email'] <> '') {
            $arr['type'] = 'emailAddress';
            $arr['value'] = $input['email'];
            $azureUser['signInNames'][] = $arr;
            $azureUser[$alterEmail] = $input['email'];
        }
        if (isset($input['password']) && $input['password'] <> '') {
            $azureUser['passwordProfile']['password'] = $input['password'];
            $azureUser['passwordProfile']['forceChangePasswordNextLogin'] = false;
        }
        return $azureUser;
    }
}
