<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category ServiceProvider
 * @package  Group
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
namespace Modules\Note;

use Illuminate\Support\ServiceProvider;

/**
 * Service provider for Master module
 *
 * @name     MasterNoteServiceProvider
 * @category Provider
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class MasterNoteServiceProvider extends ServiceProvider
{

    /**
     * Boot method to load services
     *
     * @name   boot
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function boot()
    {
        $sourceMigration = realpath(__DIR__ . '/../migrations');
        $this->loadMigrationsFrom($sourceMigration);
    }
}
