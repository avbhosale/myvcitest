<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Migration
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * Two line description of class
 *
 * @name     CreateDeclineCodeTable
 * @category Migration
 * @package  Master
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateDeclineCodeTable extends Migration
{

    public $tableName;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $tablePrefix = 'Master'; // Database Schema
        $tableName = 'DeclineCode'; // Table Name
        $seperator = config('app.db_schema_seperator');
        $this->tableName = $tablePrefix . $seperator . $tableName;
    }

    /**
     * Function used to create table Master.DeclineCode
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            (config('database.default') == 'sqlsrv') ? $table->uuid('CodeId')->primary() : $table->increments('CodeId');
            $table->string('Code', 16);
            $table->string('Description', 256)->nullable();
            $table->string('Scope', 8);
            $table->integer('EffectiveStartDate');
            $table->integer('EffectiveEndDate')->nullable();
            $table->tinyInteger('Status')->default(0);
            $table->integer('CreatedAt');
            $table->integer('Etag')->default(0);
            $table->dateTime('DeletedAt')->nullable();
        });
    }

    /**
     * Function used to Drop table Master.DeclineCode
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
