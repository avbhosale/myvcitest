<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */
use Webpatser\Uuid\Uuid;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(Modules\ACL\Models\ARO::class, function (Faker\Generator $faker) {
    return [
        'AroId' => Uuid::generate(4)->string,
        'ParentId' => Uuid::generate(4)->string,
        'Model' => $faker->Name,
        'ModelId' => Uuid::generate(4)->string,
        'Name' => $faker->Name,
        'Alias' => $faker->Name,
        'LeftNode' => null,
        'RightNode' => null,
        'Etag' => 12324,
        'DeletedAt' => $faker->datetime(),
    ];
});

$factory->define(Modules\ACL\Models\ACO::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'alias' => $faker->word,
    ];
});

$factory->define(Modules\ACL\Models\ARO::class, function (Faker\Generator $faker) {
    return [
        'model_id' => strtoupper(Uuid::generate(4)->string),
        'model' => $faker->word,
        'name' => $faker->word,
        'alias' => $faker->word,
    ];
});

$factory->define(\Modules\Group\Models\Group::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->sentence,
        'type' => $faker->word,
        'status' => (true == $faker->boolean(80)) ? 1 : 0,
        'created_by' => Uuid::generate(4)->string,
        'updated_by' => Uuid::generate(4)->string,
        'created_at' => time(),
        'etag' => time()
    ];
});


$factory->define(\Modules\ACL\Models\AROACO::class, function (Faker\Generator $faker) {
    return [
        'aro_id' => Uuid::generate(4)->string,
        'aco_id' => Uuid::generate(4)->string,
        'access' => (true == $faker->boolean(80)) ? 1 : 0
    ];
});

$factory->define(\Modules\Company\Models\Company::class, function (Faker\Generator $faker) {
    $gateway = \Modules\Gateway\Models\Gateway::get()->where('DeletedAt', null)->first();

    $companyType = ['iso', 'merchant'];
    return [
        "parent_id" => $faker->uuid,
        "type" => $companyType[array_rand($companyType, 1)],
        "name" => $faker->word,
        "doing_business_as" => $faker->word,
        "tax_id" => $faker->word,
        "odfi_id" => $faker->uuid,
        "uuid" => $faker->uuid,
        "gateway_id" => $gateway->GatewayId,
        "website_url" => "http://www.google.com",
        "created_by" => $faker->uuid,
        "status" => 1
    ];
});

$factory->define(\Modules\Company\Models\Address::class, function (Faker\Generator $faker) {

    $company = \Modules\Company\Models\Company::get()->where('DeletedAt', null)->first();

    return [
        "company_id" => $company->CompanyId,
        "id" => Uuid::generate(4)->string,
        "address_line1" => $faker->word,
        "address_line2" => $faker->word,
        "city" => $faker->word,
        "state" => str_shuffle('AB'),
        "zip" => str_shuffle('01234567'),
        "address_type" => 'primary',
        "created_at" => str_shuffle('012345'),
        "etag" => str_shuffle('012345'),
        "primary_phone" => str_shuffle('1234567890'),
        "secondary_phone" => str_shuffle('0987654321'),
        "primary_email" => $faker->email,
        "secondary_email" => $faker->email,
        "fax" => str_shuffle('12345abcde'),
    ];
});

$factory->define(\Modules\Company\Models\VelocityChecks::class, function (Faker\Generator $faker) {

    $companyId = \Modules\Company\Models\Company::get()->where('DeletedAt', null)->first();

    return [
        "company_id" => $companyId->CompanyId,
        "has_approved" => $faker->numberBetween(0, 1),
        "has_check_duplicate" => $faker->numberBetween(0, 1),
        "has_nsf_decline" => $faker->numberBetween(0, 1),
        "has_default_velicity" => $faker->numberBetween(0, 1),
        "max_checks_per_day" => $faker->numberBetween(1, 10),
        "max_amount_per_day" => (string) $faker->numberBetween(1, 999),
        "day_period_merchant" => $faker->numberBetween(1, 3),
        "max_checks_per_day_per_acc" => $faker->numberBetween(1, 999),
        "max_amount_per_day_per_acc" => (string) $faker->numberBetween(1, 2000),
        "day_period_account" => $faker->numberBetween(1, 5),
        "max_checks_per_period_per_acc" => $faker->numberBetween(1, 101),
        "max_amount_per_period_per_acc" => (string) $faker->numberBetween(1, 201),
        "heighest_dollar_amount_per_txn" => (string) $faker->numberBetween(1, 5001),
        "funding_time_id" => $faker->uuid,
        "type" => $faker->randomElement(['DEBIT', 'CREDIT']),
        "effective_start_date" => time(),
        "effective_end_date" => time(),
        "etag" => time()
    ];
});

