<?php

/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * It is used to create database schema.
 *
 * @name     CreateDatabaseSchema.php
 * @category Migrations
 * @package  Workflow
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT:$Id:
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class CreateDatabaseSchemaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public $schema = [// CHANGE SCHEMA VALUE HERE
        'Ach',
        'Acl',
        'Bank',
        'Billing',
        'Company',
        'Hardware',
        'Master',
        'Notification',
        'Transaction',
        'Workflow'
    ];

    /**
     * Execute the migration
     *
     * @name   up
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function up()
    {
        $query = DB::connection()->getPdo();

        foreach ($this->schema as $val) {
            $query->exec("CREATE SCHEMA [$val]");
        }
    }

    /**
     * Reverse the migration
     *
     * @name   down
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function down()
    {
        $query = DB::connection()->getPdo();

        foreach ($this->schema as $val) {
            $query->exec("DROP SCHEMA [$val]");
        }
    }

}
