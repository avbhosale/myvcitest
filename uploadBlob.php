<?php
/**
 * VERICHECK INC CONFIDENTIAL
 *
 * Vericheck Incorporated
 * All Rights Reserved.
 *
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * PHP version 7
 *
 * @category Category
 * @package  Package_Name
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT: $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
date_default_timezone_set('GMT');

/**
 * Two line description of class
 *
 * @name     Upload Blob
 * @category Blob
 * @package  Common
 * @author   VCI <info@vericheck.net>
 * @license  Copyright 2018 VeriCheck | All Rights Reserved
 * @version  GIT $Id$
 * @link     https://www.vericheck.com/docs/{link to Phpdoc}
 */
class UploadBlob
{

    private $_date = null;    
    private $_accountKey = null;
    private $_blobUrl = null;
    private $_dirPath = null;
    private $_buildNumber = null;
    private $_version = '2017-07-29';
    private $_storageAccount = null;
    private $_container = 'vcicontainer';
    private $_buildPath = '/opt/atlassian/pipelines/agent/build/';
	
    /**
     * Default construct
     *
     * @param Obj $storageAccount is used to get data sent by client
     * @param Obj $accountKey     is used to get data sent by client
     * @param Obj $directoryPath  is used to get data sent by client
     * @param Obj $buildNumber    is used to get data sent by client
     *
     * @name   __construct
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function __construct($storageAccount, $accountKey, $directoryPath, $buildNumber)
    {		
        $this->_date = date("D, d M Y H:i:s T");        
        $this->_storageAccount = $storageAccount;
        $this->_blobUrl = 'https://' . $this->_storageAccount . '.blob.core.windows.net/' . $this->_container . '/';
        $this->_accountKey = $accountKey;
        $this->_dirPath = $directoryPath;
        $this->_buildNumber = $buildNumber;
    }

    /**
     * Function used to Upload Files
     *
     * @name   uploadFiles
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    public function uploadFiles()
    {
        $path = realpath($this->_dirPath);
        $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
        foreach ($objects as $blobPath => $object) {
            if (is_dir($blobPath)) {
                continue;
            }
					
            $blobName = str_replace($this->_buildPath, 'build_logs/php-backend-api/'.$this->_buildNumber.'/', $blobPath);
            $blobName = str_replace('\\', '/', $blobName);
            $this->_upload($blobPath, $blobName);
        }
    }

    /**
     * Function used to Upload blob
     *
     * @param Obj $blobPath is used to get blob path
     * @param Obj $blobName is used to get blob name
     *
     * @name   upload
     * @access public
     * @author VCI <info@vericheck.net>
     *
     * @return $response
     */
    private function _upload($blobPath, $blobName)
    {
        $fdata = file_get_contents($blobPath);
        $mimeType = mime_content_type($blobPath);

        $utf8Encode = "PUT\n\n\n" . strlen($fdata) . "\n\n$mimeType; charset=UTF-8\n\n\n\n\n\n\nx-ms-blob-type:BlockBlob\nx-ms-date:" . $this->_date . "\nx-ms-version:" . $this->_version . "\n/$this->_storageAccount/$this->_container/" . $blobName;
        $signature = base64_encode(hash_hmac('sha256', $utf8Encode, base64_decode($this->_accountKey), true));

        $header = array(
            "x-ms-blob-type: BlockBlob",
            "x-ms-date: " . $this->_date,
            "x-ms-version: " . $this->_version,
            "Authorization: SharedKey $this->_storageAccount:" . $signature,
            "Content-Type: $mimeType; charset=UTF-8",
            "Content-Length: " . strlen($fdata)
        );		
		
        $url = $this->_blobUrl . $blobName;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fdata);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $content = curl_exec($ch);
         print($content);
    }
}

/*
 *  Example: php uploadBlob.php 123 static_analysis 01
 *  $argv[1] = Azure storage Account
 *  $argv[2] = Azure storage Key
 *  $argv[3] = Folder path
 *  $argv[4] = Build Number
 */

if ($argc >= 3) {
    $obj = new uploadBlob($argv[1], $argv[2], $argv[3], $argv[4]);
    $obj->uploadFiles();
}
